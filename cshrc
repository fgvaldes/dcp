# Set IRAF environment.
if (-e $iraf/unix/hlib/irafuser.csh) then
    source $iraf/unix/hlib/irafuser.csh
    set -f path = ($x11iraf/bin.${IRAFARCH} $path $hlib $hbin)
    set -f cdpath = ($iraf/extern $cdpath)
    unsetenv hlib hbin tmp
    alias cl $iraf/bin.${IRAFARCH}/ecl.e
endif

# Set NHPPS environment.
if (-e $NHPPS/config/Setup.csh) then
    source $NHPPS/config/Setup.csh
endif

# Set operator configuration directory.
setenv PipeConfig ~/SharedHome/PipeConfig
setenv Operator ~/SharedHome/PipeConfig

# Set NHPPS_DB.
setenv NHPPS_DB $PipeConfig/nhpps.db

# Set PSQDB.
setenv NHPPS_PSQDB $deccp/PSQDB/psqdb.deccp

# Set IQS mode.
setenv IQS DECam
setenv PGUSER pipeline
setenv PGDATABASE metadata
#setenv PGHOST archdbn2.tuc.noao.edu
setenv PGHOST db.sdm.noao.edu
setenv PGPORT 5432
setenv PGPASSWORD   Mosaic_DHS

# Set mkpkg for DCP.
alias mkdcp "mkpkg \!:* dcp=$deccp bin=$deccp/DCPBIN xml=$deccp/src/xml"

# Set lpq
setenv PRINTER dts

set -f path = ($deccp/DCPBIN $iraf/x11iraf/bin${arch} $shared/miscbin $path)
set -l cdpath = ($shared $cdpath)
