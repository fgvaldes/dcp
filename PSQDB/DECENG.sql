REPLACE INTO PSQ VALUES (
'DECENG', 'DECENG', 'DECENGD', 'DCP', 'dcp', 'disabled', 'instrument=''DECam'''
);

REPLACE INTO DECENG (dataset) VALUES ('20120910');
REPLACE INTO DECENG (dataset) VALUES ('20120911');
REPLACE INTO DECENG (dataset) VALUES ('20120912');
REPLACE INTO DECENG (dataset) VALUES ('20120913');
REPLACE INTO DECENG (dataset) VALUES ('20120914');
REPLACE INTO DECENG (dataset) VALUES ('20120915');
REPLACE INTO DECENG (dataset) VALUES ('20120916');
REPLACE INTO DECENG (dataset) VALUES ('20120917');
REPLACE INTO DECENG (dataset) VALUES ('20120918');
REPLACE INTO DECENG (dataset) VALUES ('20120919');
REPLACE INTO DECENG (dataset) VALUES ('20120920');
REPLACE INTO DECENG (dataset) VALUES ('20120921');
REPLACE INTO DECENG (dataset) VALUES ('20120922');
REPLACE INTO DECENG (dataset) VALUES ('20120925');
REPLACE INTO DECENG (dataset) VALUES ('20120926');
REPLACE INTO DECENG (dataset) VALUES ('20120927');
REPLACE INTO DECENG (dataset) VALUES ('20120928');
REPLACE INTO DECENG (dataset) VALUES ('20120929');
REPLACE INTO DECENG (dataset) VALUES ('20120930');
REPLACE INTO DECENG (dataset) VALUES ('20121001');
REPLACE INTO DECENG (dataset) VALUES ('20121002');
REPLACE INTO DECENG (dataset) VALUES ('20121003');
REPLACE INTO DECENG (dataset) VALUES ('20121004');
REPLACE INTO DECENG (dataset) VALUES ('20121005');
REPLACE INTO DECENG (dataset) VALUES ('20121006');
REPLACE INTO DECENG (dataset) VALUES ('20121007');
REPLACE INTO DECENG (dataset) VALUES ('20121008');
REPLACE INTO DECENG (dataset) VALUES ('20121009');
REPLACE INTO DECENG (dataset) VALUES ('20121017');
REPLACE INTO DECENG (dataset) VALUES ('20121021');
REPLACE INTO DECENG (dataset) VALUES ('20121025');
REPLACE INTO DECENG (dataset) VALUES ('20121028');

REPLACE INTO DECENGD VALUES ('20120910', '20120910', '20120910',
  'start_date between timestamp ''2012-09-10'' and timestamp ''2012-09-10''');
REPLACE INTO DECENGD VALUES ('20120911', '20120911', '20120911',
  'start_date between timestamp ''2012-09-11'' and timestamp ''2012-09-11''');
REPLACE INTO DECENGD VALUES ('20120912', '20120912', '20120912',
  'start_date between timestamp ''2012-09-12'' and timestamp ''2012-09-12''');
REPLACE INTO DECENGD VALUES ('20120913', '20120913', '20120913',
  'start_date between timestamp ''2012-09-13'' and timestamp ''2012-09-13''');
REPLACE INTO DECENGD VALUES ('20120914', '20120914', '20120914',
  'start_date between timestamp ''2012-09-14'' and timestamp ''2012-09-14''');
REPLACE INTO DECENGD VALUES ('20120915', '20120915', '20120915',
  'start_date between timestamp ''2012-09-15'' and timestamp ''2012-09-15''');
REPLACE INTO DECENGD VALUES ('20120916', '20120916', '20120916',
  'start_date between timestamp ''2012-09-16'' and timestamp ''2012-09-16''');
REPLACE INTO DECENGD VALUES ('20120917', '20120917', '20120917',
  'start_date between timestamp ''2012-09-17'' and timestamp ''2012-09-17''');
REPLACE INTO DECENGD VALUES ('20120918', '20120918', '20120918',
  'start_date between timestamp ''2012-09-18'' and timestamp ''2012-09-18''');
REPLACE INTO DECENGD VALUES ('20120919', '20120919', '20120919',
  'start_date between timestamp ''2012-09-19'' and timestamp ''2012-09-19''');
REPLACE INTO DECENGD VALUES ('20120920', '20120920', '20120920',
  'start_date between timestamp ''2012-09-20'' and timestamp ''2012-09-20''');
REPLACE INTO DECENGD VALUES ('20120921', '20120921', '20120921',
  'start_date between timestamp ''2012-09-21'' and timestamp ''2012-09-21''');
REPLACE INTO DECENGD VALUES ('20120922', '20120922', '20120924',
  'start_date between timestamp ''2012-09-22'' and timestamp ''2012-09-24''');
REPLACE INTO DECENGD VALUES ('20120925', '20120925', '20120925',
  'start_date between timestamp ''2012-09-25'' and timestamp ''2012-09-25''');
REPLACE INTO DECENGD VALUES ('20120926', '20120926', '20120926',
  'start_date between timestamp ''2012-09-26'' and timestamp ''2012-09-26''');
REPLACE INTO DECENGD VALUES ('20120927', '20120927', '20120927',
  'start_date between timestamp ''2012-09-27'' and timestamp ''2012-09-27''');
REPLACE INTO DECENGD VALUES ('20120928', '20120928', '20120928',
  'start_date between timestamp ''2012-09-28'' and timestamp ''2012-09-28''');
REPLACE INTO DECENGD VALUES ('20120929', '20120929', '20120929',
  'start_date between timestamp ''2012-09-29'' and timestamp ''2012-09-29''');
REPLACE INTO DECENGD VALUES ('20120930', '20120930', '20120930',
  'start_date between timestamp ''2012-09-30'' and timestamp ''2012-09-30''');
REPLACE INTO DECENGD VALUES ('20121001', '20121001', '20121001',
  'start_date between timestamp ''2012-10-01'' and timestamp ''2012-10-01''');
REPLACE INTO DECENGD VALUES ('20121002', '20121002', '20121002',
  'start_date between timestamp ''2012-10-02'' and timestamp ''2012-10-02''');
REPLACE INTO DECENGD VALUES ('20121003', '20121003', '20121003',
  'start_date between timestamp ''2012-10-03'' and timestamp ''2012-10-03''');
REPLACE INTO DECENGD VALUES ('20121004', '20121004', '20121004',
  'start_date between timestamp ''2012-10-04'' and timestamp ''2012-10-04''');
REPLACE INTO DECENGD VALUES ('20121005', '20121005', '20121005',
  'start_date between timestamp ''2012-10-05'' and timestamp ''2012-10-05''');
REPLACE INTO DECENGD VALUES ('20121006', '20121006', '20121006',
  'start_date between timestamp ''2012-10-06'' and timestamp ''2012-10-06''');
REPLACE INTO DECENGD VALUES ('20121007', '20121007', '20121007',
  'start_date between timestamp ''2012-10-07'' and timestamp ''2012-10-07''');
REPLACE INTO DECENGD VALUES ('20121008', '20121008', '20121008',
  'start_date between timestamp ''2012-10-08'' and timestamp ''2012-10-08''');
REPLACE INTO DECENGD VALUES ('20121009', '20121009', '20121009',
  'start_date between timestamp ''2012-10-09'' and timestamp ''2012-10-09''');
REPLACE INTO DECENGD VALUES ('20121017', '20121017', '20121020',
  'start_date between timestamp ''2012-10-17'' and timestamp ''2012-10-20''');
REPLACE INTO DECENGD VALUES ('20121021', '20121021', '20121024',
  'start_date between timestamp ''2012-10-21'' and timestamp ''2012-10-24''');
REPLACE INTO DECENGD VALUES ('20121025', '20121025', '20121025',
  'start_date between timestamp ''2012-10-25'' and timestamp ''2012-10-25''');
REPLACE INTO DECENGD VALUES ('20121028', '20121028', '20121028',
  'start_date between timestamp ''2012-10-28'' and timestamp ''2012-10-28''');
