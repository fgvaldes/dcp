#!/bin/env pipecl
#
# STA2NSA - Convert STA data products to NSA format.

int	naxis1, naxis2, nc, nl, n1, n2, c1, c2, l1, l2, ix, iy
real	exptime
file	html, tmp1, tmp2
string	dir, ref, in, inz, bpm, wt, exp, out, out1, extname, tile
string	plfname, pldname, plqueue, plqname, plprocid, subset, tileid
string	dateobs, propid, ncombine, ra, dec
struct	plver, obstype, title

# Load tasks and packages.
noao
artdata
task $stagsrc="$!stagsrc $(1) $(2)"
task $mkgraphic = "$!mkgraphic"

# Check data was created.
in = dataset.sname
inz = dataset.sname // "_z"
bpm = in // "_bpm"
wt = in // "_wt"
exp = in // "_exp"
if (imaccess (in) == NO)
    in = ""
;
if (imaccess (inz) == NO)
    inz = ""
;
if (in == "" && inz == "")
    plexit NODATA
;
if (in != "")
    ref = in
else
    ref = inz

# Set the PL keywords.
match ("PLVER:", "deccp$/Versions", > "sta2nsa.tmp")
tail ("sta2nsa.tmp", nl=1) | scan (s2, plver)
delete ("sta2nsa.tmp")
print (dataset.sname) | translit ("STDIN", "-", " ") |
    scan (pldname, tileid)
i = strlen (tileid)
subset = substr (tileid, i, i)
tileid = substr (tileid, 1, i-1)
print (pldname) | translit ("STDIN", "_", " ") |
    scan (plqueue, plqname, plprocid)

# Set global header. Note, don't exclude extension keywords here.
mkglbhdr (ref, "sta2nsa_tmp",
    exclude="@"//nhppsargs.srcdir//"sta2nsa_delete.dat")

obstype = "object"
hedit ("sta2nsa_tmp", "NEXTEND", 1, add+)
hedit ("sta2nsa_tmp", "FILENAME", "dummy", add+)
hedit ("sta2nsa_tmp", "OBSTYPE", obstype, add+)
hedit ("sta2nsa_tmp", "PROCTYPE", "Stacked", add+)
hedit ("sta2nsa_tmp", "PROCTYPA", "StackOfStacks", add+)
hedit ("sta2nsa_tmp", "PRODTYPE", "image", add+)
hedit ("sta2nsa_tmp", "PLVER", plver, add+)
hedit ("sta2nsa_tmp", "PLDNAME", "dummy", add+)
hedit ("sta2nsa_tmp", "PLQUEUE", "dummy", add+)
hedit ("sta2nsa_tmp", "PLQNAME", "dummy", add+)
hedit ("sta2nsa_tmp", "PLPROCID", "dummy", add+)
hedit ("sta2nsa_tmp", "PLDNAME", pldname, add+)
hedit ("sta2nsa_tmp", "PLQUEUE", plqueue, add+)
hedit ("sta2nsa_tmp", "PLQNAME", plqname, add+)
hedit ("sta2nsa_tmp", "PLPROCID", plprocid, add+)
hedit ("sta2nsa_tmp", "PLFNAME", "dummy", add+)
hedit ("sta2nsa_tmp", "PLTILEID", tileid, add+)

# Figure out tiling. Make the number of tiles odd in x and y.
hselect (ref, "NAXIS1,NAXIS2", yes) | scan (naxis1, naxis2)
nc = (naxis1- 1) / stk_sztile + 1; nl = (naxis2 - 1) / stk_sztile + 1
if (mod(nc,2)==0)
    nc += 1
;
if (mod(nl,2)==0)
    nl += 1
;
n1 = (naxis1 + nc - 1) / nc; n2 = (naxis2 + nl - 1) / nl
hedit ("sta2nsa_tmp", "NEXTEND", nc*nl)

# Set the RA, DEC.
x = (naxis1 - 1) / 2. + 1; y = (naxis2 - 1) / 2. + 1
print (x, y) | wcsctran ("STDIN", "STDOUT", ref, "logical", "world",
    col="1 2", formats="%.2H %.1h", min=9, verb-) | scan (ra, dec)
hedit ("sta2nsa_tmp", "RA", ra)
hedit ("sta2nsa_tmp", "DEC", dec)

# Set output name.
hselect (ref, "OBSID", yes) | scan (s2)
s2 = substr (strupr(s2), 5, 19)
printf ("%s_%s_%s-%s-%s\n", plqueue, plqname, plprocid, subset, s2) |
    scan (out)

# Delete dregs from crash.
delete ("*_s[idwe].fits,*_si1.fits")

# Create the files.
iy = 0
for (l1 = 1; l1 < naxis2; l1+= n2) {
    iy += 1; ix = 0
    l2 = min (l1+n2-1, naxis2)
    for (c1 = 1; c1 < naxis1; c1+= n1) {
        ix += 1
	c2 = min (c1+n1-1, naxis1)
	printf ("tile%d%d\n", ix, iy) | scan (extname)
	printf ("[%d:%d,%d:%d]\n", c1, c2, l1, l2) | scan (tile)

	##### Flux file #####

	if (in != "") {
	    if (ix == 1 && iy == 1) {
		plfname = out // "_si"
		hedit ("sta2nsa_tmp", "FILENAME", plfname, add+)
		hedit ("sta2nsa_tmp", "PLFNAME", plfname, add+)
		mkheader (in, "sta2nsa_tmp", append-)

		out1 = plfname // ".fits"
		mkglbhdr ("sta2nsa_tmp", out1,
		    exclude="@"//nhppsargs.src//"sta2nsa_exclude.dat")
		print (out1//"\n"//out1//".fz", >> dataset.fdelete)
	    }

	    printf ("%s_si.fits[%s,append,inherit]\n", out, extname) | scan (out1)
	    imcopy (in//tile, out1, verb+)
	    printf ("%s_si.fits[%s]\n", out, extname) | scan (out1)
	    printf ("%s_sd[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "BPM", s2, add+)
	    printf ("%s_sw[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "WTMAP", s2, add+)
	    printf ("%s_se[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "EXPMAP", s2, add+)
	    hedit (out1, "FZALGOR", "RICE_1", add+)
	    hedit (out1, "FZQMETHD", "SUBTRACTIVE_DITHER_1", add+)
	    hedit (out1, "FZQVALUE", 4, add+)
	    hedit (out1, "FZDTHRSD", "CHECKSUM", add+)
	}
	;

	##### Flux file (zero subtracted) #####

	if (inz != "") {
	    if (ix == 1 && iy == 1) {
		plfname = out // "_si1"
		hedit ("sta2nsa_tmp", "PRODTYPE", "image1", add+)
		hedit ("sta2nsa_tmp", "FILENAME", plfname, add+)
		hedit ("sta2nsa_tmp", "PLFNAME", plfname, add+)
		mkheader (inz, "sta2nsa_tmp", append-)

		out1 = plfname // ".fits"
		mkglbhdr ("sta2nsa_tmp", out1,
		    exclude="@"//nhppsargs.src//"sta2nsa_exclude.dat")
		print (out1//"\n"//out1//".fz", >> dataset.fdelete)
	    }

	    printf ("%s_si1.fits[%s,append,inherit]\n", out, extname) | scan (out1)
	    imcopy (inz//tile, out1, verb+)
	    printf ("%s_si1.fits[%s]\n", out, extname) | scan (out1)
	    printf ("%s_sd[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "BPM", s2, add+)
	    printf ("%s_sw[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "WTMAP", s2, add+)
	    printf ("%s_se[%s]\n", out, extname) | scan (s2)
	    hedit (out1, "EXPMAP", s2, add+)
	    hedit (out1, "FZALGOR", "RICE_1", add+)
	    hedit (out1, "FZQMETHD", "SUBTRACTIVE_DITHER_1", add+)
	    hedit (out1, "FZQVALUE", 4, add+)
	    hedit (out1, "FZDTHRSD", "CHECKSUM", add+)
	}
	;

	##### DQ file #####

	if (ix == 1 && iy == 1) {
	    plfname = out // "_sd"
	    hedit ("sta2nsa_tmp", "FILENAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PLFNAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PRODTYPE", "dqmask", add+)
	    mkheader (bpm, "sta2nsa_tmp", append-)

	    out1 = plfname // ".fits"
	    mkglbhdr ("sta2nsa_tmp", out1,
		exclude="@"//nhppsargs.src//"sta2nsa_exclude.dat")
	    print (out1//"\n"//out1//".fz", >> dataset.fdelete)
	}

	printf ("%s_sd.fits[%s,append,inherit,type=mask]\n", out, extname) |
	    scan (out1)
	imcopy (bpm//tile, out1, verb+)
	printf ("%s_sd.fits[%s]\n", out, extname) | scan (out1)

	##### Weight file #####

	if (ix == 1 && iy == 1) {
	    plfname = out // "_sw"
	    hedit ("sta2nsa_tmp", "FILENAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PLFNAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PRODTYPE", "wtmap", add+)
	    mkheader (wt, "sta2nsa_tmp", append-)

	    out1 = plfname // ".fits"
	    mkglbhdr ("sta2nsa_tmp", out1,
		exclude="@"//nhppsargs.src//"sta2nsa_exclude.dat")
	    print (out1//"\n"//out1//".fz", >> dataset.fdelete)
	}

	printf ("%s_sw.fits[%s,append,inherit]\n", out, extname) |
	    scan (out1)
	printf ("%s -> %s\n", wt//tile, out1)
	imexpr ("a>0?1/a: 0", out1, wt//tile, verb-)
	printf ("%s_sw.fits[%s]\n", out, extname) | scan (out1)
	hedit (out1, "FZALGOR", "RICE_1", add+)
	hedit (out1, "FZQMETHD", "SUBTRACTIVE_DITHER_2", add+)
	hedit (out1, "FZQVALUE", 2, add+)
	hedit (out1, "FZDTHRSD", "CHECKSUM", add+)

	#### Exposure file #####

	if (ix == 1 && iy == 1) {
	    plfname = out // "_se"
	    hedit ("sta2nsa_tmp", "FILENAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PLFNAME", plfname, add+)
	    hedit ("sta2nsa_tmp", "PRODTYPE", "expmap", add+)
	    mkheader (exp, "sta2nsa_tmp", append-)

	    out1 = plfname // ".fits"
	    mkglbhdr ("sta2nsa_tmp", out1,
		exclude="@"//nhppsargs.src//"sta2nsa_exclude.dat")
	    print ("sta2nsa_tmp"//"\n"//out1//".fz", >> dataset.fdelete)
	}

	printf ("%s_se.fits[%s,append,inherit,type=mask]\n", out, extname) |
	    scan (out1)
	imcopy (exp//tile, out1, verb+)
	printf ("%s_se.fits[%s]\n", out, extname) | scan (out1)
	hedit (out1, "FZALGOR", "RICE_1", add+)
	hedit (out1, "FZQMETHD", "SUBTRACTIVE_DITHER_2", add+)
	hedit (out1, "FZQVALUE", 4, add+)
	hedit (out1, "FZDTHRSD", "CHECKSUM", add+)
    }
}
delete ("sta2nsa_tmp.fits")


##### JPG file #####

if (access(out//"_si.fits")) {
    out1 = out // "_sj"
    print (out1//".jpg", >> dataset.fdelete)
    imextensions (out//"_si", > "sta2nsa.tmp")
    touch (dataset.fprotect)
    mkgraphic ("@sta2nsa.tmp", out1, "world", "jpg", 32, "'abs(i1)>0.001'")

    i = nc / 2 + 1; j = nl / 2 + 1
    imextensions (out//"_si", extname="tile"//i//j) | scan (extname)
    hselect (extname, "NAXIS1,NAXIS2", yes) | scan (naxis1, naxis2)
    if (min(naxis1,naxis2) > 16000) {
	tmp1 = "sta2nsa_x2sj_tmp.fits"
	tmp2 = "sta2nsa_x2sj_bpm_tmp.pl"
	c1 = (naxis1-16000)/2+1; c2 = c1 + 16000 - 1
	l1 = (naxis2-16000)/2+1; l2 = l1 + 16000 - 1
	printf ("%s[%d:%d,%d:%d]\n", extname, c1, c2, l1, l2) | scan (tile)
	imcopy (tile, tmp1)
	print (tmp1, > "sta2nsa.tmp")
	hselect (tmp1, "BPM", yes) | scan (extname)
	printf ("%s[%d:%d,%d:%d]\n", extname, c1, c2, l1, l2) | scan (tile)
	imcopy (tile, tmp2)
	hedit (tmp1, "BPM", tmp2)
    } else
	print (extname, > "sta2nsa.tmp")
    ;

    out1 = out // "x2_sj"
    print (out1//".jpg", >> dataset.fdelete)
    touch (dataset.fprotect)
    mkgraphic ("@sta2nsa.tmp", out1, "world", "jpg", 2, "'abs(i1)>0.001'")
}
;

if (access(out//"_si1.fits")) {
    out1 = out // "_sj1"
    print (out1//".jpg", >> dataset.fdelete)
    imextensions (out//"_si1", > "sta2nsa.tmp")
    touch (dataset.fprotect)
    mkgraphic ("@sta2nsa.tmp", out1, "world", "jpg", 32, "'abs(i1)>0.001'")

    i = nc / 2 + 1; j = nl / 2 + 1
    imextensions (out//"_si1", extname="tile"//i//j) | scan (extname)
    hselect (extname, "NAXIS1,NAXIS2", yes) | scan (naxis1, naxis2)
    if (min(naxis1,naxis2) > 16000) {
	tmp1 = "sta2nsa_x2sj1_tmp.fits"
	tmp2 = "sta2nsa_x2sj1_bpm_tmp.pl"
	c1 = (naxis1-16000)/2+1; c2 = c1 + 16000 - 1
	l1 = (naxis2-16000)/2+1; l2 = l1 + 16000 - 1
	printf ("%s[%d:%d,%d:%d]\n", extname, c1, c2, l1, l2) | scan (tile)
	imcopy (tile, tmp1)
	print (tmp1, > "sta2nsa.tmp")
	hselect (tmp1, "BPM", yes) | scan (extname)
	printf ("%s[%d:%d,%d:%d]\n", extname, c1, c2, l1, l2) | scan (tile)
	imcopy (tile, tmp2)
	hedit (tmp1, "BPM", tmp2)
    } else
	print (extname, > "sta2nsa.tmp")
    ;

    out1 = out // "x2_sj1"
    print (out1//".jpg", >> dataset.fdelete)
    touch (dataset.fprotect)
    mkgraphic ("@sta2nsa.tmp", out1, "world", "jpg", 2, "'abs(i1)>0.001'")
}
;

##### HTML file #####

if (imaccess(out//"_si.fits"))
    ref = out // "_si.fits"
else
    ref = out // "_si1.fits"

imhead (ref//"[1]", l+, > out//"_sta.txt")
print (out//"_sta.txt", >> dataset.fdelete)
hselect (ref//"[1]", "DATE-OBS,$EXPTIME,$PROPID,$NCOMBINE,TITLE", yes) |
    scan (dateobs,exptime,propid,ncombine,title)
dateobs = substr (dateobs, 1, strldx(".",dateobs)-1)

if (access(out//"_sj.jpg") && access(out//"_sj1.jpg")) {
    printf ('<tr><td><a href="%s">%s</a><br>%d<br><br>%s<br>NCOMBINE %s\
        <br>%.20s<br>Alt Version\
	<br><a href="%s_sj.jpg">Low res</a>\
	<br><a href="%sx2_sj.jpg">High res</a>\
	<td><a href="%sx2_sj1.jpg"><img src="%s_sj1.jpg"></a></tr>\n',
	out//"_sta.txt", dateobs, exptime, propid, ncombine, title,
	out, out, out, out, > dataset.sname // ".html")
} else if (access(out//"_sj.jpg")) {
    printf ('<tr><td><a href="%s">%s</a><br>%d<br><br>%s<br>NCOMBINE %s<br>%.20s<td><a href="%sx2_sj.jpg"><img src="%s_sj.jpg"></a></tr>\n',
	out//"_sta.txt", dateobs, exptime, propid, ncombine, title, out, out,
	> dataset.sname // ".html")
} else {
    printf ('<tr><td><a href="%s">%s</a><br>%d<br><br>%s<br>NCOMBINE %s<br>%.20s<td><a href="%sx2_sj1.jpg"><img src="%s_sj1.jpg"></a></tr>\n',
	out//"_sta.txt", dateobs, exptime, propid, ncombine, title, out, out,
	> dataset.sname // ".html")
}

ln ("-s", html, "index.html")
print (html//"\nindex.html", >> dataset.fdelete)

##### Compress FITS files #####

files ("*_s[idwe]*.fits") | count | scan (i)
if (i > 0)
    fpack ("-v", "-D", "-Y","*_s[idwe]*.fits")
;

# Clean-up at the end.
if (in != "")
    print (in//".fits", >> ".stadone_delete.tmp")
;
if (inz != "")
    print (inz//".fits", >> ".stadone_delete.tmp")
;
print (wt//".fits", >> ".stadone_delete.tmp")
print (bpm//".pl", >> ".stadone_delete.tmp")
print (exp//".pl", >> ".stadone_delete.tmp")

## The following is a way to quickly stage stacks.
#if (strstr("DWF", dataset.name) > 0) {
#    !!dwf *fz
#    plexit HALT
#}
#;

plexit COMPLETED
