# This smooths the calibration files received from DES when they have
# the cell structure from the "Anne" method.

sections *.fits > list
imdel temp*
list = "list"
while (fscan (list, s1) != EOF) {
    print (s1)
    imsurfit (s1//"[1:1024,*]", "temp", 1, 4, func="spline3", cross-)
    imcopy ("temp", s1//"[1:1024,*]", verb-)
    imdelete temp
    imsurfit (s1//"[1025:2048,*]", "temp", 1, 4, func="spline3", cross-)
    imcopy ("temp", s1//"[1025:2048,*]", verb-)
    imdelete temp
}
list = ""
