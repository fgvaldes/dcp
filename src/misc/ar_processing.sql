-- Create indexed table of basic data.

CREATE TEMP TABLE raw AS
SELECT start_date, dtnsanam, dtpropid FROM voi.siap
WHERE instrument='DECam' AND proctype='raw'
AND obstype IN ('object', 'standard')
AND object NOT SIMILAR TO '%(focus|donut|junk|pointing)%'
AND dtpropid NOT SIMILAR TO '%(0001|9999|0005)%'
AND start_date BETWEEN TIMESTAMP '2015-07-01' AND TIMESTAMP '2015-07-31';

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT start_date, dtnsanam, dtpropid FROM voi.siap
WHERE instrument='DECam' and proctype='InstCal'
AND start_date BETWEEN TIMESTAMP '2015-07-01' and TIMESTAMP '2015-07-31';

CREATE INDEX procidx ON proc (dtnsanam);

-- Create count tables.

CREATE TEMP TABLE unprocessed AS
SELECT start_date, dtpropid, count(start_date) unprocessed FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
GROUP BY start_date, dtpropid;

CREATE TEMP TABLE processed AS
SELECT start_date, dtpropid, count(start_date) processed FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
GROUP BY start_date, dtpropid;

CREATE TEMP TABLE unprocessed_only AS
SELECT a.start_date, a.dtpropid, 0 processed, unprocessed FROM unprocessed a,
(SELECT start_date, dtpropid FROM unprocessed EXCEPT SELECT start_date, dtpropid FROM processed) b
WHERE a.start_date=b.start_date AND a.dtpropid=b.dtpropid;

CREATE TEMP TABLE processed_only AS
SELECT a.start_date, a.dtpropid, processed, 0 unprocessed FROM processed a,
(SELECT start_date, dtpropid FROM processed EXCEPT SELECT start_date, dtpropid FROM unprocessed) b
WHERE a.start_date=b.start_date AND a.dtpropid=b.dtpropid;

CREATE TEMP TABLE foo AS
SELECT a.start_date, a.dtpropid, processed, unprocessed
FROM processed a, unprocessed b
WHERE a.start_date=b.start_date AND a.dtpropid=b.dtpropid;

-- Create output.

SELECT * FROM processed_only UNION
SELECT * FROM unprocessed_only UNION
SELECT * FROM foo
ORDER BY start_date, dtpropid;
