#!/bin/env pipecl
#
# List pipeline parameters.

dpar cl > pldpar.tmp

list = "pldpar.tmp"
while (fscan (list, line) != EOF) {
    if (fscan (line, s1) == EOF)
        break
    ;
    if (s1 == "cl.dpkg")
        break
    ;
}

while (fscan (list, line) != EOF) {
    if (line == "# EOF")
        break
    ;
    print (substr(line,4,999))
}
list = ""

delete ("pldpar.tmp")

logout
