-- Create indexed table of basic data.

CREATE TEMP TABLE raw AS
SELECT date(start_date) date, substring(dtnsanam from 1 for 17) dtnsanam, dtpropid FROM voi.siap
WHERE instrument='DECam' AND proctype='raw'
AND obstype IN ('object', 'standard')
AND object NOT SIMILAR TO '%(focus|donut|junk|pointing)%'
AND start_date BETWEEN TIMESTAMP '20171001' AND TIMESTAMP '20171011';

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT date(start_date) date, substring(dtnsanam from 1 for 17) dtnsanam, dtpropid FROM voi.siap
WHERE instrument='DECam' and proctype='InstCal'
AND start_date BETWEEN TIMESTAMP '20171001' and TIMESTAMP '20171011';

CREATE INDEX procidx ON proc (dtnsanam);

CREATE TEMP TABLE r AS
SELECT date, dtpropid, dtnsanam FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a;

CREATE TEMP TABLE p AS
SELECT date, dtpropid, dtnsanam FROM
(SELECT * FROM proc EXCEPT SELECT * FROM raw) a;

-- Create output.

SELECT distinct r.date, r.dtpropid raw, p.dtpropid proc, r.dtnsanam
FROM r, p
WHERE r.dtnsanam = p.dtnsanam
ORDER by r.date, r.dtpropid, r.dtnsanam;

SELECT r.dtpropid raw, p.dtpropid proc, count(*) N
FROM r, p
WHERE r.dtnsanam = p.dtnsanam
GROUP by raw, proc;

