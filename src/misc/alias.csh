# Valdes
alias ssh	"ssh -Y \!:*;tb"
alias ed	vi
alias dir	ls
alias del	rm
alias type	cat
alias k		"kill -9"
alias show	printenv
alias back	'cd -; pwd'
alias pd	'pushd'
alias bc	"bc -q"
#alias wc	"wc -l"
alias wc	"wc -l \!:* | grep -v total"
alias lrt	"ls -rt \!:* | tail"
alias lrtl	"ls -rtl \!:* | tail"
alias dirs	"dirs -v"
alias pltop	"top -d 10 -u $USER"
#alias pltop	"top -d 10 -u $USER | egrep -v '( python|root| ecl.e| sh| tcsh| sshd| top| egrep | java|[ _]elf| time| sleep| tee|condor)'"
alias bc	"bc -lq"
alias =		'echo \!:* | bc'


alias iqsdb	psql
alias busy	"condor_status | grep Busy | tr '@.' '\t' | cut -f 2,2 | uniq -c"
alias idle	"condor_status|grep Idle | tr '@' '\t'|cut -f 2"

alias arstat	"arstatus \!:* | grep -v ' "'0$'"'"
alias arstat1	"arstatus \!:* | grep -v ' "'0'"'"
alias subclean	"(cd Submitted; submitc \!:* )"
alias fields "sed 's+[ \t][ \t]*+\t+g;s+^\t++' | cut -f \!:*"
alias ckds	"cat $NHPPS_DATAAPP/DCP_TOP/*/topgroup_o.tmp"
alias sm	"tb $NHPPS_SYS_NAME Monitor;simplemon -l top,fil,dts"
alias smfil	"tb $NHPPS_SYS_NAME Monitor;simplemon -l fil"
alias smstk	"tb $NHPPS_SYS_NAME Monitor;simplemon -l fil,stk"
alias smcal	"tb $NHPPS_SYS_NAME Monitor;simplemon -l top,zmi,zsi,fmi,fsi,xtk -p 'zsi|fsi|xtk'"
alias cstop	"bsudo /etc/rc.d/init.d/condor stop"
alias crestart	"bsudo /etc/rc.d/init.d/condor stop; sleep 10; bsudo /etc/rc.d/init.d/condor start"
alias nsafetch  "curl -ks -E $NHPPS_PIPEAPPSRC/dcp/nsa.pem:'Mosaic DHS' 'https://voserver:7503?fileRef=\!:1' > \!:1"
alias wget	"wget --no-check-certificate"
alias   plrsync '/usr/bin/rsync -vauzP --exclude-from=$HOME/SharedHome/rsync.exclude \!* | egrep -v /$'
alias ckingest	"grep Created \!:1  | cut -d ' ' -f 7 ; grep Processing \!:1 | wc"
alias   plrsync '/usr/bin/rsync -vauzP --exclude-from=$HOME/SharedHome/rsync.exclude \!* | egrep -v /$'
alias plcondorrm "plssh -svn $SUBMITNODES condor_rm -all"
#alias	procid2date 'date -d @`echo \'ibase=16; \!:1 * A\' | tr \'[:lower:]\' \'[:upper:]\' | bc`'
alias fitsverify '/net/pipe1/cfitsio/bin64/fitsverify'
alias fornodes "foreach node (`echo $PROCNODES|tr ',' ' '`)"
alias bsudo 'echo b3rl1n12 | sudo -S -p "" \!:*'
alias ds9imtool 'ds9 -view info no -view panner no -view magnifier no -view buttons no -view colorbar no'
alias tv '(setenv iraf /iraf/tv/; pipecl)'
alias cleanonly 'plrun --cleanonly > /dev/null'
alias clean 'submit -c'
alias lpr 'lpq'
alias display 'echo Not in CL: display'
alias cl 'echo Use tv: cl'
alias imdisplay '/usr/bin/display'
alias   tty     'ps x | grep tty | sed "s+^ *++" | cut -d " " -f 2'
alias   tb       'printf "\033]0;\!:* $USER `hostname -s`:`tty`\007"'
#alias   tb      'printf "\033]0;\!:1\007"'
alias plconfig 'vi $PipeConfig/ConfigMap.list'
alias plqueue 'vi $PipeConfig/\!:1next.*dat $PipeConfig/\!:1.*dat'
alias plq 'plqueue ${Q}'
alias cdq 'cd ${Q}/\!*'
alias plver 'vi $plroot/Versions'
alias su 'ssh \!:1@$HOST'
alias plstat '(unsetenv NHPPS_NODE_DM;setenv PROCNODES \!:1; plstatus)'
alias arhdr 'archivepath -v'

#alias mss 'mssck -y 2016 -m 08 -q DEC16B -d 20160801'
alias ckfail '(cd \!:1/red; find */runtime/*tjobs -iname "*fail*")'

alias filclean 'rm `cat .fildone_delete.tmp`'
alias decals 'archivestage -d schlegeld -s decam-data@desi.lbl.gov'
alias dtstat 'decam_dtstat'
alias ckcpfail 'find */runtime -name JOBS_FAILED.txt'
alias nersc 'echo "D3SI@NERSC______ (______=DUO authentication)"; ssh fvaldes@dtn01.nersc.gov'
alias ckps 'plssh -svn $PROCNODES ps xh | grep -v "[cs]sh" | grep -v python | grep -v grep | grep -v ps'
alias retrig 'trig -r'
alias sck '(cd \!:1; echo; stbcount *dts; echo; grep MST submitblocks.log | tail -n 1; echo)'
alias sck1 '(cd \!:1; echo; stbcount *dts; echo; grep MST ../submitblocks.log | tail -n 1; echo)'
alias sck2 '(cd \!:1; echo; stbcount */*dts; echo; grep MST submitblocks.log | tail -n 1; echo)'
alias s '(cd 19A; echo; stbcount PH*/*dts; echo; grep MST submitblocks.log | tail -n 1; echo)'
alias psg 'ps x | grep -E \!:1 | grep -v grep | cut -c 1-6,28-101'
alias psgw 'ps xw | grep -E \!:1 | grep -v grep | cut -c 1-6,28-'
alias psga 'ps x | grep -E \!:1 | grep -v grep | cut -c 1-7,28-100'
alias psgaw 'ps xw | grep -E \!:1 | grep -v grep | cut -c 7-6,28-'
alias viw 'vi `which \!:1`'
alias aliasg 'alias | grep -E \!:1'
alias grp '(cd LUNA;cat "\!:1"*"\!:2"/*dts/*[AD]_grp.cat | grep -v "#" | sort -n -k 6)'
alias fake '(cd LUNA;cat "\!:1"*/*dts/*F_grp.cat | grep -v "#" | sort -n -k 6)'
alias newgrp '(cd LUNA;ls */*dts/*[AD]_grp.cat|sort > tmp;comm -2 -3 tmp dorev.listAD > dorev.list; cat dorev.list dorev.listAD > tmp; sort tmp > dorev.listAD; wc dorev.list)'
alias dotiles1 'dotiles.sql \!:* > dotiles.tmp; dotiles dotiles.tmp; rm dotiles.tmp'
alias movaliases 'source $NHPPS_PIPEAPPSRC/plapp/mov/aliases \!:*'
alias rsp2stk "egrep '^      [0-9][0-9]* [0-9]'"
alias arstatreset "(cd /tmp; arstatus > arstatus1.tmp; diff -e arstatus.tmp arstatus1.tmp | egrep '^ ' | sort ; mv arstatus1.tmp arstatus.tmp)"
alias arstatdiff "(cd /tmp; arstatus > arstatus1.tmp; diff -e arstatus.tmp arstatus1.tmp | egrep '^ ' | sort)"
alias tf 'tail -s 10 -f'
alias thist 'history | tail'
alias rspstk '(cd FIL;cat */*stk.tmp | grep -v " 1 ")'

# For DES distribution.
alias umountOPS 'bsudo umount $Archive/OPS'
alias mountOPS1 'bsudo mount -t nfs -r dldev1:/data/archive_data/Archive/OPS $Archive/OPS1'
alias umountOPS1 'bsudo umount $Archive/OPS1'
alias mountOPS2 'bsudo mount -t nfs -r dldev2:/data/archive_data/Archive/OPS $Archive/OPS2'
alias umountOPS2 'bsudo umount $Archive/OPS2'
alias destrig 'ls *tbd | tail -n 15 > tmp;trig -n $DECNODES12 `cat tmp`;rm tmp'
# Herrera 
alias hg        "history | grep \!:1" 
alias mw        'more `which  \!:1`' 

alias x2a	'printf "%s0a" \!:1 | cut -c 3- | xxd -r -p'
alias a2x	'printf "\\x%s\n" `printf \!:1 | xxd -p`'
