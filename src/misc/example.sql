\pset footer
-- Create indexed table of basic data.


CREATE TEMP TABLE raw AS
SELECT date(start_date) date, substring(dtnsanam from 1 for 17) dtnsanam, dtpropid, last_name
FROM voi.siap voi, viewspace.principal_investigator pi
WHERE instrument='DECam' AND proctype='raw'  
AND filesize > 150000000
AND obstype IN ('object', 'standard')
AND object NOT SIMILAR TO '%(focus|donut|junk|AMAP|PMAP|juck)%'
AND (object NOT SIMILAR TO '%pointing%' OR dtpropid = '2019B-1009')
AND dtpropid NOT SIMILAR TO '%(0005)%'
AND filter NOT SIMILAR TO '%solid%'
AND start_date BETWEEN TIMESTAMP '20191201' AND TIMESTAMP '20191210'
AND voi.dtpropid=pi.proposal
AND NOT (start_date IN ('2018-01-03', '2018-01-07', '2018-12-18') AND prop_id='2013A-9999')
AND dtnsanam NOT IN (

-- VSUB=F
'c4d_171214_082213_ori.fits.fz',
'c4d_180312_035350_ori.fits.fz',
-- (nextend=34)
'c4d_171225_010306_ori.fits.fz',
-- (nextend=52)
'c4d_171225_010404_ori.fits.fz',
'c4d_171225_010539_ori.fits.fz',
'c4d_180215_011240_ori.fits.fz',
-- (nextend=52)
'c4d_180428_041113_ori.fits.fz',
-- VSUB=F
'c4d_180520_224118_ori.fits.fz',
-- Ignore list
'c4d_180727_051659_ori.fits.fz',
'c4d_180727_052227_ori.fits.fz',
'c4d_180731_051026_ori.fits.fz',
'c4d_180731_051802_ori.fits.fz',
'c4d_180731_052148_ori.fits.fz',
'c4d_180731_060942_ori.fits.fz',
'c4d_180731_061329_ori.fits.fz',
'c4d_180731_061716_ori.fits.fz',
'c4d_180731_062105_ori.fits.fz',
'c4d_180731_062450_ori.fits.fz',
'c4d_180731_062836_ori.fits.fz',
'c4d_180731_073009_ori.fits.fz',
'c4d_180731_073338_ori.fits.fz',
'c4d_180731_073708_ori.fits.fz',
'c4d_180731_074037_ori.fits.fz',
'c4d_180731_074407_ori.fits.fz',
'c4d_180731_074734_ori.fits.fz',
'c4d_180916_234658_ori.fits.fz',
'c4d_190219_002644_ori.fits.fz',
'c4d_190219_002848_ori.fits.fz',
'c4d_190219_003051_ori.fits.fz',
'c4d_190219_003838_ori.fits.fz',
'c4d_190219_004305_ori.fits.fz',
'c4d_190219_004504_ori.fits.fz',
'c4d_190219_004704_ori.fits.fz',
'c4d_190219_005203_ori.fits.fz',
'c4d_190219_005534_pri.fits.fz',
'c4d_190219_005617_pri.fits.fz',
'c4d_190219_005659_pri.fits.fz',
'c4d_190219_005743_pri.fits.fz',
'c4d_190219_005825_pri.fits.fz',
'c4d_190219_010738_ori.fits.fz',
'c4d_190219_011925_ori.fits.fz',
'c4d_190219_012201_ori.fits.fz',
'c4d_190219_012401_ori.fits.fz',
'c4d_190219_012600_ori.fits.fz',
'c4d_190219_012818_ori.fits.fz',
'c4d_190219_015018_ori.fits.fz',
'c4d_190219_015219_ori.fits.fz',
'c4d_190219_015819_ori.fits.fz',
'c4d_190219_020656_ori.fits.fz',
'c4d_190219_020949_ori.fits.fz',
'c4d_190315_051551_ori.fits.fz',
'c4d_190315_081347_ori.fits.fz',
'c4d_190630_061201_ori.fits.fz',
'c4d_190630_095053_ori.fits.fz',
-- (nextend=52)
'c4d_190326_035753_ori.fits.fz',
'c4d_190601_101656_ori.fits.fz',
'c4d_190601_101855_ori.fits.fz',
'c4d_190601_102056_ori.fits.fz',
'c4d_190601_102254_ori.fits.fz',
'c4d_190601_102452_ori.fits.fz',
'c4d_190601_102651_ori.fits.fz',
'c4d_190601_103128_pri.fits.fz',
'c4d_190601_103212_pri.fits.fz',
-- CPignore
'c4d_191023_031651_ori.fits.fz',
'c4d_191023_031035_ori.fits.fz',
'c4d_191023_031343_ori.fits.fz',
'c4d_191024_002658_ori.fits.fz',
'c4d_191024_001112_ori.fits.fz',
'c4d_191024_045513_ori.fits.fz',
'c4d_191024_001730_ori.fits.fz',
'c4d_191024_000452_ori.fits.fz',
'c4d_191024_002040_ori.fits.fz',
'c4d_191024_001421_ori.fits.fz',
'c4d_191024_044855_ori.fits.fz',
'c4d_191024_045204_ori.fits.fz',
'c4d_191024_045824_ori.fits.fz',
'c4d_191024_002350_ori.fits.fz',
'c4d_191024_000802_ori.fits.fz',
'c4d_191024_003006_ori.fits.fz',
-- corrupted in mass store
'c4d_141022_003354_ori.fits.tx'

)
;

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT date(start_date) date, substring(dtnsanam from 1 for 17) dtnsanam, dtpropid, last_name
FROM voi.siap voi, viewspace.principal_investigator pi
WHERE instrument='DECam' and proctype='InstCal' and prodtype='image'
 
AND voi.dtpropid=pi.proposal
AND start_date BETWEEN TIMESTAMP '20191201' and TIMESTAMP '20191210';

CREATE INDEX procidx ON proc (dtnsanam);

-- Create count tables.

CREATE TEMP TABLE unprocessed AS
SELECT date, dtpropid, last_name, count(date) unprocessed FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
GROUP BY date, dtpropid, last_name;

CREATE TEMP TABLE processed AS
SELECT date, dtpropid, last_name, count(date) processed FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
GROUP BY date, dtpropid, last_name;

CREATE TEMP TABLE unprocessed_only AS
SELECT a.date, a.dtpropid, a.last_name, 0 processed, unprocessed FROM unprocessed a,
(SELECT date, dtpropid, last_name FROM unprocessed EXCEPT SELECT date, dtpropid, last_name FROM processed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.last_name=b.last_name;

CREATE TEMP TABLE processed_only AS
SELECT a.date, a.dtpropid, a.last_name, processed, 0 unprocessed FROM processed a,
(SELECT date, dtpropid, last_name FROM processed EXCEPT SELECT date, dtpropid, last_name FROM unprocessed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.last_name=b.last_name;

CREATE TEMP TABLE foo AS
SELECT a.date, a.dtpropid, a.last_name, processed, unprocessed
FROM processed a, unprocessed b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.last_name=b.last_name;

-- Create output.

SELECT * FROM processed_only UNION
SELECT * FROM unprocessed_only UNION
SELECT * FROM foo
ORDER BY date, dtpropid, last_name ;
