--
-- Initialize database.
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: archive_sites; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE archive_sites (
    location_id character varying(8) NOT NULL,
    location_name character varying(100),
    archive_host character varying(60),
    archive_root character varying(100),
    site_name character varying(32),
    site_id smallint NOT NULL
);



--
-- Name: block; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE block (
    block_id integer NOT NULL,
    block_name character varying(255),
    module_list character varying(1000),
    target_node character varying(255),
    target_release character varying(32),
    target_baseversions text,
    target_env text,
    target_build text,
    target_install text,
    target_svn_revision character varying(16),
    run_id integer,
    start_jobid integer,
    end_jobid integer,
    block_submit timestamp without time zone,
    block_start timestamp without time zone,
    block_end timestamp without time zone,
    status smallint,
    submit_node character varying(255),
    submit_release character varying(32),
    submit_baseversions text,
    submit_env text,
    submit_build text,
    submit_install text,
    submit_svn_revision character varying(16),
    pfwtask_list character varying(2000),
    id integer,
    name character varying(255),
    submit_id integer,
    submit_time timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    dorder smallint,
    orchtask_list character varying(2000),
    active_orchtask_id integer,
    active_orchtask_name character varying(255)
);



--
-- Name: block_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE block_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: block_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('block_seq', 1, true);


--
-- Name: catalog; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE catalog (
    id integer NOT NULL,
    run character varying(100),
    nite character varying(20),
    band character varying(20),
    tilename character varying(40),
    catalogname character varying(100),
    ccd smallint,
    catalogtype character varying(20),
    parentid integer,
    exposureid integer,
    objects integer,
    project character varying(20)
);



--
-- Name: coadd; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE coadd (
    id bigint NOT NULL,
    run character varying(100),
    band character varying(20),
    tilename character varying(40),
    imagename character varying(100),
    radecequin real,
    htmid bigint,
    cx double precision,
    cy double precision,
    cz double precision,
    wcstype character varying(80),
    wcsdim smallint,
    equinox real,
    ctype1 character varying(20),
    ctype2 character varying(20),
    cunit1 character varying(20),
    cunit2 character varying(20),
    crval1 double precision,
    crval2 double precision,
    crpix1 numeric(10,4),
    crpix2 numeric(10,4),
    cd1_1 double precision,
    cd2_1 double precision,
    cd1_2 double precision,
    cd2_2 double precision,
    naxis1 integer,
    naxis2 integer,
    nextend smallint,
    fwhm real,
    ellipticity real,
    project character varying(20),
    ra numeric(8,5),
    "dec" numeric(8,5),
    coadd_type character varying(20),
    combinet character varying(10),
    gain real,
    saturate real,
    exptime real,
    sexmgzpt real
);



--
-- Name: coadd_src; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE coadd_src (
    coadd_imageid bigint NOT NULL,
    src_imageid bigint NOT NULL,
    magzp real,
    magzp_err real,
    weight_scaling real
);



--
-- Name: coaddtile; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE coaddtile (
    coaddtile_id integer NOT NULL,
    project character varying(25),
    tilename character varying(40),
    ra double precision,
    "dec" double precision,
    equinox double precision,
    pixelsize double precision,
    npix_ra integer,
    npix_dec integer
);



--
-- Name: coaddtile_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE coaddtile_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: coaddtile_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('coaddtile_seq', 1, false);


--
-- Name: desjob; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE desjob (
    id integer NOT NULL,
    name character varying(255),
    tjob_id integer,
    block_id integer,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    wall_time integer,
    status integer,
    status4 integer,
    status5 integer,
    pubnotes character varying(4000),
    privnotes character varying(4000),
    redo integer
);



--
-- Name: desjob_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE desjob_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: desjob_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('desjob_seq', 1, true);


--
-- Name: exposure; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE exposure (
    id integer NOT NULL,
    nite character varying(80),
    band character varying(68),
    exposurename character varying(100),
    exposuretype character varying(80),
    telradec character varying(80),
    telequin double precision,
    htmid bigint,
    cx double precision,
    cy double precision,
    cz double precision,
    ha character varying(80),
    zd double precision,
    airmass double precision,
    telfocus double precision,
    object character varying(80),
    observer character varying(80),
    propid character varying(80),
    detector character varying(80),
    detsize character varying(40),
    telescope character varying(80),
    observatory character varying(80),
    latitude character varying(80),
    longitude character varying(80),
    altitude character varying(80),
    timesys character varying(80),
    date_obs character varying(80),
    time_obs character varying(80),
    mjd_obs double precision,
    exptime double precision,
    expreq double precision,
    darktime double precision,
    epoch double precision,
    windspd character varying(80),
    winddir character varying(80),
    ambtemp character varying(80),
    humidity character varying(80),
    pressure character varying(80),
    skyvar double precision,
    fluxvar double precision,
    dimmsee double precision,
    photflag smallint,
    imagehwv character varying(80),
    imageswv character varying(80),
    naxis1 integer,
    naxis2 integer,
    nextend smallint,
    project character varying(80),
    telra double precision,
    teldec double precision,
    obstype character varying(80),
    expnum integer,
    proctype character varying(80),
    prodtype character varying(80),
    proposer character varying(80),
    instrument character varying(80),
    pixscal1 double precision,
    pixscal2 double precision,
    camshut character varying(80),
    radesys character varying(80),
    filtid character varying(80),
    filpos smallint,
    telstat character varying(80),
    domestat character varying(80),
    domeshut character varying(80),
    domelamp character varying(80),
    moonangle double precision,
    exp_exclude smallint
);



--
-- Name: exposure_qa; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE exposure_qa (
    id integer NOT NULL,
    run character varying(100),
    nite character varying(20),
    project character varying(20),
    astromndets_reference integer,
    astromndets_reference_highsn integer,
    astromcorr_reference double precision,
    astromcorr_reference_highsn double precision,
    astromchi2_reference double precision,
    astromchi2_reference_highsn double precision,
    astromsigma_reference_1 double precision,
    astromsigma_reference_2 double precision,
    astromsigma_reference_highsn_1 double precision,
    astromsigma_reference_highsn_2 double precision,
    astromoffset_reference_1 double precision,
    astromoffset_reference_2 double precision,
    astromoffset_reference_highsn_1 double precision,
    astromoffset_reference_highsn_2 double precision,
    exposureid integer
);



--
-- Name: exposure_qa_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE exposure_qa_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: exposure_qa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: deccp
--

ALTER SEQUENCE exposure_qa_seq OWNED BY exposure_qa.id;


--
-- Name: exposure_qa_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('exposure_qa_seq', 1, true);


--
-- Name: image; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE image (
    id integer NOT NULL,
    run character varying(100),
    nite character varying(20),
    band character varying(20),
    tilename character varying(40),
    imagename character varying(100),
    ccd smallint,
    imagetype character varying(20),
    project character varying(20),
    exposureid integer,
    parentid integer,
    airmass double precision,
    exptime double precision,
    device_id integer,
    gaina double precision,
    rdnoisea double precision,
    gainb double precision,
    rdnoiseb double precision,
    equinox double precision,
    htmid bigint,
    cx double precision,
    cy double precision,
    cz double precision,
    wcsdim smallint,
    ctype1 character varying(10),
    cunit1 character varying(20),
    crval1 double precision,
    crpix1 double precision,
    cd1_1 double precision,
    cd1_2 double precision,
    pv1_1 double precision,
    pv1_2 double precision,
    pv1_3 double precision,
    pv1_4 double precision,
    pv1_5 double precision,
    pv1_6 double precision,
    pv1_7 double precision,
    pv1_8 double precision,
    pv1_9 double precision,
    pv1_10 double precision,
    ctype2 character varying(10),
    cunit2 character varying(20),
    crval2 double precision,
    crpix2 double precision,
    cd2_1 double precision,
    cd2_2 double precision,
    pv2_1 double precision,
    pv2_2 double precision,
    pv2_3 double precision,
    pv2_4 double precision,
    pv2_5 double precision,
    pv2_6 double precision,
    pv2_7 double precision,
    pv2_8 double precision,
    pv2_9 double precision,
    pv2_10 double precision,
    skybrite double precision,
    skysigma double precision,
    elliptic double precision,
    fwhm double precision,
    scampnum integer,
    scampchi double precision,
    scampflg smallint,
    pv1_0 double precision,
    pv2_0 double precision,
    naxis1 integer,
    naxis2 integer,
    nextend smallint,
    ra double precision,
    "dec" double precision,
    pcount integer,
    gcount smallint,
    bzero double precision,
    bscale double precision,
    bunit character varying(10),
    ccdname character varying(40),
    detsec character varying(30),
    ccdsum character varying(10),
    datasec character varying(30),
    trimsec character varying(30),
    ampseca character varying(30),
    biasseca character varying(30),
    saturata double precision,
    saturatb double precision,
    ltm1_1 double precision,
    ltm2_2 double precision,
    ltv1 double precision,
    ltv2 double precision,
    radesys character varying(20),
    wcsaxes smallint,
    ampsecb character varying(30),
    biassecb character varying(30),
    zp double precision,
    zero double precision,
    sky double precision,
    seesiga double precision,
    nsatpix bigint,
    nobject_scamp integer
);



--
-- Name: location; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE location (
    id integer NOT NULL,
    fileclass character varying(20),
    filetype character varying(20),
    filename character varying(200),
    filedate timestamp without time zone,
    filesize bigint,
    run character varying(100),
    nite character varying(20),
    band character varying(68),
    tilename character varying(40),
    exposurename character varying(100),
    ccd smallint,
    project character varying(20),
    archivesites character varying(30) DEFAULT 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN'::character varying,
    filesize_gz bigint,
    filesize_fz bigint,
    dr integer,
    checksum character varying(64),
    checksum_fz character varying(64),
    checksum_gz character varying(64)
);



--
-- Name: location_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE location_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: location_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('location_seq', 1, true);


--
-- Name: objects_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE objects_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: objects_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('objects_seq', 1, false);


--
-- Name: objects_template; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE objects_template (
    object_id bigint NOT NULL,
    ra double precision,
    "dec" double precision,
    zeropoint real,
    mag_auto real,
    magerr_auto real,
    mag_aper_8 real,
    magerr_aper_8 real,
    x_image real,
    y_image real,
    flags smallint,
    errx2_world double precision,
    erry2_world double precision,
    class_star real,
    imageid integer,
    catalogid integer,
    alpha_j2000 double precision,
    delta_j2000 double precision,
    band character varying(20)
);



--
-- Name: orchtask; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE orchtask (
    id integer NOT NULL,
    name character varying(255),
    block_id integer,
    orch_id integer,
    submit_time timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    wall_time integer,
    status integer,
    pubnotes character varying(4000),
    privnotes character varying(4000),
    provenance character varying(4000),
    redo integer,
    dorder smallint
);



--
-- Name: orchtask_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE orchtask_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: orchtask_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('orchtask_seq', 1, true);


--
-- Name: run; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE run (
    run character varying(255) NOT NULL,
    project character varying(255) NOT NULL,
    operator character varying(255) NOT NULL,
    submit_config_text text NOT NULL,
    all_config_text text NOT NULL,
    archive_node character varying(255) NOT NULL,
    event_tag character varying(255),
    submit_host character varying(255) NOT NULL,
    submit_dir character varying(255) NOT NULL,
    block_list character varying(1000) NOT NULL,
    notes character varying(1000),
    submit_release character varying(32),
    submit_baseversions text,
    submit_env text,
    submit_build text,
    submit_install text,
    submit_svn_revision character varying(16),
    id integer NOT NULL,
    submit_node character varying(255),
    run_submit timestamp without time zone,
    run_start timestamp without time zone,
    run_end timestamp without time zone,
    status smallint,
    run_comment character varying(255),
    fileclass character varying(20),
    orch_id integer,
    name character varying(255),
    run_tag character varying(255),
    pubnotes character varying(4000),
    privnotes character varying(4000),
    datarelease character varying(255),
    numblocks smallint,
    active_block_id integer,
    active_block_name character varying(255),
    submit_time timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone
);



--
-- Name: run_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE run_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: run_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('run_seq', 1, true);


--
-- Name: sites; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE sites (
    site_name character varying(32) NOT NULL,
    site_id smallint NOT NULL,
    login_host character varying(255),
    login_gsissh_port character varying(8),
    grid_host character varying(255),
    grid_port character varying(8),
    grid_type character varying(8),
    gridftp_host character varying(255),
    gridftp_port character varying(8),
    compute_ppn smallint,
    batch_type character varying(16),
    is_archive smallint
);



--
-- Name: sites_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE sites_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: sites_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('sites_seq', 1, false);


--
-- Name: tjob; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE tjob (
    id integer NOT NULL,
    name character varying(255),
    block_id integer,
    orchtask_id integer,
    orch_id integer,
    submit_time timestamp without time zone,
    target_submit_time timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    firstid_desjob character varying(6),
    lastid_desjob character varying(6),
    exec_host character varying(255),
    target_jobid character varying(255),
    queue_wait_time integer,
    wall_time integer,
    su real,
    status integer,
    pubnotes character varying(4000),
    privnotes character varying(4000),
    provenance character varying(4000)
);



--
-- Name: tjob_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE tjob_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;



--
-- Name: tjob_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('tjob_seq', 1, true);


--
-- Name: zeropoint; Type: TABLE; Schema: public; Owner: deccp; Tablespace: 
--

CREATE TABLE zeropoint (
    id bigint NOT NULL,
    imageid bigint,
    insert_date timestamp without time zone,
    mag_zero real,
    sigma_mag_zero real,
    originid bigint,
    source character varying(10),
    tilename character varying(40)
);



--
-- Name: zeropoint_seq; Type: SEQUENCE; Schema: public; Owner: deccp
--

CREATE SEQUENCE zeropoint_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: zeropoint_seq; Type: SEQUENCE SET; Schema: public; Owner: deccp
--

SELECT pg_catalog.setval('zeropoint_seq', 1, true);


--
-- Data for Name: archive_sites; Type: TABLE DATA; Schema: public; Owner: deccp
--

COPY archive_sites (location_id, location_name, archive_host, archive_root, site_name, site_id) FROM stdin;
1	dec01	dec01.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec01	1
2	dec02	dec02.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec02	2
3	dec03	dec03.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec03	3
4	dec04	dec04.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec04	4
5	dec05	dec05.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec05	5
6	dec06	dec06.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec06	6
7	dec07	dec07.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec07	7
8	dec08	dec08.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec08	8
9	dec09	dec09.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec09	9
10	dec10	dec10.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec10	10
11	dec11	dec11.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec11	11
12	dec12	dec12.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec12	12
13	dec13	dec13.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec13	13
14	dec14	dec14.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec14	14
15	dec15	dec15.tuc.noao.edu	/data/sharedfs/deccp/Archive	dec15	15
16	dec16	dec16.sdm.noao.edu	/data/sharedfs/deccp/Archive	dec16	16
17	dec17	dec17.sdm.noao.edu	/data/sharedfs/deccp/Archive	dec17	17
18	dec18	dec18.sdm.noao.edu	/data/sharedfs/deccp/Archive	dec18	18
\.

--
-- Data for Name: sites; Type: TABLE DATA; Schema: public; Owner: deccp
--

COPY sites (site_name, site_id, login_host, login_gsissh_port, grid_host, grid_port, grid_type, gridftp_host, gridftp_port, compute_ppn, batch_type, is_archive) FROM stdin;
dec01	1	dec01.tuc.noao.edu	22	dec01.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec02	2	dec02.tuc.noao.edu	22	dec02.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec03	3	dec03.tuc.noao.edu	22	dec03.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec04	4	dec04.tuc.noao.edu	22	dec04.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec05	5	dec05.tuc.noao.edu	22	dec05.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec06	6	dec06.tuc.noao.edu	22	dec06.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec07	7	dec07.tuc.noao.edu	22	dec07.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec08	8	dec08.tuc.noao.edu	22	dec08.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec09	9	dec09.tuc.noao.edu	22	dec09.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec10	10	dec10.tuc.noao.edu	22	dec10.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec11	11	dec11.tuc.noao.edu	22	dec11.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec12	12	dec12.tuc.noao.edu	22	dec12.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec13	13	dec13.tuc.noao.edu	22	dec13.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec14	14	dec14.tuc.noao.edu	22	dec14.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec15	15	dec15.tuc.noao.edu	22	dec15.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec16	16	dec16.sdm.noao.edu	22	dec16.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec17	17	dec17.sdm.noao.edu	22	dec17.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
dec18	18	dec18.sdm.noao.edu	22	dec18.tuc.noao.edu	\N	condor	\N	\N	\N	dynslotscondor	\N
\.

--
-- Name: catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY catalog
    ADD CONSTRAINT catalog_pkey PRIMARY KEY (id);


--
-- Name: coaddtile_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY coaddtile
    ADD CONSTRAINT coaddtile_pkey PRIMARY KEY (coaddtile_id);


--
-- Name: desjob_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY desjob
    ADD CONSTRAINT desjob_pkey PRIMARY KEY (id);


--
-- Name: exposure_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY exposure
    ADD CONSTRAINT exposure_pkey PRIMARY KEY (id);


--
-- Name: exposure_qa_id_key; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY exposure_qa
    ADD CONSTRAINT exposure_qa_id_key UNIQUE (id);


--
-- Name: image_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (id);


--
-- Name: location_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: orchtask_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY orchtask
    ADD CONSTRAINT orchtask_pkey PRIMARY KEY (id);


--
-- Name: run_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY run
    ADD CONSTRAINT run_pkey PRIMARY KEY (id);


--
-- Name: tjob_pkey; Type: CONSTRAINT; Schema: public; Owner: deccp; Tablespace: 
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT tjob_pkey PRIMARY KEY (id);


--
-- Name: block_name_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX block_name_idx ON block USING btree (name);


--
-- Name: block_runid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX block_runid_idx ON block USING btree (run_id);


--
-- Name: catalog_nite_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX catalog_nite_idx ON catalog USING btree (nite);


--
-- Name: catalog_parentid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX catalog_parentid_idx ON catalog USING btree (parentid);


--
-- Name: catalog_run_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX catalog_run_idx ON catalog USING btree (run);


--
-- Name: desjob_blockid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX desjob_blockid_idx ON desjob USING btree (block_id);


--
-- Name: desjob_tjobid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX desjob_tjobid_idx ON desjob USING btree (tjob_id);


--
-- Name: exposure_nite_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX exposure_nite_idx ON exposure USING btree (nite);


--
-- Name: exposure_qa_exposureid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX exposure_qa_exposureid_idx ON exposure_qa USING btree (exposureid);


--
-- Name: exposure_qa_project_nite_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX exposure_qa_project_nite_idx ON exposure_qa USING btree (project, nite);


--
-- Name: exposure_qa_project_run_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX exposure_qa_project_run_idx ON exposure_qa USING btree (project, run);


--
-- Name: exposure_radec_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX exposure_radec_idx ON exposure USING btree (telra, teldec);


--
-- Name: image_nite_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX image_nite_idx ON image USING btree (nite);


--
-- Name: image_parentid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX image_parentid_idx ON image USING btree (parentid);


--
-- Name: image_radec_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX image_radec_idx ON image USING btree (ra, "dec");


--
-- Name: image_run_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX image_run_idx ON image USING btree (run);


--
-- Name: location_nite_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX location_nite_idx ON location USING btree (nite);


--
-- Name: location_run_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX location_run_idx ON location USING btree (run);


--
-- Name: location_tilename_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX location_tilename_idx ON location USING btree (tilename);


--
-- Name: orchtask_blockid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX orchtask_blockid_idx ON orchtask USING btree (block_id);


--
-- Name: orchtask_name_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX orchtask_name_idx ON orchtask USING btree (name);


--
-- Name: run_name_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX run_name_idx ON run USING btree (name);


--
-- Name: tjob_blockid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX tjob_blockid_idx ON tjob USING btree (block_id);


--
-- Name: tjob_orchtaskid_idx; Type: INDEX; Schema: public; Owner: deccp; Tablespace: 
--

CREATE INDEX tjob_orchtaskid_idx ON tjob USING btree (orchtask_id);


--
-- Name: archive_sites; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE archive_sites FROM deccp;
GRANT ALL ON TABLE archive_sites TO deccp;
GRANT ALL ON TABLE archive_sites TO cp_writer;
GRANT SELECT ON TABLE archive_sites TO cp_reader;


--
-- Name: block; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE block FROM deccp;
GRANT ALL ON TABLE block TO deccp;
GRANT ALL ON TABLE block TO cp_writer;
GRANT SELECT ON TABLE block TO cp_reader;


--
-- Name: block_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE block_seq FROM deccp;
GRANT ALL ON SEQUENCE block_seq TO deccp;
GRANT ALL ON SEQUENCE block_seq TO postgres;
GRANT ALL ON SEQUENCE block_seq TO cp_writer;
GRANT SELECT ON SEQUENCE block_seq TO cp_reader;


--
-- Name: catalog; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE catalog FROM deccp;
GRANT ALL ON TABLE catalog TO deccp;
GRANT ALL ON TABLE catalog TO cp_writer;
GRANT SELECT ON TABLE catalog TO cp_reader;


--
-- Name: coadd; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE coadd FROM deccp;
GRANT ALL ON TABLE coadd TO deccp;
GRANT ALL ON TABLE coadd TO cp_writer;
GRANT SELECT ON TABLE coadd TO cp_reader;


--
-- Name: coadd_src; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE coadd_src FROM deccp;
GRANT ALL ON TABLE coadd_src TO deccp;
GRANT ALL ON TABLE coadd_src TO cp_writer;
GRANT SELECT ON TABLE coadd_src TO cp_reader;


--
-- Name: coaddtile; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE coaddtile FROM deccp;
GRANT ALL ON TABLE coaddtile TO deccp;
GRANT SELECT ON TABLE coaddtile TO cp_reader;
GRANT ALL ON TABLE coaddtile TO cp_writer;


--
-- Name: coaddtile_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE coaddtile_seq FROM deccp;
GRANT ALL ON SEQUENCE coaddtile_seq TO deccp;
GRANT ALL ON SEQUENCE coaddtile_seq TO postgres;
GRANT ALL ON SEQUENCE coaddtile_seq TO cp_writer;
GRANT SELECT,USAGE ON SEQUENCE coaddtile_seq TO cp_reader;


--
-- Name: desjob; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE desjob FROM deccp;
GRANT ALL ON TABLE desjob TO deccp;
GRANT SELECT ON TABLE desjob TO cp_reader;
GRANT ALL ON TABLE desjob TO cp_writer;


--
-- Name: desjob_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE desjob_seq FROM deccp;
GRANT ALL ON SEQUENCE desjob_seq TO deccp;
GRANT ALL ON SEQUENCE desjob_seq TO postgres;
GRANT SELECT ON SEQUENCE desjob_seq TO cp_reader;
GRANT ALL ON SEQUENCE desjob_seq TO cp_writer;


--
-- Name: exposure; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE exposure FROM deccp;
GRANT ALL ON TABLE exposure TO deccp;
GRANT ALL ON TABLE exposure TO cp_writer;
GRANT SELECT ON TABLE exposure TO cp_reader;


--
-- Name: exposure_qa; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE exposure_qa FROM deccp;
GRANT ALL ON TABLE exposure_qa TO deccp;
GRANT ALL ON TABLE exposure_qa TO cp_writer;
GRANT SELECT ON TABLE exposure_qa TO cp_reader;


--
-- Name: exposure_qa_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE exposure_qa_seq FROM deccp;
GRANT ALL ON SEQUENCE exposure_qa_seq TO deccp;
GRANT SELECT ON SEQUENCE exposure_qa_seq TO cp_reader;
GRANT ALL ON SEQUENCE exposure_qa_seq TO cp_writer;


--
-- Name: image; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE image FROM deccp;
GRANT ALL ON TABLE image TO deccp;
GRANT ALL ON TABLE image TO cp_writer;
GRANT SELECT ON TABLE image TO cp_reader;


--
-- Name: location; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE location FROM deccp;
GRANT ALL ON TABLE location TO deccp;
GRANT ALL ON TABLE location TO cp_writer;
GRANT SELECT ON TABLE location TO cp_reader;


--
-- Name: location_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE location_seq FROM deccp;
GRANT ALL ON SEQUENCE location_seq TO deccp;
GRANT ALL ON SEQUENCE location_seq TO postgres;
GRANT ALL ON SEQUENCE location_seq TO cp_writer;
GRANT SELECT ON SEQUENCE location_seq TO cp_reader;


--
-- Name: objects_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE objects_seq FROM deccp;
GRANT ALL ON SEQUENCE objects_seq TO deccp;
GRANT ALL ON SEQUENCE objects_seq TO postgres;
GRANT ALL ON SEQUENCE objects_seq TO cp_writer;
GRANT SELECT ON SEQUENCE objects_seq TO cp_reader;


--
-- Name: objects_template; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE objects_template FROM deccp;
GRANT ALL ON TABLE objects_template TO deccp;
GRANT ALL ON TABLE objects_template TO cp_writer;
GRANT SELECT ON TABLE objects_template TO cp_reader;


--
-- Name: orchtask; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE orchtask FROM deccp;
GRANT ALL ON TABLE orchtask TO deccp;
GRANT SELECT ON TABLE orchtask TO cp_reader;
GRANT ALL ON TABLE orchtask TO cp_writer;


--
-- Name: orchtask_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE orchtask_seq FROM deccp;
GRANT ALL ON SEQUENCE orchtask_seq TO deccp;
GRANT ALL ON SEQUENCE orchtask_seq TO postgres;
GRANT SELECT ON SEQUENCE orchtask_seq TO cp_reader;
GRANT ALL ON SEQUENCE orchtask_seq TO cp_writer;


--
-- Name: run; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE run FROM deccp;
GRANT ALL ON TABLE run TO deccp;
GRANT ALL ON TABLE run TO cp_writer;
GRANT SELECT ON TABLE run TO cp_reader;


--
-- Name: run_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE run_seq FROM deccp;
GRANT ALL ON SEQUENCE run_seq TO deccp;
GRANT ALL ON SEQUENCE run_seq TO postgres;
GRANT ALL ON SEQUENCE run_seq TO cp_writer;
GRANT SELECT ON SEQUENCE run_seq TO cp_reader;


--
-- Name: sites; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE sites FROM deccp;
GRANT ALL ON TABLE sites TO deccp;
GRANT ALL ON TABLE sites TO cp_writer;
GRANT SELECT ON TABLE sites TO cp_reader;
GRANT ALL ON TABLE sites TO deccp;


--
-- Name: sites_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE sites_seq FROM deccp;
GRANT ALL ON SEQUENCE sites_seq TO deccp;
GRANT ALL ON SEQUENCE sites_seq TO postgres;
GRANT ALL ON SEQUENCE sites_seq TO cp_writer;
GRANT SELECT ON SEQUENCE sites_seq TO cp_reader;


--
-- Name: tjob; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE tjob FROM deccp;
GRANT ALL ON TABLE tjob TO deccp;
GRANT SELECT ON TABLE tjob TO cp_reader;
GRANT ALL ON TABLE tjob TO cp_writer;


--
-- Name: tjob_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE tjob_seq FROM deccp;
GRANT ALL ON SEQUENCE tjob_seq TO deccp;
GRANT ALL ON SEQUENCE tjob_seq TO postgres;
GRANT SELECT ON SEQUENCE tjob_seq TO cp_reader;
GRANT ALL ON SEQUENCE tjob_seq TO cp_writer;


--
-- Name: zeropoint; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON TABLE zeropoint FROM deccp;
GRANT ALL ON TABLE zeropoint TO deccp;
GRANT ALL ON TABLE zeropoint TO cp_writer;
GRANT SELECT ON TABLE zeropoint TO cp_reader;


--
-- Name: zeropoint_seq; Type: ACL; Schema: public; Owner: deccp
--

REVOKE ALL ON SEQUENCE zeropoint_seq FROM deccp;
GRANT ALL ON SEQUENCE zeropoint_seq TO deccp;
GRANT ALL ON SEQUENCE zeropoint_seq TO postgres;
GRANT ALL ON SEQUENCE zeropoint_seq TO cp_writer;
GRANT SELECT ON SEQUENCE zeropoint_seq TO cp_reader;


