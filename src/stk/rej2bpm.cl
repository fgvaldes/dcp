# When imcombine rejection masks are made, likely tiled because of
# very large field, this code coverts each plane to a mask for the
# image of the plane.

int	tile, naxis1, naxis2, crpix1, crpix2
int	nc, nl, nx, ny
int	c0, l0, c1, c2, l1, l2
string	root, rej

root = "ODIN_COSMOS20210207_9a29c0e-N501-94677L"

list = "../stkcrmask_imz.tmp"
for (i=1; fscan(list,s1)!=EOF; i+=1) {
    hselect (s1, "NAXIS1,NAXIS2,CRPIX1,CRPIX2", yes) | scan (nc, nl, nx, ny)

    for (tile=1; tile<=9; tile+=1) {

	rej = root // tile // "_z_rej.pl"

	hselect (rej, "NAXIS1,NAXIS2,CRPIX1,CRPIX2", yes) |
	   scan (naxis1, naxis2, crpix1, crpix2)

	c0 = crpix1 - nx
	l0 = crpix2 - ny
	c1 = 1 + c0
	c2 = nc + c0
	l1 = 1 + l0
	l2 = nl + l0
	if (c2 < 1 || c1 > naxis1 || l2 < 1 || l1 > naxis2)
	    next
	;
	c1 = max (1, c1)
	c2 = min (naxis1, c2)
	l1 = max (1, l1)
	l2 = min (naxis2, l2)

	printf ("%s[%d:%d,%d:%d,%d]\n", rej, c1, c2, l1, l2, i) | scan (s2)
	printf ("test%d_%d.pl\n", i, tile) | scan (s3)

	imdel (s3)
	imcopy (s2, s3)
	hedit (s3, "WAXMAP01,WCSDIM", del+)

	if (c0 < 0)
	    hedit (s3, "LTV1", c0, add+)
	else
	    hedit (s3, "LTV1", del+)
	;
	if (l0 < 0)
	    hedit (s3, "LTV2", l0, add+)
	else
	    hedit (s3, "LTV2", del+)
	;

	print (s3, >> "test.list")
    }

    imcombine ("@test.list", "bpm"//i//".pl", offset="physical")
    imdelete ("@test.list")
    delete ("test.list")
}
list = ""
