# Take MPC records and jitter to 3 "observations" per night using
# random noise.  Also order them in time.

string	id, yy, sgn
real	m, d, rah, ram, ras, decd, decm, decs, mag, t, dt
real	c1, c2, c3, c4, c5, c6, n1, n2, n3, n4
struct	*fd, *noise

# Separate into individual TNOs.
del ("*.tno")
list = "tno.txt"
while (fscan (list, line) != EOF) {
    if (substr(line, 1, 5) != "     ")
        next
    i = fscan (line, s1)
    print (line, >> s1//".tno")
}
list = ""

# For each TNO expand the measurements.

# For each night jitter the measurment by this many minutes
# (before and after the central timestamp).
dt = 5. / 60. / 24.

noise = "NHPPS_PIPEAPPSRC$/mov/noise.dat"
files ("*.tno", > "tno.list")
delete ("mpc.txt")
list = "tno.list"
while (fscan (list, s1) != EOF) {
    fd = s1
    while (fscan (fd, line) != EOF) {
        i = fscan (line,id,yy,m,d,rah,ram,ras,decd,decm,decs,mag)
	# Specific to particular run.
	d -= 27
        if (d < 0)
	    d += 30
        x = d * 24
	printf ("%g %.1f %s\n", x, mag, substr(line,1,55), >> "rec.dat") 
    }
    fd = ""
    sort ("rec.dat", num+) | concat ("STDIN", > "rec.dat")
    fd = "rec.dat"; i = 0
    while (fscan (fd,x,mag,id,yy,m,d,rah,ram,ras,decd,decm,decs) != EOF) {
	printf ("%d:%d:%g\n", rah, ram, ras) | scan (rah)
	printf ("%d:%d:%g\n", decd, decm, decs) | scan (decd)
	if (decd < 0) {
	    sgn = "-"
	    decd = -decd
	} else
	    sgn = " "

        if (fscan (noise, n1, n2, n3, n4) == EOF) {
	    noise = "noise.dat"
	    k = fscan (noise, n1, n2, n3, n4)
	}

	i += 1
        t = d - dt
	if (t < 1) {
	    m -= 1
	    t += 30
	}
        y = rah + 2 * n1 / 3600. / 15.
        z = decd + 2 * n2 / 3600.
	if (i == 1)
	    printf ("%6t%s%15t%s %02d %08.5f %011.2h %s%010.1h%66t%4.1f%78tW84\n",
	        id, yy, m, t, y, sgn, z, mag) |
		translit ("STDIN", ":", " ", >> "mpc.txt")
	else
	    printf ("%6t%s%15t%s %02d %08.5f %011.2h %s%010.1h%78tW84\n",
	         id, yy, m, t, y, sgn, z) |
		 translit ("STDIN", ":", " ", >> "mpc.txt")

	t = d
        y = rah
        z = decd
	printf ("%6t%s%15t%s %02d %08.5f %011.2h %s%010.1h%78tW84\n",
	     id, yy, m, t, y, sgn, z) |
	     translit ("STDIN", ":", " ", >> "mpc.txt")

        t = d + dt
        y = rah + 2 * n3 / 3600. / 15.
        z = decd + 2 * n4 / 3600.
	printf ("%6t%s%15t%s %02d %08.5f %011.2h %s%010.1h%78tW84\n",
	     id, yy, m, t, y, sgn, z) |
	     translit ("STDIN", ":", " ", >> "mpc.txt")
    }
    fd = ""
    delete ("rec.dat")
}
list = ""
noise = ""
delete ("@tno.list,tno.list")
