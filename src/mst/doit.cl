struct *fd
real	magref, skyref
string	out, nrej, bpm

delete mstsetup_ims.tmp

fd = "list"
while (fscan (fd, s1) != EOF) {
    # Create relative scalings.
    list = s1; magref = INDEF; skyref = INDEF
    while (fscan (list, s2) != EOF) {
	hselect (s2, "$MAGZERO,$SKYSUB", yes) | scan (x,y)
	if (isindef(magref)) {
	    magref = x
	    skyref = y
	}
	;
	z = 10. ** (0.4 * (magref - x))
	hedit (s2, "STKSCALE", z, add+)
	z = skyref / z - y
	hedit (s2, "STKZERO", z, add+)
    }
    list = ""

    # Set first and last exposures.
    head (s1, nl=1) | scan (s2)
    tail (s1, nl=1) | scan (s3)
    s2 = substr (s2, 1, strldx(".",s2)-1)
    s3 = substr (s3, 1, strldx(".",s3)-1)

    # Create stack and bad pixel mask.
    out = "new.fits"; nrej="nrej.pl"; bpm = "bpm.pl"
    imcombine ("@"//s1, out, nrej=nrej, offsets="world",
        masktype="goodvalue", maskvalue=0, scale="!STKSCALE", zero="!STKZERO")
    imexpr ("a<2?0: a", bpm, nrej)
    hedit (out, "BPM", bpm, add+)
    imdelete (nrej)

    # Compute exposure time including overheads.
    hselect (s2, "TIME-OBS", yes) | scan (x)
    hselect (s3, "TIME-OBS,EXPTIME", yes) | scan (y, z)
    z = (y - x) * 3600 + z
    hedit (out, "EXPTIME", z, show+)

    # Delete the input and set the output.
    hselect (s2, "BPM", yes) | scan (s3)
    hselect ("@"//s1, "BPM", yes, > "bpm"//s1)
    imdelete ("@"//s1//",@bpm"//s1)
    imrename (out, s2)
    imrename (bpm, s3)
    hedit (s2, "BPM", s3, add+)

    print (s2//".fits", >> "mstsetup_ims.tmp")
}
fd = ""
