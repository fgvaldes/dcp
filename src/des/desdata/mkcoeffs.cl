# Usage: cl < mkcoeffs.cl > coeffs.dat

del mc.dat
for (i=1; i<=62; i+=1) {
    if (i==31 || i==61)
        next
    printf ("%02d\n", i) | scan (s1)
    s2 = "ccd_" // s1
    match ("_"//s1//".fits", "data.txt") | fields STDIN 6,7 | match NULL stop+ |
        sort (num+, > s2)
    printf ("%s", s1)
    curfit (s2, order=4, interactive+, listdata-, power+,
        cursor="cur.dat", >>G "mc.dat") |
	translit STDIN "#" " " | fields STDIN 2 lines=25-28 | table STDIN
    delete (s2)
}
