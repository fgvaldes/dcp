-- Create indexed table of basic data.

CREATE TEMP TABLE raw AS
SELECT date(start_date) date, reference, dtnsanam, dtpropid,
substring(dtacqnam from 'DECam[^.]*') dtacq, object FROM voi.siap
WHERE instrument='DECam'
AND proctype='raw'
-- AND obstype IN ('object', 'standard')
-- AND object NOT SIMILAR TO '%(focus|donut|junk|pointing)%'
-- AND object NOT SIMILAR TO '%SDSS%'
-- AND dtpropid SIMILAR TO '%(0001)%'
AND start_date BETWEEN TIMESTAMP '19000101' AND TIMESTAMP '20150101';

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT date(start_date) date, reference, dtnsanam, dtpropid,
substring(dtacqnam from 'DECam[^.]*') dtacq, object FROM voi.siap
WHERE instrument='DECam'
AND proctype!='raw'
AND surveyid LIKE '%DES%'
-- AND object NOT SIMILAR TO '%SDSS%'
AND start_date BETWEEN TIMESTAMP '19000101' and TIMESTAMP '20150101';

CREATE INDEX procidx ON proc (dtnsanam);

-- Create count tables.

CREATE TEMP TABLE unprocessed AS
SELECT date, dtpropid, count(date) unprocessed FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
GROUP BY date, dtpropid;

CREATE TEMP TABLE processed AS
SELECT date, dtpropid, count(date) processed FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
GROUP BY date, dtpropid;

CREATE TEMP TABLE unprocessed_only AS
SELECT a.date, a.dtpropid, 0 processed, unprocessed FROM unprocessed a,
(SELECT date, dtpropid FROM unprocessed EXCEPT SELECT date, dtpropid FROM processed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid;

CREATE TEMP TABLE processed_only AS
SELECT a.date, a.dtpropid, processed, 0 unprocessed FROM processed a,
(SELECT date, dtpropid FROM processed EXCEPT SELECT date, dtpropid FROM unprocessed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid;

CREATE TEMP TABLE foo AS
SELECT a.date, a.dtpropid, processed, unprocessed
FROM processed a, unprocessed b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid;

-- Create output.

SELECT reference FROM raw a,
(SELECT dtacq FROM raw INTERSECT SELECT dtacq FROM proc) b
WHERE a.dtacq = b.dtacq
;
SELECT reference FROM proc a,
(SELECT dtacq FROM raw INTERSECT SELECT dtacq FROM proc) b
WHERE a.dtacq = b.dtacq
;
