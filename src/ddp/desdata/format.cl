# Note how this would be called twice to bootstrap the coefficients file.
#
# Usage: cl < format.cl > <out>.txt

noao
astutil

struct	*fd
string	yyyy, mm, dd, exp, ccd, explast, date, date1, dmag
real	c1, c2, c3, c4

date1 = "2013-08-30"

list = "NOAO_handoff.txt"; explast = ""
while (fscan (list, line) != EOF) {
    exp = substr (line, 74, 87)
    if (exp == explast) {
        print (line, >> "tmp.txt")
	next
    }
    if (explast == "") {
        print (line, > "tmp.txt")
        explast = exp
	next
    }
    explast = exp

    # Set the reference.
    match ("_28", "tmp.txt") | scan (s1, s2, s3)
    if (s2 == "NULL") {
	tstat ("tmp.txt", "c2", out="")
	z = tstat.mean
    } else
        z = real(s2)

    fd = "tmp.txt"
    while (fscan (fd, s1, s2, s3) != EOF) {
	yyyy = substr (s1, 46, 49)
	mm = substr (s1, 50, 51)
	dd = substr (s1, 52, 53)
	exp = substr (s1, 74, 87)
	ccd = substr (s1, 89, 90)
	date = yyyy // "-" // mm // "-" // dd
	printf ("print(julday('%s')-julday('%s'))\n", date, date1) |
	    astcalc (prompt="", verbose-) | scan (x)
	if (s2 == "NULL") {
	    if (access("coeffs.dat")) {
	        match ("^"//ccd, "coeffs.dat") | scan (i, c1, c2, c3, c4)
		if (i != int(ccd))
		    error (1, "EXIT")
		y = c1 + x * (c2 + x * (c3 + x * c4))
		printf ("%7.4f\n", y) | scan (dmag)
		y += z
		printf ("%7.4f\n", y) | scan (s2)
	    } else
		dmag = "NULL"
	} else {
	    y = real(s2) - z
	    printf ("%7.4f\n", y) | scan (dmag)
	}
	s1 = "Archive$" // substr (s1, strstr("/OPS",s1), 999)
	printf ("%s %s %s %s %7.4f %d %s\n", s1, s2, s3, ccd, z, x, dmag)
    }
    fd = ""

    print (line, > "tmp.txt")
}
list = ""

# Set the reference.
match ("_28", "tmp.txt") | scan (s1, s2, s3)
if (s2 == "NULL") {
    tstat ("tmp.txt", "c2", out="")
    z = tstat.mean
} else
    z = real(s2)

fd = "tmp.txt"
while (fscan (fd, s1, s2, s3) != EOF) {
    yyyy = substr (s1, 46, 49)
    mm = substr (s1, 50, 51)
    dd = substr (s1, 52, 53)
    exp = substr (s1, 74, 87)
    ccd = substr (s1, 89, 90)
    date = yyyy // "-" // mm // "-" // dd
    printf ("print(julday('%s')-julday('%s'))\n", date, date1) |
	astcalc (prompt="", verbose-) | scan (x)
    if (s2 == "NULL") {
	if (access("coeffs.dat")) {
	    match ("^"//ccd, "coeffs.dat") | scan (i, c1, c2, c3, c4)
	    if (i != int(ccd))
		error (1, "EXIT")
	    y = c1 + x * (c2 + x * (c3 + x * c4))
	    printf ("%7.4f\n", y) | scan (dmag)
	    y += z
	    printf ("%7.4f\n", y) | scan (s2)
	} else
	    dmag = "NULL"
    } else {
	y = real(s2) - z
	printf ("%7.4f\n", y) | scan (dmag)
    }
    s1 = "Archive$" // substr (s1, strstr("/OPS",s1), 999)
    printf ("%s %s %s %s %7.4f %d %s\n", s1, s2, s3, ccd, z, x, dmag)
}
fd = ""; delete ("tmp.txt")
