string	band = "z"
real num = 65
real maxexp = 250
real maxsky = 5500
real maxrate = 40

string	in, dat, mc

in = "list" // band
dat = "tmp" // band
mc = "mc" // band

if (b1) {

    hselect ("@"//in, "exptime,skysub,frgscale", yes, > dat)
    tcalc (dat, "c4", "c2/c1")
    tcalc (dat, "c5", "c3/c2")
    tcalc (dat, "c6", "c3/c4")

    tstat (dat, "c1")
    tstat (dat, "c2")
    tstat (dat, "c4")

    b1 = no

} else {

    fields (dat, "5") |
	graph (wx1=0, wx2=num, wy1=0, wy2=1,
	    ylabel="scale/sky", title="z-band", >G mc)
    fields (dat, "1,5") |
	graph (wx1=0, wx2=maxexp, wy1=0, wy2=1,
	    xlabel="exptime", ylabel="scale/sky", title="z-band", >>G mc)
    fields (dat, "2,5") |
	graph (wx1=0, wx2=maxsky, wy1=0, wy2=1,
	    xlabel="sky", ylabel="scale/sky", title="z-band", >>G mc)
    fields (dat, "4,5") |
	graph (wx1=0, wx2=maxrate, wy1=0, wy2=1,
	    xlabel="skyrate", ylabel="scale/sky", title="z-band", >>G mc)

    gkimos (mc, nx=1, ny=1)

}
