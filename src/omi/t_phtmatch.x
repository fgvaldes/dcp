# PHTZP -- Find a zero point from two catalogs.

include	<mach.h>
include	<math.h>
include	<tbset.h>

define	RA	8		# ALPHA_J2000
define	DEC	9		# DELTA_J2000
define	MAG	6		# MAG_APER
define	RADIUS  40		# FLUX_RADIUS
define	APER	3		# Aperture 3 = 3 arcsec diam.

task	phtmatch = t_phtmatch

procedure t_phtmatch ()

char	input[SZ_FNAME]			# Input catalog
char	output[SZ_FNAME]		# Output catalog
char	reference[SZ_FNAME]		# Reference catalog
double	match				# Match half-box size (arcsec)
real	mrefmin				# Minimum ref mag to use
real	mrefmax				# Maximum ref mag to use

char	fname[SZ_FNAME]
int	reflist, inlist
int	i, j, k, n, nref, nobs, rowref, rowobs, ccd
real	dra, ddec, r, mref, mobs, radius
double	raref, decref, ra, dec, c
pointer	in, out, ref[4], obs[6], ptr

int	clpopnu(), clgfil(), compare(), tbpsta(), tbcigi(), strldxs(), ctoi()
real	clgetr()
pointer	tbtopn(), tbtcre(), tbcnum()
extern	compare
errchk	tbtopn, tbcdef, tbtcre, tbpsta

double rarefmin, rarefmax, raobsmin, raobsmax

begin
	# Get parameters.
	inlist = clpopnu ("input")
	call clgstr ("output", output, SZ_FNAME)
	reflist = clpopnu ("reference")
	match = clgetr ("match")
	mrefmin = clgetr ("mrefmin")
	mrefmax = clgetr ("mrefmax")

	# Read reference sources into memory and sort by declination.
	nref = 0
	while (clgfil (reflist, reference, SZ_FNAME) != EOF) {
	    in = tbtopn (reference, READ_ONLY, NULL)
	    i = tbpsta (in, TBL_NROWS)
	    if (nref == 0) {
	        call malloc (ref[1], i, TY_DOUBLE)
	        call malloc (ref[2], i, TY_DOUBLE)
	        call malloc (ref[3], i, TY_REAL)
	    } else {
	        call realloc (ref[1], nref+i, TY_DOUBLE)
	        call realloc (ref[2], nref+i, TY_DOUBLE)
	        call realloc (ref[3], nref+i, TY_REAL)
	    }
	    call malloc (ptr, i, TY_BOOL)
	    call tbcgtd (in, tbcnum(in,1), Memd[ref[1]+nref], Memb[ptr], 1, i)
	    call tbcgtd (in, tbcnum(in,2), Memd[ref[2]+nref], Memb[ptr], 1, i)
	    call tbcgtr (in, tbcnum(in,3), Memr[ref[3]+nref], Memb[ptr], 1, i)
	    call mfree (ptr, TY_BOOL)
	    nref = nref + i
	    call tbtclo (in)
	}
	call malloc (ref[4], nref, TY_INT)
	ptr = ref[4]
	rarefmin = Memd[ref[1]]
	rarefmax = Memd[ref[1]]
	do i = 0, nref-1 {
	    rarefmin = min (rarefmin, Memd[ref[1]+i])
	    rarefmax = max (rarefmax, Memd[ref[1]+i])
	    Memi[ptr] = i
	    ptr = ptr + 1
	}
	call gqsort (Memi[ref[4]], nref, compare, ref[2])

	# Read observed sources into memory and sort by declination.
	nobs = 0
	while (clgfil (inlist, input, SZ_FNAME) != EOF) {
	    #j = strldxs ("_", input) - 2
	    j = strldxs ("_", input) + 1
	    if (j > 1 && ctoi (input, j, k) > 0)
	        ccd = k
	    else
	        ccd = 0
	    call sprintf (fname, SZ_FNAME, "%s[2]")
	        call pargstr (input)
	    in = tbtopn (fname, READ_ONLY, NULL)
	    i = tbpsta (in, TBL_NROWS)
	    if (nobs == 0) {
	        call malloc (obs[1], i, TY_DOUBLE)
	        call malloc (obs[2], i, TY_DOUBLE)
	        call malloc (obs[3], i, TY_REAL)
		call malloc (obs[5], i, TY_SHORT)
		call malloc (obs[6], i, TY_REAL)
	    } else {
	        call realloc (obs[1], nobs+i, TY_DOUBLE)
	        call realloc (obs[2], nobs+i, TY_DOUBLE)
	        call realloc (obs[3], nobs+i, TY_REAL)
		call realloc (obs[5], nobs+i, TY_SHORT)
		call realloc (obs[6], nobs+i, TY_REAL)
	    }
	    call malloc (ptr, i, TY_BOOL)
	    call tbcgtd (in, tbcnum(in,RA), Memd[obs[1]+nobs], Memb[ptr], 1, i)
	    call tbcgtd (in, tbcnum(in,DEC), Memd[obs[2]+nobs], Memb[ptr], 1, i)
	    call tbcgtr (in, tbcnum(in,RADIUS), Memr[obs[6]+nobs], Memb[ptr], 1, i)
	    call mfree (ptr, TY_BOOL)
	    do j = 0, i-1 {
		call tbagtr (in, tbcnum(in,MAG), j+1, Memr[obs[3]+nobs+j],
		    APER, 1)
	    }
	    call amovks (ccd, Mems[obs[5]+nobs], i)
	    nobs = nobs + i
	    call tbtclo (in)
	}
	call malloc (obs[4], nobs, TY_INT)
	ptr = obs[4]
	raobsmin = Memd[obs[1]]
	raobsmax = Memd[obs[1]]
	do i = 0, nobs-1 {
	    raobsmin = min (raobsmin, Memd[obs[1]+i])
	    raobsmax = max (raobsmax, Memd[obs[1]+i])
	    Memi[ptr] = i
	    ptr = ptr + 1
	}
	call gqsort (Memi[obs[4]], nobs, compare, obs[2])

	# Some log information.
	call eprintf ("Ref: RA=(%h,%h) DEC=(%h,%h)\n")
	call pargd (rarefmin)
	call pargd (rarefmax)
	call pargd (Memd[ref[2]+Memi[ref[4]]])
	call pargd (Memd[ref[2]+Memi[ref[4]+nref-1]])
	call eprintf ("In:  RA=(%h,%h) DEC=(%h,%h)\n")
	call pargd (raobsmin)
	call pargd (raobsmax)
	call pargd (Memd[obs[2]+Memi[obs[4]]])
	call pargd (Memd[obs[2]+Memi[obs[4]+nobs-1]])

	# Open output catalog.
	out = tbtopn (output, NEW_FILE, NULL)
	call tbcdef (out, ptr, "CCD", "", "%d", TY_SHORT, 1, 1)
	call tbcdef (out, ptr, "RA", "deg", "%g", TY_DOUBLE, 1, 1)
	call tbcdef (out, ptr, "DEC", "deg", "%g", TY_DOUBLE, 1, 1)
	call tbcdef (out, ptr, "REFMAG", "", "%.3f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "INSTMAG", "", "%.3f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "RADIUS", "", "%.2f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "DRA", "", "%.3f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "DDEC", "", "%.3f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "POSERR", "", "%.3f", TY_REAL, 1, 1)
	call tbcdef (out, ptr, "ZP", "", "%.3f", TY_REAL, 1, 1)
	call tbtcre (out)

	# Match.
	j = 0; n = 0
	do i = 0, nref-1 {
	    rowref = Memi[ref[4]+i]
	    rowobs = Memi[obs[4]+j]
	    mref = Memr[ref[3]+rowref]
	    if (mref > mrefmax || mref < mrefmin)
	        next
	    decref = Memd[ref[2]+rowref]
	    dec = Memd[obs[2]+rowobs]
	    ddec = 3600. * (decref - dec)
	    if (ddec < -match)
	        next
	    else if (ddec > match) {
	        i = i - 1; j = j + 1
	    } else {
	        raref = Memd[ref[1]+rowref]
	        ra = Memd[obs[1]+rowobs]
		c = cos (DEGTORAD(decref))
		dra = 3600. * (raref - ra) * c
		if (abs(dra) <= match)
		    j = j + 1
		else {
		    do k = j+1, nobs-1 {
			rowobs = Memi[obs[4]+k]
			ra = Memd[obs[1]+rowobs]
			dec = Memd[obs[2]+rowobs]
			radius = Memr[obs[6]+rowobs]
			ddec = 3600. * (decref - dec)
			dra = 3600. * (raref - ra) * c
			if (ddec < -match || abs(dra) <= match)
			    break
		    }
		}
		if (abs(dra) <= match) {
		    mref = Memr[ref[3]+rowref]
		    mobs = Memr[obs[3]+rowobs]
		    r = sqrt (dra**2+ddec**2)

		    if (mobs < 30) {
			n = n + 1
			call tbepts (out, tbcnum(out,1), n, Mems[obs[5]+rowobs])
			call tbeptd (out, tbcnum(out,2), n, raref)
			call tbeptd (out, tbcnum(out,3), n, decref)
			call tbeptr (out, tbcnum(out,4), n, mref)
			call tbeptr (out, tbcnum(out,5), n, mobs)
			call tbeptr (out, tbcnum(out,6), n, radius)
			call tbeptr (out, tbcnum(out,7), n, dra)
			call tbeptr (out, tbcnum(out,8), n, ddec)
			call tbeptr (out, tbcnum(out,9), n, r)
			call tbeptr (out, tbcnum(out,10), n, mref-mobs+25)
		    }
		}
	    }
	    if (j >= nobs)
	        break
	}

	# Finish up.
	call tbtclo (out)
	call mfree (ref[1], TY_DOUBLE)
	call mfree (ref[2], TY_DOUBLE)
	call mfree (ref[3], TY_REAL)
	call mfree (ref[4], TY_INT)
	call mfree (obs[1], TY_DOUBLE)
	call mfree (obs[2], TY_DOUBLE)
	call mfree (obs[3], TY_REAL)
	call mfree (obs[4], TY_INT)
	call mfree (obs[5], TY_SHORT)
	call mfree (obs[6], TY_REAL)
	call clpcls (reflist)
	call clpcls (inlist)
end


int procedure compare (coords, row1, row2)

pointer	coords			# Pointer to coordinates (RA|DEC)
int	row1, row2		# Rows to compare

double	c1, c2

begin
	c1 = Memd[coords+row1]; c2 = Memd[coords+row2]
	if (c1 < c2)
	    return (-1)
	else if (c1 > c2)
	    return (1)
	else
	    return (0)
end
