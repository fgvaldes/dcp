#!/bin/env pipecl

# This only applies to certain filters.
if (omi_dofrg == NO)
    plexit COMPLETED
;

int	frg_blk = 5		{prompt="Fringe smoothing block"}
real	fudge = 1.5
real	maxskyfrac = 2.0	{prompt="Maximum fraction of sky"}

file	frt
real	scale, statwt, median

# Packages and task.
nfextern
msctools

print (dataset.sname) | translit ("STDIN", "-", " ") | scan (s1, s2)
if (s2 == 'x')
    s2 = substr (dataset.child, 13, 99)
;
if (s2 == "z")
    fudge = 2.5
else
    fudge = 2.0
;

# Set input.
files (dataset.sname//"_[0-9][0-9].fits", > "omifrg_ims.tmp")

# Compute constrained scale.
list = "omifrg_ims.tmp"; scale = 0.; statwt = 0.; i = 0
while (fscan (list, s1) != EOF) {
    hselect (s1, "$ILLMASK,$SKYIM", yes) | scan (s2, s3)
    if (s2 == "INDEF" || s3 == "INDEF")
        next
    ;
    frt = ""
    getcal ("fringe", image=s1, imageid="!ccdnum", filter="!filter",
        mjd="!mjd-obs", quality=">0", path+)
    if (getcal.nfound == 0) {
	getcal ("fringe", image=s1, imageid="!ccdnum", filter="!filter",
	    mjd="!mjd-obs", quality=">0", path+, sql="STDOUT")
	next
    }
    ;
    frt = getcal.value
    if (access(frt) == NO) {
        printf ("Unpack calibration? (%s)\n", frg)
	break
    }
    ;

    patfit (s1, "", frt, mask="!illmask", background="!skyim", bkgpattern="0.",
        ncblk=frg_blk, nlblk=frg_blk, logname="FRGCOR", > "omifrg_scale.tmp")
    concat ("omifrg_scale.tmp")

    match ("scale", "omifrg_scale.tmp") | scanf ("  scale = %g", x)
    match ("statwt", "omifrg_scale.tmp") | scanf ("  statwt = %g", y)
    match ("<background>", "omifrg_scale.tmp") |
        scanf ("  <background> = %g", z)

    if (x < 0 || z < 0)
        next
    ;

    i += 1
    scale += x * y
    statwt += y
    print (x, y, z, >> "omifrg_tbl.tmp")
}
list = ""

if (i < 30) {
    printf ("WARNING: Not enough good fringe template CCDs found\n")
    plexit NODATA
}
;
if (access(frt) == 0) {
    printf ("WARNING: Unpack calibration? (%s)\n", frg)
    plexit HALT
}
;

#if (statwt > 0)
#    scale /= statwt
#else
#    scale = 0
#scale *= fudge
#printf ("\nFinal scale = %.3f\n", scale)

if (i > 0) {
    tsort ("omifrg_tbl.tmp", "c1")
    j = max(0.5 * i, 1); k = max(0.9 * i, j)
    tstat ("omifrg_tbl.tmp", "c3", rows=j//"-"//k)
    x = tstat.mean
    tstat ("omifrg_tbl.tmp", "c1", rows=j//"-"//k)
    #scale = tstat.median * fudge
    z = tstat.mean * fudge
    scale = min (z, maxskyfrac*x)
} else {
    x = INDEF
    scale = 0
}
;
y = scale / max (1,x)
z = z / max (1,x)
if (y == z)
    printf ("\nFinal scale = %.2f  sky = %d  skyfrac = %.1f\n", scale, x, y)
else
    printf ("\nFinal scale = %.2f  sky = %d  skyfrac = %.1f -> %.1f\n",
	scale, x, z, y)
;

#if (scale == 0)
#    plexit NODATA
#;

# Subtract scaled fringe template from data.
list = "omifrg_ims.tmp"
while (fscan (list, s1) != EOF) {
    s2 = "BAK"//substr (s1, strldx("/",s1)+1, 999)
    rename (s1, s2)
    printf ("rename %s %s\n", s2, s1, >> dataset.frestore)
    print (s2, >> ".omidone_delete.tmp")

    s3 = substr (frt, strldx("/",frt)+1, strldx(".",frt)-1)
    getcal ("fringe", image=s2, imageid="!ccdnum",
        filter="!filter", mjd="!mjd-obs", quality=">=0", path+)
    if (getcal.nfound == 0) {
	imexpr ("a", s1, s2, outtype="real")
    } else {
	frt = getcal.value
	imexpr ("a - b * c", s1, s2, frt, scale, outtype="real")
	nhedit (s1, "FRINGE", s3, "Fringe template", add+)
	nhedit (s1, "FRGSCALE", scale, "Pupil template scale", add+, show-)
    }
}
list = ""

#plexit HALT
plexit COMPLETED
