# CKPROPID -- Check if propid is selected by propids list.
# 
# This outputs either "accept" or "reject".

procedure ckpropids (propids, propid)

string	propids		{prompt = "PropID list"}
string	propid		{prompt = "PropID to match against list"}
struct	*fd

begin
	file	tmp
	int	i
	string	p1, p2, p3
	bool	accept = YES

	p1 = propids
	p2 = propid
	p3 = propid

	tmp = mktemp ("ckpropids")
	files (propids, > tmp)
	fd = tmp
	while (fscan (fd, p3) != EOF) {
	    i = stridx ('!', p3) + 1
	    if (strstr (substr(p3,i,99), p2) > 0) {
		if (i == 1)
		    accept = YES
		else
		    accept = NO
		break
	    }
	    if (i == 1)
	        accept = NO
	}
	fd = ""; delete (tmp)
	if (accept)
	    print ("accept")
	else
	    print ("reject")
end
