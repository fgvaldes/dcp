#{ PLAPP - Pipeline specific IRAF package

cl < "plapp$lib/zzsetenv.def"
package plapp, bin = plappbin$

# Define some Unix commands.
# Use the Bourne shell which is faster to start up in the pipeline environment.
task	$vi		= "$!vi $(*)"
task	$ed		= "$!vi $(*)"
task	$sed		= "$!sed $(*)"
task	$find		= "$!find $(*)"
task	$grep		= "$!grep $(*)"
task	$ls		= "$!ls $(*)"
task	$mv		= "$!mv $(*)"
task	$rm		= "$!rm $(*)"
task    $rmdir          = "$! rm -r $(*)"
task    $mkpdir         = "$!mkdir -p $(*)"
task    $ln             = "$!ln $(*)"
task    $rsync          = "$!rsync $(*)"
task    $scp            = "$!scp $(*)"
task    $fpack          = "$!fpack $(*)"
task    $funpack        = "$!funpack $(*)"

task	$pldb		= "$!pldb"

# Tasks.
task	ckpropid	= "plappsrc$ckpropid.cl"
task    getcal          = "plappsrc$getcal.cl"
cache   getcal
unlearn getcal
task    putcal          = "plappsrc$putcal.cl"
cache   putcal
unlearn putcal
task    getrefcats      = "plappsrc$getrefcats.cl"


# Subpackages.
set	mov		= "plapp$mov/"
task	$mov.pkg	= "mov$mov.cl"

keep
