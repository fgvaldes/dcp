# MOVID -- Create an MPC ID from an STB or DECam ID.
# This is a complicated process to map to a short MPC length.

procedure movid ()

string	stbid			{prompt="STB/DEC ID"}
int	grpid			{prompt="Group ID"}
string	movid			{prompt="MOV ID"}
string	dir = "to"		{prompt="Direction",
				 enum="to|from"}
bool	decamnum = yes		{prompt="Is stbid a DECam expnum?"}
bool	verbose	= yes		{prompt="Print IDs?"}

begin
	#int	numoffset = 500000
	int	numoffset = 1000000
	int	base = 62
	int	i, j, k, l, r, d, n
	string	s, id

	if (dir == "to") {
	    if (stridx ("_", stbid) > 0)
	        stbid = substr (stb, 1, stridx("_",stbid)-1)
	    movid = ""
	    for (j = 0; j < 2; j+=1) {
		if (j == 0) {
		    if (decamnum)
			r = int (stbid) + 2383280 + numoffset
		    else if (substr(stbid, 1, 1) == 'd')
			r = int (substr (stbid, 4, 999)) + 2383280
		    else
			r = int (substr (stbid, 3, 999)) + 2383280
		} else {
		    s = substr (stbid, 1, 1)
		    if (s == "k")
			r = 9000 + grpid
		    else if (s == "c")
			r = 9500 + grpid
		    else if (s == "d")
			r = 5000 + grpid
		    else
			r = 5000 + grpid
		}
		for (i=0; base**(i+1) <= r; i=i+1)
		    ;
		for (; i >= 0; i -= 1) {
		    d = base**i
		    n = r / d
		    r = r % d
		    if (n < 10)
			n += 48
		    else if (n < 36)
			n += 55
		    else
			n += 61
		    printf ("%s%c\n", movid, n) | scan (id)
		    movid = id
		}
	    }
	} else {
	    for (j=0; j<2; j+=1) {
		k = min(6,strlen (movid)) 
		if (j == 0)
		    s = substr (movid,1,k-2)
		else
		    s = substr (movid,k-1,99)
		k = strlen (s); r = 0
		for (i=1; i<=k; i+=1) {
		    n = substr (s, i, i)
		    if (n <= 57)
			n -= 48
		    else if (n <= 90)
			n -= 55
		    else
			n -= 61
		    r = (base * r) + n
		}
		if (j == 0) {
		    r -= 2383280
		    l = r
		} else {
		    if (decamnum) {
			l -= numoffset
			printf ("%06d\n", l) | scan (id)
			grpid = r - 5000
		    } else {
			if (r >= 9000 && r < 9500) {
			    grpid = r - 9000
			    printf ("kp%07d\n", l) | scan (id)
			} else if (r >= 9500 && r < 10000) {
			    grpid = r - 9500
			    printf ("ct%07d\n", l) | scan (id)
			} else if (r >= 5000 && r < 9000) {
			    grpid = r - 5000
			    printf ("dec%06d\n", l) | scan (id)
			} else
			    printf ("xx%d\n", l) | scan (id)
		    }
		    stbid = id
		}
	    }
	}

	if (verbose)
	    printf ("%s %3d %s\n", stbid, grpid, movid)
end
