struct	*fd1, *fd2
string	s4, s5, s6, s7, s8,s9, s10, s11, s12, s13, s14
real	mag1, mag2, rate1, rate2

#delete ("tmpfake*")
#print ("#c GRPID ch*7", >> "tmpfake")
#print ("#c DATE1 ch*5", >> "tmpfake")
#print ("#c DATE2 ch*2", >> "tmpfake")
#print ("#c DATE3 ch*8", >> "tmpfake")
#print ("#c RA d %13.2h hours", >> "tmpfake")
#print ("#c DEC d %13.1h degrees", >> "tmpfake")
#print ("#c MAG d %4.1f", >> "tmpfake")
#
#list = "DEC14A_20140423_8595ac6-VR_C_mpc.txt"
#while (fscan (list, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, x) != EOF) {
#    printf ("%s %s %s %s %s:%s:%s %s:%s:%s %4.1f\n",
#        s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, x, >> "tmpfake")
#    printf ("%s\n", s1, >> "tmpfake_id")
#}
#list = ""
#sort tmpfake_id | uniq > tmpfake_id

# No fake sources found as real sources!
#tmatch ("tmpnew", "tmpfake", "matchfake.tmp", "c5,c6", "c5,c6",
#    0:00:02, incol1="c1,c2,c3,c4,c5,c6,c7",
#    incol2="c1,c5,c6,c7", factor=" ", diagfile="", nmcol1=" ", nmcol2="",
#    sphere=yes)
#tmatch ("tmpfake", "tmpnew", "matchfake.tmp", "c5,c6", "c5,c6",
#    0:00:02, incol1="c1,c2,c3,c4,c5,c6,c7",
#    incol2="c1,c5,c6,c7", factor=" ", diagfile="", nmcol1=" ", nmcol2="",
#    sphere=yes)

#tmatch ("tmpfake", "DEC14A_20140423_8595ac6-VR_addast.txt", "matchfake.tmp",
#    "RA,DEC", "RA,DEC",
#    0:00:02, incol1="GRPID,DATE1,DATE2,DATE3,RA,DEC,MAG",
#    incol2="", factor=" ", diagfile="", nmcol1=" ", nmcol2="",
#    sphere=yes)

#tproject matchfake.tmp work.tmp "MAG COMAG CORATE"
#!!grep -v "#" work.tmp | sort | uniq > work1.tmp
#fields work1.tmp 2,3,1 > work2.tmp
#surfit work2.tmp
##tcalc work1.tmp MAG1 "-5.354+1.20281*C2-0.00461*C3"
##fields work1.tmp 1,5 | graph point+ mark=point

#tcalc matchfake.tmp MAG1 "-5.354+1.20281*COMAG-0.00461*CORATE"
#tcalc DEC14A_20140423_8595ac6-VR_addast.txt MAG1 "-5.354+1.20281*COMAG-0.00461*CORATE"

#tproject matchfake.tmp work.tmp ID
#!!grep -v "#" work.tmp | sort | uniq > work1.tmp
#tproject DEC14A_20140423_8595ac6-VR_addast.txt work.tmp ID
#!!grep -v "#" work.tmp | sort | uniq > work2.tmp

#j = 0; s5 = ""; delete addast_no.tmp,addast_yes.tmp
#fd1 = "work.tmp"; fd2 = "addast.txt"
#while (fscan(fd2,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,i,s14) != EOF) {
#    if (s1 == "#c") {
#        printf ("%s %s %s %s %s\n", s1, s2, s3, s4, s5, >> "addast_no.tmp")
#        printf ("%s %s %s %s %s\n", s1, s2, s3, s4, s5, >> "addast_yes.tmp")
#        next
#    }
#    ;
#    if (i > j) {
#        if (fscan(fd1, j) == EOF)
#	    j = 1000000
#	;
#    }
#    if (i == j)
#	printf ("%s %s %s %s %s %s %s %s %s %s %s %s %s %d %s\n",
#	    s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, i, s14,
#	    >> "addast_no.tmp")
#    else
#	printf ("%s %s %s %s %s %s %s %s %s %s %s %s %s %d %s\n",
#	    s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, i, s14,
#	    >> "addast_yes.tmp")
#    ;
#}
#fd1 = ""; fd2 = ""

mag1 = 20; mag2 = 26; rate1 = 60; rate2 = 110; k = 0; b1 = no
for (rate1=60; rate1<360; rate1+=75) {

rate2 = rate1 + 75
printf ("COMAG>=%.1f&&COMAG<%.1f&&CORATE>=%d&&CORATE<%d\n",
    mag1, mag2, rate1, rate2) | scan (line)
tselect ("addast_yes.tmp", "work3_yes.tmp", line)
tselect ("addast_no.tmp", "work3_no.tmp", line)
fields work3_yes.tmp 15 | uniq > mag_yes.dat
fields work3_no.tmp 15 | uniq > mag_no.dat
#fields addast_yes.tmp 8 | uniq > rate_yes.dat
#fields addast_no.tmp 8 | uniq > rate_no.dat

phist ("mag_yes.dat", z1=20., z2=26., binwidth=0.2, nbins=512,
    autoscale=yes, top_closed=no, hist_type="normal", listout=yes,
    title="Artificial Asteroids", xlabel="Mag", ylabel="Counts", wx1=INDEF,
    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=no,
    pattern="solid", device="stdgraph", > "addast_yes.hist")
phist ("mag_no.dat", z1=20., z2=26., binwidth=0.2, nbins=512,
    autoscale=yes, top_closed=no, hist_type="normal", listout=yes,
    title="Artificial Asteroids", xlabel="Mag", ylabel="Counts", wx1=INDEF,
    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=no,
    pattern="solid", device="stdgraph", > "addast_no.hist")
delete addast_both.hist
join addast_yes.hist addast_no.hist out=addast_both.hist
list = "addast_both.hist"; delete addast_prob.hist
while (fscan (list, x, i, x, j) != EOF) {
    j += i
    if (j == 0)
        next
    y = real (i) / j
    printf ("%4.1f %4.2f\n", x, y, >> "addast_prob.hist")
}
list = ""
k += 1
graph ("addast_prob.hist", wx1=20, wx2=26, wy1=0., wy2=1., wcs="logical",
    axis=1, transpose=no, pointmode=no, marker="box", szmarker=0.005,
    ltypes=k, colors=k, logx=no, logy=no, box=yes, ticklabels=yes,
    xlabel="Magnitude", ylabel="Probability", xformat="wcsformat", yformat="",
    title="Simulated Asteroids: 2014-04-23", lintran=no,
    p1=0., p2=0., q1=0., q2=1., vx1=0.,
    vx2=0., vy1=0., vy2=0., majrx=5, minrx=5, majry=6, minry=5,
    overplot=no, append=b1, device="stdgraph", round=no, fill=yes)
b1 = yes

}


#
#delete mc.dat
#phist ("mag_yes.dat", z1=20., z2=26., binwidth=0.2, nbins=512,
#    autoscale=yes, top_closed=no, hist_type="normal", listout=no,
#    title="Artificial Asteroids", xlabel="Mag", ylabel="Counts", wx1=INDEF,
#    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
#    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
#    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=no,
#    pattern="solid", device="stdgraph", >>G "mc.dat")
#
#phist ("mag_no.dat", z1=20., z2=26., binwidth=0.2, nbins=512,
#    autoscale=yes, top_closed=no, hist_type="normal", listout=no,
#    title="Artificial Asteroids", xlabel="Mag", ylabel="Counts", wx1=INDEF,
#    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
#    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
#    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=yes,
#    pattern="dashed", device="stdgraph", >>G "mc.dat")
#
#phist ("rate_yes.dat", z1=0., z2=400., binwidth=20, nbins=512,
#    autoscale=yes, top_closed=no, hist_type="normal", listout=no,
#    title="Artificial Asteroids", xlabel="Rate (arcsec/hr)", ylabel="Counts", wx1=INDEF,
#    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
#    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
#    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=no,
#    pattern="solid", device="stdgraph", >>G "mc.dat")
#
#phist ("rate_no.dat", z1=0, z2=400, binwidth=20, nbins=512,
#    autoscale=yes, top_closed=no, hist_type="normal", listout=no,
#    title="Artificial Asteroids", xlabel="Rate (arcsec/hr)", ylabel="Counts", wx1=INDEF,
#    wx2=INDEF, wy1=0., wy2=2000., logx=no, logy=no, round=no,
#    plot_type="box", box=yes, ticklabels=yes, majrx=5, minrx=5, majry=5,
#    minry=5, fill=yes, vx1=0., vx2=1., vy1=0., vy2=1., append=yes,
#    pattern="dashed", device="stdgraph", >>G "mc.dat")
#
##fields addast_yes.tmp 15,8 | uniq > both_yes.dat
##fields addast_no.tmp 15,8 | uniq > both_no.dat
#list = "both_yes.dat"
#while (fscan (list, x, y) != EOF) {
#    x = int (x * 5) / 5.
#    y = int (y / 20) * 20
#    printf ("%5.1f %3d\n", x, y, >> "both_yes1.dat")
#}
#list = ""

