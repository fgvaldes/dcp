# Start with a list of names in file "list"

string	root, id, grp, sec, ra, dec
real	mag

list = "list"; del doit[12].tmp
while (fscan (list, s1) != EOF) {
    match (s1, "*mpc.cat", print-) | scan (root, id, grp)
    printf ("%03d\n", int(grp)) | scan (grp)
    printf ("%s %s %s\n", id, root, grp, >> "doit1.tmp")
    mscexten (root//"_![AB]_mov.fits", extnam="?*"//grp//"_?*",
	lindex-, lname+, >> "doit2.tmp")
}
list = ""

list = "doit2.tmp"; del doit[12]_tmp.fits
while (fscan (list, s1) != EOF) {
    root = substr (s1, 1, strldx("[",s1)-1)
    sec = substr (s1, strldx("[",s1), strldx("]",s1))
    grp = substr (sec, 6, 9)
    if (imaccess("doit_tmp[0]")==NO) {
	imcopy (root//"[0]", "doit_tmp")
	imcopy (root//"[0]", "doit1_tmp")
    }
    imcopy (s1, "doit_tmp", v-)
    imcopy (s1, "doit1_tmp", v-)
    imreplace ("doit1_tmp"//sec, 0, low=INDEF, up=INDEF)
    hedit ("doit_tmp"//sec, "BPM", "doit1_tmp"//sec)
    imedit.value = int(grp); imedit.radius = 3
    bpmedit ("doit_tmp"//sec)
}
list = ""

list = "doit2.tmp"; del doit_tmp_*
while (fscan (list, s1) != EOF) {
    root = "doit_tmp"
    sec = substr (s1, strldx("[",s1), strldx("]",s1))
    grp = substr (sec, 6, 9)
    s2 = root//"_"//grp//".txt"
    aceall (root//sec, objmask="doit1_tmp"//sec, catalog=s2)
    tail (s2, nl=1) | scan (i, i, s1, x, x, ra, dec, mag) 
    if (nscan() == 8) {
	hedit (root//sec, "CORA", ra)
	hedit (root//sec, "CODEC", dec)
	hedit (root//sec, "COMAG", mag)
    }
    ;
}
;
list = ""

movdp (input="doit_tmp.fits")
