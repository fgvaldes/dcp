# MOVSQL -- Create SQL from tables.

procedure movsql (root)

string	root			{prompt="Root name for tables and sql"}
bool	del = no		{prompt="Delete from DB first?"}
bool	addobs = yes		{prompt="Include grp, obs, mpc entries?"}
string	checked = "NULL"	{prompt="Checked"}
string	movid = ""		{prompt="Only include this id"}

struct	*fd

begin

	int	i
	string	ds
	file	in, sql, tmp, tbl, tbl1, insert

	int	ival
	real	rval
	string	sval
	struct	lval

	# Get query parameters.
	in = root

	# Set SQL filename.
	sql = in // ".sql"
	if (access(sql))
	    delete (sql)

	# Set temp files.
	tmp = in // ".tmp"
	insert = in // "_insert.tmp"

	# Drop existing data if desired. This assumes the dataset name
	# has a particular relationship to the input root name.
	if (strldx('_',in) > stridx('-',in))
	    ds = substr (in, 1, strldx('_',in)-1)
	else
	    ds = in
#	if (del) {
#	    printf ("DELETE FROM MOVDS  WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("DELETE FROM MOVEXP WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("DELETE FROM MOVGRP WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("DELETE FROM MOVOBS WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("DELETE FROM MOVMPC WHERE dataset='%s';\n", ds, >> sql)
#	} else {
#	    printf ("-- DELETE FROM MOVDS  WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("-- DELETE FROM MOVEXP WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("-- DELETE FROM MOVGRP WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("-- DELETE FROM MOVOBS WHERE dataset='%s';\n", ds, >> sql)
#	    printf ("-- DELETE FROM MOVMPC WHERE dataset='%s';\n", ds, >> sql)
#	}

	# Dataset table.
	tbl = in // "_ds.cat"
	if (access(tbl) && movid == "") {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    if (del)
		printf ("DELETE FROM MOVDS WHERE dataset='%s';\n",
		    ds, >> sql)
	    else
		printf ("-- DELETE FROM MOVDS WHERE dataset='%s';\n",
		    ds, >> sql)
	    printf ("INSERT INTO MOVDS (\n", >> insert)
	    fd = tmp
	    for (j=1; fscan(fd,sval)!=EOF; j+=1) {
		if (j == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, rval); printf ("%g,\n",    rval, >> sql)
		i = fscan (fd, lval); printf ("'%s',\n",  lval, >> sql)
		i = fscan (fd, rval); rval*=15; printf ("%.6f,\n",   rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, lval); printf ("'%s',\n", lval, >> sql)
		i = fscan (fd, sval)
		    if (sval == "INDEF")
			printf ("NULL,\n", >> sql)
		    else
			printf ("timestamp '%s',\n",sval) |
			    translit ("STDIN", "-", del+, >> sql)
		i = fscan (fd, lval); printf ("'%s');\n", lval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	# Exposure table.
	tbl = in // "_exp.cat"
	if (access(tbl) && movid == "") {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    if (del)
		printf ("DELETE FROM MOVEXP WHERE dataset='%s';\n",
		    ds, >> sql)
	    else
		printf ("-- DELETE FROM MOVEXP WHERE dataset='%s';\n",
		    ds, >> sql)
	    printf ("INSERT INTO MOVEXP (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, rval); printf ("%g,\n",   rval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, rval); printf ("%d,\n",   rval, >> sql)
		i = fscan (fd, rval); printf ("%g);\n",  rval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	if (!addobs)
	    return
	;

	# Group table.
	tbl1 = in // "_grp.cat"
	if (movid == "")
	    tbl = tbl1
	else {
	    tbl = tbl1 // movid
	    tselect (tbl1, tbl, "movid=='"//movid//"'")
	}
	;
	if (access(tbl)) {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    if (del && movid == "")
		printf ("DELETE FROM MOVGRP WHERE dataset='%s';\n",
		    ds, >> sql)
	    else if (del && movid != "")
		printf ("DELETE FROM MOVGRP WHERE dataset='%s' AND MOVID='%s';\n",
		    ds, movid, >> sql)
	    else
		printf ("-- DELETE FROM MOVGRP WHERE dataset='%s';\n",
		    ds, >> sql)
	    printf ("INSERT INTO MOVGRP (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf (",%s", "checked", >> insert)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",  ival, >> sql)
		if (checked == "NULL")
		    printf ("%s);\n", checked, >> sql)
		else
		    printf ("'%s');\n", checked, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	# Observation table.
	tbl1 = in // "_obs.cat"
	if (movid == "")
	    tbl = tbl1
	else {
	    tbl = tbl1 // movid
	    tselect (tbl1, tbl, "movid=='"//movid//"'")
	}
	;
	if (access(tbl)) {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    if (del && movid == "")
		printf ("DELETE FROM MOVOBS WHERE dataset='%s';\n",
		    ds, >> sql)
	    else if (del && movid != "")
		printf ("DELETE FROM MOVOBS WHERE dataset='%s' AND MOVID='%s';\n",
		    ds, movid, >> sql)
	    else
		printf ("-- DELETE FROM MOVOBS WHERE dataset='%s';\n",
		    ds, >> sql)
	    printf ("INSERT INTO MOVOBS (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, rval); rval*=15; printf ("%.6f,\n",   rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.2f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, sval); printf ("'%s');\n", sval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	# MPC table.
	tbl1 = in // "_mpc.cat"
	tbl = tbl1
	if (movid == "")
	    tbl = tbl1
	else {
	    tbl = tbl1 // movid
	    tselect (tbl1, tbl, "movid=='"//movid//"'")
	}
	;
	if (access(tbl)) {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    if (del && movid == "")
		printf ("DELETE FROM MOVMPC WHERE dataset='%s';\n",
		    ds, >> sql)
	    else if (del && movid != "")
		printf ("DELETE FROM MOVMPC WHERE dataset='%s' AND MOVID='%s';\n",
		    ds, movid, >> sql)
	    else
		printf ("-- DELETE FROM MOVMPC WHERE dataset='%s';\n",
		    ds, >> sql)
	    printf ("INSERT INTO MOVMPC (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, lval); printf ("'%s');\n", lval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	sed ("-i", "s+.INDEF.+NULL+", sql)
	sed ("-i", "s+INDEF+NULL+", sql)
end
