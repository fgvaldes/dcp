# MOVSQLU -- Update merged tracklets.

procedure movsqlu (root)

string	root			{prompt="Root name for tables and sql"}
file	merge = "merge.dat"	{prompt="Input merge information"}

struct	*fd, *fd1

begin

	file	in, sql, co, tmp, tmp1, tmp2, tbl, upd
	int	i, grp, nobs, digest
	real	rate, ratec, pa, mag
	string	ds, movid

	int	ival
	real	rval
	string	sval
	struct	lval

	# Get query parameters.
	in = root

	# Set SQL filename.
	sql = in // ".sql"
	if (access(sql))
	    delete (sql)

	# Set temp files.
	tmp = in // ".tmp"
	tmp1 = in // "1.tmp"
	tmp2 = in // "2.tmp"
	upd = in // "_upd.sql"

	delete (upd, >& "dev$null")

	fields (merge, "2,3") | sort | unique (> tmp2)

	fd1 = tmp2
	while (fscan (fd1, co, grp) != EOF) {
	    if (in != substr(co, 1, strstr("_mov",co)-1))
		next

	    # Group table.
	    tbl = in // "_grp.cat"
	    if (access(tbl)) {
	        tselect (tbl, tmp1, "groupid="//grp)
		tdump (tmp1, cdfile="", pfile="", datafile=tmp)
		delete (tmp1)
		fd = tmp
		while (fscan(fd, ds, movid, ival, nobs, rate, ratec, pa, mag,
		    digest) != EOF) {

		    printf ("DELETE FROM MOVOBS WHERE movid='%s';\n",
		        movid, >> sql)
		    printf ("DELETE FROM MOVMPC WHERE movid='%s';\n\n",
		        movid, >> sql)

		    printf ("UPDATE MOVGRP SET nobs=%d WHERE movid='%s';\n",
		        nobs, movid, >> sql)
		    printf ("UPDATE MOVGRP SET rate=%.1f WHERE movid='%s';\n",
		        rate, movid, >> sql)
		    printf ("UPDATE MOVGRP SET ratec=%.1f WHERE movid='%s';\n",
		        ratec, movid, >> sql)
		    printf ("UPDATE MOVGRP SET pa=%.1f WHERE movid='%s';\n",
		        pa, movid, >> sql)
		    printf ("UPDATE MOVGRP SET mag=%.1f WHERE movid='%s';\n",
		        mag, movid, >> sql)
		    printf ("UPDATE MOVGRP SET digest=%d WHERE movid='%s';\n",
		        digest, movid, >> sql)
		}
		fd = ""
	    }

	    # Observation table.
	    tbl = in // "_obs.cat"
	    if (access(tbl)) {
	        tselect (tbl, tmp1, "groupid="//grp)
		tdump (tmp1, cdfile=tmp, pfile="", datafile="")
		printf ("INSERT INTO MOVOBS (\n", >> upd)
		fd = tmp
		for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		    if (i == 1)
			printf ("%s", sval, >> upd)
		    else
			printf (",%s", sval, >> upd)
		}
		fd = ""; delete (tmp)
		printf ("\n) VALUES (\n", >> upd)

		tdump (tmp1, cdfile="", pfile="", datafile="STDOUT") |
		    words (> tmp)
		delete (tmp1)
		fd = tmp
		while (fscan (fd, sval) != EOF) {
		    concat (upd, sql, append+)
					  printf ("'%s',\n",  sval, >> sql)
		    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		    i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		    i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		    i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		    i = fscan (fd, rval); rval*=15; printf ("%.6f,\n",   rval, >> sql)
		    i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		    i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		    i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		    i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		    i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		    i = fscan (fd, sval); printf ("'%s');\n", sval, >> sql)
		}
		fd = ""; delete (upd//","//tmp)
	    }

	    # MPC table.
	    tbl = in // "_mpc.cat"
	    if (access(tbl)) {
	        tselect (tbl, tmp1, "groupid="//grp)
		tdump (tmp1, cdfile=tmp, pfile="", datafile="")
		printf ("INSERT INTO MOVMPC (\n", >> upd)
		fd = tmp
		for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		    if (i == 1)
			printf ("%s", sval, >> upd)
		    else
			printf (",%s", sval, >> upd)
		}
		fd = ""; delete (tmp)
		printf ("\n) VALUES (\n", >> upd)

		tdump (tmp1, cdfile="", pfile="", datafile="STDOUT") |
		    words (> tmp)
		delete (tmp1)
		fd = tmp
		while (fscan (fd, sval) != EOF) {
		    concat (upd, sql, append+)
					  printf ("'%s',\n",  sval, >> sql)
		    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		    i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		    i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		    i = fscan (fd, lval); printf ("'%s');\n", lval, >> sql)
		}
		fd = ""; delete (upd//","//tmp)
	    }
	}
	fd1 = ""; delete (tmp2)
end
