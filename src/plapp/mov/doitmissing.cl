files ("../input/*missing", > "missing.list")

list = ""
list = "missing.list"
while (fscan (list, s3) != EOF) {
    if (stridx ("#", s3) > 0)
        next
    ;
    s2 = substr (s3, 1, strstr("missing",s3)-1)
    s1 = substr (s2, strldx("/",s2)+1, strldx(".",s2)-1)

    print (s1)

    cd (s1//"/data")
    retrieve
    cd ../..

    delete (s3)
    path (s1//"/data/*cat,"//s1//"/data/*fits", > s2)
    touch (s2//"trig")
}
list = ""; delete ("missing.list")
