# MPCUPACK -- Unpack MPC identifiers.
# This will also pass through already unpacked identifiers.

# If not sql then the input and output are just MPC identifiers.
# Otherwise the input is movid and MPC identifiers and the output
# is an sql update script.

procedure mpcunpack (input)

string	input		{prompt="Input name or list"}
bool	sql = yes	{prompt="Make sql update"}

struct	*fd

begin
	string	digit = "0123456789"
	string	alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	string	code = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	
	file	in
	struct	rec, packed
	string	id, c, a, year, seq, num
	int	i, j
	bool	inlist

	in = input
	inlist = access(in)
	
	if (inlist)
	    fd = in
	for (j=1; (inlist||j==1); j+=1) {
	    if (inlist) {
	 	if (fscan (fd, rec) == EOF) {
		    fd = ""
		    break
		}
	    } else
	        rec = in
	    if (sql) {
	        if (fscan (rec, id, packed) == EOF)
		    break
	    } else
	        packed = rec

	    c = substr (packed, 1, 1)
	    num = substr (packed, 2, 99)
	
	    i = stridx (c, code)
	    if (i < 10) {
		if (sql)
		    printf ("update movgrp set mpcid='%s' where movid='%s';\n",
			packed, id)
		else
		    printf ("%s\n", packed)
		next
	    }
	
	    if (stridx (alpha, num) == 0) {
		if (sql)
		    printf ("update movgrp set mpcid='%d%s' where movid='%s';\n",
			i, num, id)
		else
		    printf ("%d%s\n", i, num)
		next
	    }
	
	    printf ("%d%s\n", i, substr(packed,2,3)) | scan (year)
	    seq = substr (packed, 4, 4) // substr (packed, 7, 7)
	
	    i = stridx (substr(packed,5,5), code)
	    printf ("%d%s\n", i, substr(packed,6,6)) | scan (num)
	    if (num != "00") {
	        if (substr(num,1,1) == '0')
		    num = substr(num,2,99)
	        seq += num
	    }
	    if (sql)
		printf ("update movgrp set mpcid='%s %s' where movid='%s';\n",
		    year, seq, id)
	    else
		printf ("%s %s\n", year, seq)

	}
end
