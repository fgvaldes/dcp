# Redo data products.

files ("MOV*[SM]", > "list")
files ("MOV*[SM]-dts", >> "list")
files ("TNO*[SM]", >> "list")
files ("TNO*[SM]-dts", >> "list")

list = "list"
while (fscan (list, s1) != EOF) {
    s1 += "/"
    cd (s1)
    s2 = ""; files ("*_A_mov.fits") | scan (s2)
    if (s2 != "") {
        delete ("*_A_*cat,*_A_*txt")
	movdp (in=s2, duration=1155)
    }
    ;
    s2 = ""; files ("*_B_mov.fits") | scan (s2)
    if (s2 != "") {
        delete ("*_B_*cat,*_B_*txt")
	movdp (in=s2, duration=1155)
    }
    ;
    cd ..
}
list = ""
