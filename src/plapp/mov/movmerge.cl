# Merge tracklets.

procedure movmerge (input, output, groupid, expid)

file	input		{prompt="Extension to copy"}
file	output		{prompt="Targe cutout file"}
int	groupid		{prompt="Target group ID"}
int	expid		{prompt="Target exposure ID"}

begin
	file	in, out
	int	grp, exp

	file	extns, coimage
	string	extn, cora, codec
	real	ltv1, ltv2, crpix1, crpix2, cox, coy, copx, copy, comag

	# Query parameters.
	in = input
	out = output
	grp = groupid
	exp = expid

	if (strstr(".fits",out) > 0)
	    out = substr (out, 1, strstr(".fits",out)-1)

	extns = out // ".extns"

	# Create look-up for extensions if needed.
	if (access (extns) == NO)
	    mscselect (out, "EXTNAME,COGRPID,COEXPID", > extns)

	# Find the target extension.
	extn = "unknown"
	tselect (extns, "STDOUT", "c2="//grp//"&&c3="//exp) | scan (extn)
	if (extn == "unknown") {
	    printf ("ERROR\n")
	    return
	}
	printf ("%s[%s][*,*]\n", out, extn) | scan (out)

	# Get the keywords to update.
	ltv1 = 0; hselect (in, "LTV1", yes) | scan (ltv1)
	ltv2 = 0; hselect (in, "LTV2", yes) | scan (ltv2)
	crpix1 = 0; hselect (in, "CRPIX1", yes) | scan (crpix1)
	crpix2 = 0; hselect (in, "CRPIX2", yes) | scan (crpix2)
	hselect (in, "COIMAGE,COX,COY,COPX,COPY,CORA,CODEC,COMAG", yes) |
	    scan (coimage, cox, coy, copx, copy, cora, codec, comag)

	# Update keywords.
	hedit (out, "LTV1", ltv1, add+)
	hedit (out, "LTV2", ltv2, add+)
	hedit (out, "CRPIX1", crpix1, add+)
	hedit (out, "CRPIX2", crpix2, add+)
	hedit (out, "COIMAGE", coimage)
	hedit (out, "COX", cox)
	hedit (out, "COY", coy)
	hedit (out, "COPX", copx)
	hedit (out, "COPY", copy)
	hedit (out, "CORA", cora)
	hedit (out, "CODEC", codec)
	hedit (out, "COMAG", comag)

	# Update pixels.
	imcopy (in, out, v-)
end
