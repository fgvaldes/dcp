bool	dp = yes
dp.p_mode = "a"

files ("../input/*done", > "done.list")

list = ""
list = "done.list"
while (fscan (list, s3) != EOF) {
    if (stridx ("#", s3) > 0)
        next
    ;
    s2 = substr (s3, 1, strstr("done",s3)-1)
    s1 = substr (s2, strldx("/",s2)+1, strldx(".",s2)-1)

    cd (s1)
    print (s1)
    files ("*sqldone") | count | scan (i)
    if (i > 0) {
        cd ..
        next
    }
    ;
    files ("*sql") | count | scan (i)
    if (i > 0) {
        files ("*sql") | scan (s2)
	printf ("Archive %s\n",  s2)
	psqdb (s2)
	rename (s2, s2//"done")
	rename ("../../input/"//s1//".movdone",
	    "../../input/"//s1//".archived")
        cd ..
        next
    }
    ;
    movreview
    if (dp) {
        movdp
        files ("*sql") | scan (s2)
	printf ("Archive %s\n",  s2)
	psqdb (s2)
	rename (s2, s2//"done")
	if (access("../../input/"//s1//".movdone"))
	    rename ("../../input/"//s1//".movdone",
	        "../../input/"//s1//".archived")
	;
    }
    ;
    cd ..
}
list = ""
