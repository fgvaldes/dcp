# Merge tracklets that have different combinations of common measurements.
# Input is from duplicates.sql query.

procedure domerge ()

file	merge = "merge.dat"	{prompt="Input merge information"}
bool	updco = yes		{prompt="Update cutout files"}
bool	upddp = yes		{prompt="Update data products?"}
bool	upddb = yes		{prompt="Update database?"}
bool	decamnum = yes		{prompt="Use EXPNUM for IDs?"}
string	work = "domerge"	{prompt="Root for temporary files"}

struct	*fd

begin
	string	stat
	file	in,  mov, out, work1
	int	grp, expid

	# Set input/output.
	work1 = work // ".tmp"

	# Update each cutout in the merge list.
	fd = merge; touch (work1)
	while (fscan (fd, in, out, grp, expid) != EOF) {
	    mov = substr (in, 1, strldx("[",in)-1)
	    if (access(mov//".fits")==NO) {
		printf ("Input cutout file not found (%s)\n", mov)
		next
	    }
	    ;
	    if (out != mov && access(out//".fits")==NO) {
		printf ("Output cutout file not found (%s)\n", out)
		next
	    }
	    ;

	    # Do the merge.
	    if (updco) {
		printf ("UPDATE %s %d %d: ", out, grp, expid
		stat="OK"; movmerge (in, out, grp, expid) | scan (stat)
		printf ("  %s\n", stat)
		if (stat == "ERROR")
		    next
	    }
	    ;
	    print (out, >> work1)
	}
	fd = ""

	# For each unique updated cutout file update the database.
	sort (work1) | unique (> work1)
	fd = work1
	while (fscan (fd, in) != EOF) {
	    out = substr (in, 1, strldx("_",in)-1)
	    if (upddp) {
		movdp (in=in, decamnum=decamnum, fast+, icat-)
		movsqlu (out)
	    }
	    ;
	    if (upddb)
		adb ("-q", "-f", out//".sql")
	    ;
	}
	fd = ""; delete (work1)
end
