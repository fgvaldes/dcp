# MOVALERT -- Find important candidates.

procedure movalert (root)

string	root			{prompt="Rootname for files"}
int	threshold = 95		{prompt="MPC interest threshold"}

string	comment = ""		{prompt="Comment"}

struct	*fd

begin
	int	i
	file	digest, grp, mpc, out, tmp
	string	id
	real	rms, interest, rate, pa
	struct	record, rest

	# Set names for files.
	i = strstr ("_mov", root) - 1
	if (i > 0)
	    root = substr (root, 1, i)

	digest = root // "_digest2.txt"
	grp = root // "_grp.cat"
	mpc = root // "_mpc.cat"
	tmp = root // "_tmp.cat"
	out = root // "_alert.txt"

	# Check digest file.
	if (!access(digest))
	    return
	if (access(out))
	    delete (out)

	# Find high interest records.
	fd = digest
	while (fscan (fd, record) != EOF) {
	    if (fscan (record, id, rms, interest, rest) < 4)
	        next
	    if (interest < threshold)
	        next
	    if (!access(tmp)) {
		printf ("Subject: Interesting Moving Object Alerts\n\n", >> out)
		tjoin (grp, mpc, tmp, "groupid", "groupid")
		if (comment != "")
		    printf ("%s\n\n", comment, >> out)
		printf ("Dataset: %s\n\n", root, >> out)
		printf ("%6s %4s %4s %s %s %s %s %s\n", "Rate", "PA", "RMS",
		    "Int", "NEO", "N22", "N18", "Other Possibilities", >> out)
	    }
	    match (id, tmp) | fields ("STDIN", "4,5") | uniq | scan (rate, pa)
	    printf ("%6.1f %4d %4.2f %3d %s\n",
	        rate, pa, rms, interest, rest, >> out)
	    match (id, tmp) | fields ("STDIN", "10", >> out)
	    printf ("\n", record, >> out)
	}
	fd = ""

	if (access(tmp))
	    delete (tmp)

	if (access(out))
	    concat (out)
end
