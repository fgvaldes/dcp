include	<math.h>
include	<fset.h>

# T_ECLIPTIC -- convert between equatorial and ecliptic coordinates.

procedure t_ecliptic ()

char	fname[SZ_FNAME]
int	filelist, units, prt_coords, in_coords
bool	streq(), clgetb()
int	clpopni(), clgfil(), btoi(), clgwrd()

begin
	# Input can come from the standard input, a file, or a list of files.
	# The following procedure makes both cases look like a list of files.

	filelist = clpopni ("input")

	# Get output option
	
	in_coords = clgwrd ("in_coords", fname, SZ_FNAME,
	    "|equatorial|ecliptic|")
	units = clgwrd ("units", fname, SZ_FNAME, "|hours|degrees|")
	prt_coords = btoi (clgetb ("print_coords"))

	# Process each coordinate list.  If reading from the standard input,
	# set up the standard output to flush after every output line, so that
	# converted coords come back immediately when working interactively.

	while (clgfil (filelist, fname, SZ_FNAME) != EOF) {
	    if (streq (fname, "STDIN"))
		call fseti (STDOUT, F_FLUSHNL, YES)
	    else
		call fseti (STDOUT, F_FLUSHNL, NO)
	    switch (in_coords) {
	    case 1:
		call in_equatorial (STDOUT, fname, units, prt_coords)
	    case 2:
	        call in_ecliptic (STDOUT, fname, units, prt_coords)
	    }
	}

	call clpcls (filelist)
end


# IN_EQUITORIAL -- convert a list of equatorial coordinates read from the
# named file to ecliptic coordinates, writing the results on the output file.  

procedure in_equatorial (out, listfile, units, prt_coords)

int	out				# output stream
char	listfile[SZ_FNAME]		# input file
int	units				# units 
int	prt_coords			# print coordinates in output file?

int	in
double	ra, dec, mjd, gamma, beta
int	fscan(), nscan(), open()
errchk	open, fscan, printf

begin
	in = open (listfile, READ_ONLY, TEXT_FILE)

	# Read successive RA,DEC coordinate pairs from the standard input,
	# converting and printing the result on the standard output.

	while (fscan (in) != EOF) {
	    call gargd (ra)
	    call gargd (dec)
	    call gargd (mjd)

	    switch (nscan()) {
	    case 2:
		mjd = 51545
	    case 3:
	    default:
		call eprintf ("Bad entry in coordinate list\n")
		next
	    }

	    switch (units) {
	    case 1:
		ra = ra*15D0
		if (prt_coords == YES ) {
		    call fprintf (out, "%13.2H %12.1h")
			    call pargd (ra)
			    call pargd (dec)
		}
	    case 2:
		if (prt_coords == YES ) {
		    call fprintf (out, "%13.4f %9.4f")
			    call pargd (ra)
			    call pargd (dec)
		}
	    }

	    ra = DEGTORAD(ra)
	    dec = DEGTORAD(dec)
	    call slEQEC (ra, dec, mjd, gamma, beta)
	    gamma = RADTODEG(gamma)
	    beta = RADTODEG(beta)
		
	    call fprintf (out, "%13.4f %9.4f" )
		call pargd (gamma)
		call pargd (beta)

	    call fprintf (out, "\n")
	}

	call close (in)
end


# IN_ECLIPTIC -- convert a list of ecliptic coordinates read from the
# named file to equatorial coordinates, writing the results on the output
# file.  

procedure in_ecliptic (out, listfile, units, prt_coords)

int	out				# output stream
char	listfile[SZ_FNAME]		# input file
int	units			# RA units 
int	prt_coords			# print coordinates in output file?

int	in
double	ra, dec, mjd, gamma, beta
int	fscan(), nscan(), open()
errchk	open, fscan, printf

begin
	in = open (listfile, READ_ONLY, TEXT_FILE)

	# Read successive RA,DEC coordinate pairs from the standard input,
	# converting and printing the result on the standard output.

	while (fscan (in) != EOF) {
	    call gargd (gamma)
	    call gargd (beta)
	    call gargd (mjd)

	    switch (nscan()) {
	    case 2:
		mjd = 51545
	    case 3:
	    default:
		call eprintf ("Bad entry in coordinate list\n")
		next
	    }

	    # Call routine to perform conversion and write to the output.


	    if (prt_coords == YES ) {
	        call fprintf (out, "%13.4f %9.4f" )
		    call pargd (gamma)
		    call pargd (beta)
            }

	    gamma = DEGTORAD(gamma)
	    beta = DEGTORAD(beta)
	    call slECEQ (gamma, beta, mjd, ra, dec)
	    ra = RADTODEG (ra)
	    dec = RADTODEG (dec)
		
	    switch (units) {
	    case 1:
		call fprintf (out, "%13.2H %12.1h")
		    call pargd (ra)
		    call pargd (dec)
	    case 2:
		call fprintf (out, "%13.4f %9.4f")
		    call pargd (ra)
		    call pargd (dec)
	    }

	    call fprintf (out, "\n")
	}

	call close (in)
end
