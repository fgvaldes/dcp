# MPCMAGFIX -- This is a quick script to adjust detection magnitudes
# in MPC records.  The ZP file contains the STB numbers for those
# calibrated with PS1.  So only those not in the file are adjusted.

procedure mpcmagfix (mpcfile)

file	mpcfile			{prompt="MPC file"}
file	photref = "photref.dat"	{prompt="Photref file"}
real	dmag = 0.155		{prompt="Magnitude correction"}
bool	update = no		{prompt="Update?"}

struct	*fd

begin
	file	mpc, phtref
	string	movid, stbid, ref
	int	nin, nmod
	real	mag
	struct	rec

	# Set parameters.
	mpc = mpcfile
	phtref = photref

	if (access(mpc//"ORIG")) {
	    if (update)
		rename (mpc//"ORIG", mpc)
	    else
	        return
	}

	if (!access(mpc) || !access(phtref))
	    return

	rename (mpc, mpc//"ORIG")

	fd = mpc//"ORIG"; nin = 0; nmod = 0
	while (fscan (fd, rec) != EOF) {
	    if (substr(rec, 1, 1) != " ") {
	        print (rec, >> mpc)
		next
	    }
	    if (substr (rec, 66, 66) == " ") {
	        print (rec, >> mpc)
		next
	    }
	    nin += 1
	    movid = substr (rec, 6, 12)
	    movid (movid=movid, dir="from", v-)
	    ref = ""; match (movid.stbid, phtref) | scan (stbid, ref)
	    if (ref == "" || ref == "PS1") {
	        print (rec, >> mpc)
		next
	    }
	    nmod += 1
	    mag = real (substr (rec, 66, 69))
	    mag += dmag
	    printf ("%s%4.1f%s\n",
	        substr(rec,1,65), mag, substr(rec,70,80), >> mpc)
	}
	fd = ""

	printf ("%s: Modified %d / %d\n", mpc, nmod, nin)
end
