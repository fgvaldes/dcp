# ADDFIX -- Fix addast.txt files.
#
# Compute rate, ratec, nobs
# This requires the name to be related to the dataset name.

procedure addfix (input)

file	input			{prompt="Input file"}

struct	*fd

begin
	file	in, tmp
	string	ds
	int	last, nobs
	int	row, row1, row2, ccd, grp, exp, i
	real	ra, dec, x, y, mag, rate, pa, tsep, opp, ratec
	real	ra1, ra2, dra, dec1, dec2, ddec, tsep1, tsep2, dt, dpa, pa1

	in = input
	tmp = "addfix.tmp"

	# Expand in case of wildcards.  Only the first is used.
	files (in) | scan (in)
	printf ("ADDFIX: %s\n", in)

	# Make a backup if necessary.
	if (access(in//"BAK") == NO)
	    copy (in, in//"BAK")

	# Setup up for rate, ratec, and pa.
	tchcol (in, "CORATE", "CORATE", "%6.2f", "", verbose-)
	tcalc (in, "CORATEC", "CORATE", datatype="real", colf="%6.2f")
	tchcol (in, "COPA", "COPA", "%4d", "", verbose-)

	ds = substr(in, 1, strldx("_",in)) // "F_ds.cat"
	tabpar (ds, "OPPOSITION", 1); opp = real (tabpar.value)
	opp = min (abs(opp), 85.)
	opp = dcos (opp)

	tstat (in, "COPA", out="")
	if (tstat.vmin > 0) {
	    tabpar (ds, "RA", 1); ra1 = real(tabpar.value)
	    tabpar (ds, "DEC", 1); dec1 = real(tabpar.value)
	    movmotion (ra1, dec1, 60., rate=10., pa=0) | scan (ra2, dec2)
	    movmotion (ra1, dec1, 60., ra2=ra2, dec2=dec2) |
	        scan (rate, pa, dpa) 
	} else
	    dpa = 0

	# Sort by group ID.
	tsort (in, "COGRPID,COEXPID")

	# Loop through each tracklet.
	fd = in; row2 = 0; last = 0
	while ( fscan (fd, ds, ccd, ra, dec, x, y, mag, rate, pa, grp, exp,
	    i, tsep) != EOF) {
	    if (substr(ds,1,1) == '#')
	        next
	    if (grp != last) {
	        if (last != 0) {
		    dra = 3600 * 15 * (ra2 - ra1) * dcos ((dec2 + dec1) / 2)
		    ddec = 3600 * (dec2 - dec1)
		    dt = (tsep2 - tsep1) / 3600.
		    rate = sqrt (dra**2+ddec**2) / max(0.01,dt)
		    ratec = rate / opp
		    pa1 = dpa - pa1
		    if (pa1 < -180)
		        pa1 += 360
		    if (pa1 > 180)
		        pa1 -= 180
		    printf ("%d %d %d %d %.2f %.2f %d\n",
		        row1, row2, last, nobs, rate, ratec, pa1, >> tmp)
		}
		row1 = row2 + 1
	        nobs = 0
		ra1 = ra
		dec1 = dec
		tsep1 = tsep
		pa1 = pa

	    }
	    row2 += 1
	    nobs += 1
	    last = grp
	    ra2 = ra
	    dec2 = dec
	    tsep2 = tsep
	}
	ra1 = 3600 * 15 * (ra2 - ra1) * dcos ((dec2 + dec1) / 2)
	dec1 = 3600 * (dec2 - dec1)
	tsep1 = tsep2 - tsep1
	rate = sqrt (ra1*ra1+dec1*dec1) / max(1.,tsep1)
	ratec = rate / opp
	pa1 = dpa - pa1
	if (pa1 < -180)
	    pa1 += 360
	if (pa1 > 180)
	    pa1 -= 180
	printf ("%d %d %d %d %.2f %.2f %d\n",
	    row1, row2, last, nobs, rate, ratec, pa1, >> tmp)
	fd = ""

	if (access(tmp)) {
	    fd = tmp
	    while (fscan (fd, row1, row2, grp, nobs, rate, ratec, pa) != EOF) {
		for (row=row1; row<=row2; row+=1) {
		    partab (nobs, in, "CONOBS", row)
		    partab (rate, in, "CORATE", row)
		    partab (ratec, in, "CORATEC", row)
		    if (dpa != 0)
			partab (pa, in, "COPA", row)
		}
	    }
	    fd = ""; delete (tmp)
	}
	    
end
