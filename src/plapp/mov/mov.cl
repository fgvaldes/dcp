#{ MOV -- MOV Package

# Load dependent packages.
plot
nfextern
ace
aceproto

cl< "plapp$lib/zzsetenv.def"
package mov, bin = plappbin$

task	$adb		= "$!adb"
task	adbreview	= "movsrc$adbreview.cl"
task	$adbpairs	= "movsrc$adbpairs.cl"
task	$adbmpcsubid	= "$!adbmpcsubid"

set	movsrc		= "mov$"

task	mkcographic	= "movsrc$mkcographic.cl"
task	movreview	= "movsrc$movreview.cl"
task	movdp		= "movsrc$movdp.cl"
task	movimov		= "movsrc$movimov.cl"
task	movmerge	= "movsrc$movmerge.cl"
task	domerge		= "movsrc$domerge.cl"
task	movsql		= "movsrc$movsql.cl"
task	addfix		= "movsrc$addfix.cl"
task	addsql		= "movsrc$addsql.cl"
task	addupdmag	= "movsrc$addupdmag.cl"
task	movsqlu		= "movsrc$movsqlu.cl"
task	movmotion	= "movsrc$movmotion.cl"
task	movalert	= "movsrc$movalert.cl"
task	movckdup	= "movsrc$movckdup.cl"
task	suncoord	= "movsrc$suncoord.cl"
task	movid		= "movsrc$movid.cl"
task	dorev		= "movsrc$dorev.cl"
task	doimov		= "movsrc$doimov.cl"
task	adbingest	= "movsrc$adbingest.cl"
task	dts2mov		= "home$dts2mov.cl"

task	psqdb		= "$!psqdb < $(1)"
task	$movdisp	="$movsrc$Proto/mefdisp.cl"
task	$retrieve	= "home$retrieve.cl"
task	$doitdone	= "home$doitdone.cl"
task	$doitmissing	= "home$doitmissing.cl"
task	$doitclean	= "home$doitclean.cl"
task	$doit		= "home$doit.cl"
task	mkasteroids	= "movsrc$mkasteroids.cl"
task	mkranast	= "movsrc$mkranast.cl"
task	$movacsort	= "$!grep -hv '#' `cat $(1)` | sort -n -k 4 -k1,1 > $(2)"
task	movacmerge	= "movsrc$movacmerge.cl"
task	$movsrtsel	= "$!grep -v '#' $(1) | grep -v INDEF | tr -s ' ' '\t' | cut -f 2,9,10,11,14 | sort -n -k 1,1 -k 5,5"
task	movselect	= "movsrc$movselect.cl"
task	movmail		= "movsrc$movmail.cl"
task	allreview	= "movsrc$allreview.cl"
task	mpc_match	= "movsrc$mpc_match.cl"
task	addastprob	= "movsrc$addastprob.cl"
task	mpcmagfix	= "movsrc$mpcmagfix.cl"
task	mpcunpack	= "movsrc$mpcunpack.cl"

task	$digest2	= "$!digest2 -p $NHPPS_PIPEAPPSRC/plapp/mov/digest2.c.0.15 $(1)"

cache	movid
cache	suncoord
cache	movreview

# Proto

#task	dorev		= "movsrc$dorev.cl"
#task	$dorevB		= "movsrc$dorevB.cl"
task	sendit		= "$!/shared/deccp/DECCP/src/plapp/mov/sendit"

task	ecliptic	= "movsrc$x_plapp.e"

clbye
