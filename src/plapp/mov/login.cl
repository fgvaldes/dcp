plot
nfextern
ace
aceproto
set movsrc = "NHPPS_PIPEAPPSRC$/mov/iraf/"
task movreview = "movsrc$movreview.cl"
task movdp = "movsrc$movdp.cl"
task movmotion = "movsrc$movmotion.cl"
task movalert = "movsrc$movalert.cl"
task movckdup = "movsrc$movckdup.cl"
task $movdisp="$movsrc$Proto/mefdisp.cl"
task dts2mov = "home$dts2mov.cl"
task suncoord = "movsrc$suncoord.cl"
task movid = "movsrc$movid.cl"
task psqdb = "$!psqdb < $(1)"
task $retrieve = "home$retrieve.cl"
task $doitdone = "home$doitdone.cl"
task $doitmissing = "home$doitmissing.cl"
task $doitclean = "home$doitclean.cl"
task $doit = "home$doit.cl"
task mkasteroids = "movsrc$mkasteroids.cl"
task mkranast = "movsrc$mkranast.cl"
task $movacsort = "$!grep -hv '#' `cat $(1)` | sort -n -k 4 -k1,1 > $(2)"
task movacmerge = "movsrc$movacmerge.cl"
task $movsrtsel = "$!grep -v '#' $(1) | grep -v INDEF | tr -s ' ' '\t' | cut -f 2,9,10,11,14 | sort -n -k 1,1 -k 5,5"
task movselect = "movsrc$movselect.cl"
task movmail = "movsrc$movmail.cl"
task allreview = "movsrc$allreview.cl"
task mpc_match = "movsrc$mpc_match.cl"
task addastprob = "movsrc$addastprob.cl"
task mpcmagfix = "movsrc$mpcmagfix.cl"

task $digest2 = "$!digest2 -p $NHPPS_PIPEAPPSRC/mov/digest2.c.0.15 $(1)"

cache	movid

keep
