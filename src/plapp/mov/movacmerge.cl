# Merge two acecluster catalogs.

# The following command has to be defined.
# task $movacsort = "$!grep -hv '#' `cat $(1)` | sort -n -k 4 -k1,1 > $(2)"

# Assumes file clobber is on and delete verify is off.

procedure movacmerge (icats, ocat)

string	icats			{prompt="List of catalogs"}
file	ocat			{prompt="Output catalog"}
file	tmp = "movacmerge"	{prompt="Root for working files"}

struct	*fd, *fd1

begin
	file	ic, oc, t1, t2, t3
	int	n1, n2, n3, ln1, ln2, ln3, nout
	struct	rec, lrec
	bool	ckdup

	# Set working files.
	t1 = tmp // "1.tmp"
	t2 = tmp // "2.tmp"
	t3 = tmp // "3.tmp"
	delete (tmp//"[123].tmp")

	# Set input and output.
	files (icats) | count | scan (n1)
	if (n1 < 1)
	    return
	files (icats, > t2)
	head (t2, nl=1) | scan (ic)
	oc = ocat
	if (access(oc))
	    delete (oc)

	# Concatenate and sort input catalogs without headers.
	movacsort (t2, t1); delete (t2)

	# Eliminate duplicates and renumber.
	fd = t1; ln1 = 0; lrec = ""; nout = 0
	while (fscan (fd, n1, n2, n3, rec) != EOF) {
	    if (n1 == ln1)
	        next
	    if (rec != lrec)
		nout += 1
	    printf ("%11d %11d %11d %s\n", n1, nout, n3, rec, >> t2)
	    ln1 = n1
	    lrec = rec
	}
	fd = ""; delete (t1)

	# Eliminate groups with common members.
	sort (t2, col=1, num+, > t1); delete (t2)
	fd = t1; ln1 = 0
	while (fscan (fd, n1, n2, n3) != EOF) {
	    if (n1 == ln1) {
		if (n3 < ln3)
		    print (n2, >> t2)
		else if (ln3 < n3)
		    print (ln2, >> t2)
		else
		    print (min(ln2,n2), max(ln2,n2), >> t2)
	    }
	    ln1 = n1
	    ln2 = n2
	    ln3 = n3
	}
	fd = ""

	if (access(t2)) {
	    sort (t1, col=2, num+) | concat ("STDIN", > t1)
	    sort (t2, num+) | unique ( > t2)
	    fd = t1; fd1 = t2; ln2 = 0; ln3 = 0; ckdup = NO
	    while (fscan (fd, n1, n2, n3, rec) != EOF) {
		if (n2 > ln2) {
		    ln3 = 0
		    if (fscan (fd1, ln2, ln3) == EOF)
		        ln2 = 999999
		}
	        if (n2 == ln2) {
		    if (ln3 != 0) {
			printf ("%11d %11d %11d %s\n", n1, ln3, n3, rec, >> t3)
			ckdup = YES
		    }
		} else
		    printf ("%11d %11d %11d %s\n", n1, n2, n3, rec, >> t3)
	    }
	    fd = ""; delete (t1)
	    fd1 = ""; delete (t2)
	    if (ckdup) {
		sort (t3, col=1, num+) | concat ("STDIN", > t3)
		fd = t3; ln1 = 0
		while (fscan (fd, n1, n2, n3, rec) != EOF) {
		    if (n1 != ln1)
			printf ("%11d %11d %11d %s\n", n1, n2, n3, rec, >> t1)
		    ln1 = n1
		}
		fd = ""; delete (t3)
	    } else
	        rename (t3, t1)
	}

	# Create output.
	fd = ic
	while (fscan (fd, rec) != EOF) {
	    if (substr(rec,1,1) != "#")
	        break
	    print (rec, >> oc)
	}
	fd = ""
	sort (t1, col=2, num+, >> oc)
	delete (t1)
end
