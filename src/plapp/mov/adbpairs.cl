# Do the pair reconcilation.

int	frame
string	id, id1, id2
bool	jflag = no

delete ("adbpairs.sql,adbpairs.tmp", >& "dev$null")

list = "adbpairs.list"; touch ("adbpairs.tmp")
while (fscan (list, s1, i, j, s2, id1, id2) != EOF) {
    s3 = ""; match (id1, "adbpairs.tmp") | scan (s3)
    if (s3 == id1)
        next
    ;
    s3 = ""; match (id2, "adbpairs.tmp") | scan (s3)
    if (s3 == id2)
        next
    ;
    movrev.lastkey = ""
    movrev (s1, id=i, frame=1, onlyone+, query-, auto-)
    movrev.lastkey = ""
    movrev (s2, id=j, frame=2, onlyone+, query-, auto-)
    s1 = substr (s1, 1, strldx('_',s1)-3)
    s2 = substr (s2, 1, strldx('_',s2)-3)
    printf ("\nquit = q, delete =  d | framenum | b(oth), join = join ... ")
    k = fscan (imcur, x, y, frame, s3)
    printf ("update movgrp set checked='visually' where movid='%s';\n",
	id1, >> "adbpairs.sql")
    printf ("update movgrp set checked='visually' where movid='%s';\n",
	id2, >> "adbpairs.sql")
    if (s3 == 'q') {
	printf ("%s\n\n", s3)
        break
    }
    ;
    if (s3 == 'b') {
	printf ("%s\n\n", s3)
	printf ("%s\n", id1, >> "adbpairs.tmp")
	printf ("delete from movgrp where dataset='%s' and groupid=%d;\n",
	    s1, i, >> "adbpairs.sql")
	printf ("delete from movobs where dataset='%s' and groupid=%d;\n",
	    s1, i, >> "adbpairs.sql")
	printf ("delete from movmpc where dataset='%s' and groupid=%d;\n",
	    s1, i, >> "adbpairs.sql")
	printf ("%s\n", id2, >> "adbpairs.tmp")
	printf ("delete from movgrp where dataset='%s' and groupid=%d;\n",
	    s2, j, >> "adbpairs.sql")
	printf ("delete from movobs where dataset='%s' and groupid=%d;\n",
	    s2, j, >> "adbpairs.sql")
	printf ("delete from movmpc where dataset='%s' and groupid=%d;\n",
	    s2, j, >> "adbpairs.sql")
    } else if (s3 == 'd' || s3 == '1' || s3 == '2') {
        if (s3 == 'd')
	    k = frame / 100
	else
	    k = int (s3)
	;
	printf ("d(%d)\n\n", k)
	if (k == 1) {
	    printf ("%s\n", id1, >> "adbpairs.tmp")
	    printf ("delete from movgrp where dataset='%s' and groupid=%d;\n",
	        s1, i, >> "adbpairs.sql")
	    printf ("delete from movobs where dataset='%s' and groupid=%d;\n",
	        s1, i, >> "adbpairs.sql")
	    printf ("delete from movmpc where dataset='%s' and groupid=%d;\n",
	        s1, i, >> "adbpairs.sql")
	} else if (k == 2) {
	    printf ("%s\n", id2, >> "adbpairs.tmp")
	    printf ("delete from movgrp where dataset='%s' and groupid=%d;\n",
	        s2, j, >> "adbpairs.sql")
	    printf ("delete from movobs where dataset='%s' and groupid=%d;\n",
	        s2, j, >> "adbpairs.sql")
	    printf ("delete from movmpc where dataset='%s' and groupid=%d;\n",
	        s2, j, >> "adbpairs.sql")
	}
	;
    } else if (s3 == 'j') {
	printf ("%s\n\n", s3)
        if (jflag == no) {
	    printf ("drop table adbmpcsubid;\n", >> "adbpairs.sql")
	    printf ("create table adbmpcsubid (movid text, mpcsubid text);\n",
	        >> "adbpairs.sql")
	    jflag = yes
	}
	;
        printf ("insert into adbmpcsubid values ('%s','%s');\n",
	    id2, id1, >> "adbpairs.sql")
    }
    ;
}
list = ""

if (jflag)
    printf ("Be sure to run 'adb -qf adbpairs.sql; adbmpcsubid set' afterwards\n")
else
    printf ("Run 'adb -qf adbpairs.sql'\n")
;
