# ADBUPDATE -- After an update to an mov file this updates the database.

procedure adbupdate (root)

string	root			{prompt="Root of the dataset"}

begin
	movdp (in=root//"_mov")
	movsql (root, del+)
	adb ("-q", "-f", root//".sql")
end
