# ADBREVIEW -- Wrapper script to review a list of cutout files and object IDs.

procedure adbreview ()

file	input = "adb.list"	{prompt="File with cutouts and IDs"}
string	select = ""		{prompt="Selection pattern"}
file	output = "adb"		{prompt="File with sql updates or adb"}
file	deleted = "deleted.dat"	{prompt="Record of deleted objects"}
file	rates = ""		{prompt="Rate file"}
bool	auto = no		{prompt="Automatic decisions?"}
struct	*fd

begin
    int		m, n, a, d, p, f
    string	cutout, movid, groupid
    struct	pars

    if (access(output)==YES) {
        delete (output)
#	error (1, "Output sql already exists ("//output//")")
    }
    ;
#    if (access(deleted)==YES)
#        delete (deleted)
#    ;
    if (rates!="" && access (rates)==YES)
        delete (rates)
    ;
    count (input) | scan (n)
    fd = input; pars = ""; a = 0; d = 0; f = 4; movreview.lastkey = ""
    for (m=0; fscan (fd, movid, cutout, pars) != EOF; m+=1) {
        p = real (a * 100.) / max (1, m) + 0.5
	printf ("%s: %s %d %d/%d=%d%%\n", movid, pars, n, a, m, p)
        n -= 1

	# Default.
	f = mod (f, 4) + 1
	movreview.lastkey = 'D'
	movreview (movfile=cutout, id=movid, onlyone+, select=select,
	    rates=rates, auto=auto, frame=f)
	#movreview (movfile=cutout, id=groupid, onlyone+, select=select,
	#    rates=rates, auto=auto, frame=f)
	if (rates != "")
	    next
	;

	## If 5 or more
	#movreview (movfile=cutout, id=movid, onlyone+, auto=auto, select="",
	#    minrate=5, maxrate=50, minerr=5, maxpcta=15,
	#    maxerr=10, maxpctr=100)
	## If 4
	#movreview (movfile=cutout, id=movid, onlyone+, auto=auto, select="",
	#    minrate=5, maxrate=50, minerr=5, maxpcta=10,
	#    maxerr=10, maxpctr=100)
	# For 3
	#movreview (movfile=cutout, id=movid, onlyone+, auto=auto, select="",
	#    minrate=5, maxrate=50, minerr=.3, maxpcta=0.1,
	#    maxerr=0, maxpctr=1.)
	# For 3 but no display.
	#movreview (movfile=cutout, id=movid, onlyone+, auto=auto, select="",
	#    minrate=5, maxrate=50, minerr=.3, maxpcta=1.,
	#    maxerr=0., maxpctr=0.9)

	# Record if the tracklet was reviewed.
	if (movreview.lastkey == '\\040')
	    printf ("update movgrp set checked='visually' where movid='%s';\n", movid,
	        >> output)
	;

	# If deleted interactively or autmatically delete from DB and record.
	if (movreview.lastkey == 'd' || movreview.lastkey == 'D') {
	    printf ("%s %s %d %s\n",
	        movid, cutout, pars, movreview.lastkey, >> deleted)
	    printf ("delete from movmpc where movid='%s';\n", movid, >> output)
	    printf ("delete from movobs where movid='%s';\n", movid, >> output)
	    printf ("delete from movgrp where movid='%s';\n", movid, >> output)
	    d += 1
	} else if (movreview.lastkey == 'v') {
	    printf ("update movgrp set checked=NULL where movid='%s';\n", movid,
	        >> output)
	} else
	    a += 1
	;

	if (movreview.lastkey == "q" || movreview.lastkey == "Q")
	    break
	;
    }
    fd = ""

    if (output == "adb")
        adb ("-qf", "adb")
    ;
end
