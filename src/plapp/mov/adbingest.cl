# ADBINGEST -- Create sql and ingest.
#
# Input is list of _*.* files, typically grp.cat files.
# This creates a .sql file in the data directory which is then used
# to avoid further ingests.  It also links the cutout file to a single
# directory for use by the database review.

procedure adbingest ()

string	input = "ABC"		{prompt="ls grp.cat wildcard list or letter"}
string	reviewed = "auto"	{prompt="Reviewed?"}
bool	donerev = yes		{prompt="Require review done?"}
bool	sqldel = NO		{prompt="Delete existing sql?"}
bool	dbdrop = NO		{prompt="Drop previous ingestions?"}
file	movdir = "NHPPS_FINAL$/MOV/" {prompt="Directory for cutout hard links"}

struct	*fd

begin
	file	in, f, dir, fildir, root, sql
	string	checked
	int	i

	# Set input list of files.
	in = input
	i = stridx ("ABC", in)
	if (i > 0 && i < 4)
	   in = "*/*-dts/*_["//in//"]_grp.cat"
	ls (in, > "adbingest.list")

	# Set reviewed.
	#if (reviewed == YES)
	#    checked = "visually"
	#else
	#    checked = "NULL"
	#;
	checked = reviewed

	# Create directory for mov files to be collected.
	# Create MOV directory if needed.
	if (movdir == "")
	    movdir = "MOV/"
	;
	fspath (movdir) | scan (movdir)
	if (access(movdir)==NO)
	    mkdir (movdir)
	;

	# Loop through files.
	fd = "adbingest.list"
	while (fscan (fd, f) != EOF) {

	    # Check whether to ingest.
	    i = strldx("/", f)
	    dir = substr (f, 1, i)
	    root = substr (f, i+1, strldx("_",f)-1)
	    sql = dir // root // ".sql"
	    if (access (sql) == YES) {
		if (sqldel == YES)
		    delete (sql, v-)
		else
		    next
		;
	    }
	    ;
	    if (donerev==YES && access(dir//root//"_dorev.done")==NO)
	        next
	    ;

	    # Create SQL, ingest, and link mov file.
	    cd (dir)
	    sql = root // ".sql"
	    print (sql)
	    movsql (root, checked=checked, del=dbdrop)
	    adb ("-q", "-f", sql)
	    if (strstr("-stk",dir) > 0) {
		fildir = substr (dir, 1, strstr("-stk",dir)-1)
	        fildir = substr (fildir, strldx("/",fildir)+1, 999)
	        fildir = "../../FIL/" // fildir
		if (access(fildir)==YES) {
		    ln (sql, fildir)
		    if (access(root//"_dorev.done")==YES)
		        ln (root//"_dorev.done", fildir)
		    ;
		}
		;
	    }
	    ;
	    if (access(movdir//root//"_mov.fits")==NO)
		ln (root//"_mov.fits", movdir)
	    ;
	    back (> "dev$null")
	}
	fd = ""; delete ("adbingest.list")
end
