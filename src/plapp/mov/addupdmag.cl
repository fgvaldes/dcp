procedure adddupdmag (input, output)

file	input			{prompt="File with updated magnitudes"}
file	output			{prompt="SQL update file"}
file	work="addupdmag"	{prompt="Work file rootname"}

struct	*fd

begin
	file	in, out
	string	ds, imid, imid1, movid
	int	grpid
	real	mag

	# Set parameters.
	in = input
	out = output
	if (access(out))
	    delete (out)

	# Get the mapping between pipeline exposure name and dataset.
	tmerge ("ADDAST/*C_exp.cat", work // "_cat.tmp", "append")

	# Get update data.  Sort to minimize lookups.
	tdump (in, col="COIMID,COGRPID,MAG1") | sort | unique (> work//".tmp")

	# Make updates.
	printf ("begin transaction;\n", >> out)
	fd = work // ".tmp"; imid1 = ""
	while (fscan (fd, imid, grpid, mag) != EOF) {
	    # Set root of movid.
	    if (imid != imid1) {
	        match (imid, work // "_cat.tmp") | scan (ds)
		movid = substr (ds, strldx("_",ds)+1, stridx("-",ds)-1)
		movid += "_" // substr (ds, strldx("-",ds)+1, 99)
		imid1 = imid
	    }

	    # Output SQL update.
	    printf ("update addgrp set mag=%.2f where movid='%s_%03d';\n",
	        mag, movid, grpid, >> out)
	}
	fd = ""
	printf ("end transaction;\n", >> out)
	
	delete (work//"*.tmp")

	# Remove repeats.
	sort (out) | unique (> out)
end
