# ALLREVIEW -- This is a quick script to review detections given in MPC records.

procedure allreview (quality)

string	quality			{prompt="Quality level (A|B)"}
string	level = "[hm]?"		{prompt="Interest file pattern"}
string	ids = ""		{prompt="ID source"}
string	mpcfile = ""		{prompt="MPC file"}
string	expfile = ""		{prompt="Exp file"}
string	movfile = ""		{prompt="MOV file"}
string	movdir = ""		{prompt="Directory with MOV files"}
string	mpcdir = ""		{prompt="Directory with MPC files"}
file	rates = ""		{prompt="Rates output"}
bool	verbose = no		{prompt="Verbose?"}

struct	*fd

begin
	string	mpc
	file	exps
	string	movf
	string	work

	string	q, last, movid, stbid, ds, mov, dir, id
	int	i, j, grpid

	# Set parameters.
	q = quality
	id = ids
	mpc = mpcfile
	exps = expfile
	movf = movfile
	if (mpc == "")
	    mpc = mpcdir // "*_"//q//"_"//level//".txt"
	if (exps == "")
	    exps = "*_"//q//"_exp.cat"
	if (movf == "")
	    movf = "_"//q//"_mov.fits"
	work = "allreview"//q

#print (mpc)
#print (exps)
#print (movf)

	# Initialize.
	delete (work//"*.tmp")

	# Make a list of identifiers.
	if (id == "")
	    id = mpc
	files (id, > work//"1.tmp")
	count (work//"1.tmp") | scan (i)
	if (i < 1)
	    return

	if (verbose) {
	    printf ("ID sources: ")
	    concat (work//"1.tmp")
	}

	fd = work//"1.tmp"
	while (fscan (fd, movid) != EOF) {
	    if (access(movid))
		concat (movid, work//".tmp", append+)
	    else
	        print (movid, >> work//".tmp")
	}
	fd = ""; delete (work//"1.tmp")

	if (verbose) {
	    count (work//".tmp") | scan (i)
	    printf ("Number of sources: %d\n", i)
	}

	fd = work//".tmp"; last = ""
	while (fscan (fd, movid, stbid) != EOF) {
	    if (nscan () > 1) {
		if (substr(stbid,1,3) != "C20")
		    next
	    }
	    if (movid == last)
	        next
	    last = movid

	    if (access(mpc)) {
		ds = ""; match (movid, mpc, print-) | scan (ds)
		if (ds == "") {
		    if (verbose)
			printf ("  %s not found in %s\n", movid, mpc)
		    next
		}
	    }

	    movid (movid=movid, dir="from", v-)
	    stbid = movid.stbid
	    grpid = movid.grpid
	    if (movfile == "") {
		ds = ""; match (stbid, exps, print-) | scan (ds)
		if (ds == "")
		    next
		mov = movdir // ds // movf
	    } else
	        mov = movdir // movf
	    if (!access(mov)) {
	        i = stridx("-",ds)
	        j = strldx("-",ds)
		dir = substr(ds,1,i) // "fil" // substr(ds,i+1,j-1) // "-grp-stk" // substr(ds,j+1,999) // "/"
	        mov = "NHPPS_DATAAPP$/DCP_STK/" // dir // mov
	    }
	    if (!access(mov)) {
		if (verbose)
		    printf ("  Not found: movfile = %s\n", mov)
	        next
	    }
	    if (access(work//".list")) {
		match (movid, work//".list") | count | scan (i)
		if (i > 0) {
		    if (verbose)
			printf ("  %s in %s\n", movid, work//".list")
		    next
		}
	    }
	    printf ("=======\n%s\n=======\n", movid)
	    movreview (movfile=mov, id=movid, onlyone+, rates=rates)
	    print (movid, >> work//".list")
	}
	fd = ""; delete (work//"*.tmp")
end
