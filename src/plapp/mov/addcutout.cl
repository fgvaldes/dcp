# Add positions to cutout file.

file	im, root, dir
real	ra, dec, rate, tsep
int	imid, grpid

root = "/net/decdata1/deccp/Archive/DEC13A/red/20140228133150_20130415rS/remap/"
delete ("foo.fits")
b1 = access("foo.fits")

list = "foo3.tmp"
while (fscan (list, im, ra, dec, imid, grpid, rate, tsep) != EOF) {
    dir = substr (im, 1, stridx("_",im)-1) // "/"
    im = root // dir // im

    delete ("foo.cat")
    printf ("#c COEXPID\n", >> "foo.cat")
    printf ("#c COGRPID\n", >> "foo.cat")
    printf ("#c RA\n", >> "foo.cat")
    printf ("#c DEC\n", >> "foo.cat")
    printf ("#c CORATE\n", >> "foo.cat")
    printf ("#c COTSEP\n", >> "foo.cat")
    printf ("%d %d %g %g %g %g\n", imid, grpid, ra, dec, rate, tsep, >> "foo.cat")
    acecutouts (im, "foo.cat", "foo.fits",
	extname="CA", ncpix=300, nlpix=300, blkavg=1, coords="RA,DEC",
	wcs="world h 1", tiles="COEXPID,COGRPID", gap=2,
	fields="COGRPID,COEXPID,CORATE,COTSEP", nobp=no)
    if (b1 == NO)
        hedit ("foo.fits[0]", "CONEXP", 4, add+)
    b1 = YES
}
list = ""
