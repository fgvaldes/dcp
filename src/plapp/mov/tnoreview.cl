# Review

files ("MOV*[SM]", > "list")
files ("MOV*[SM]-dts", >> "list")
files ("TNO*[SM]", >> "list")
files ("TNO*[SM]-dts", >> "list")

list = "list"
while (fscan (list, s1) != EOF) {
    s1 += "/"
    files (s1//"*_A_mov.info") | count | scan (i)
    if (i == 0) {
	cd (s1)
	s2 = ""; files ("*_A_mov.fits") | scan (s2)
	if (s2 != "") {
	    printf ("%s", s1)
	    unlearn movrev
	    movrev (s2)
	}
	;
	cd ..
    }
    ;
    files (s1//"*_B_mov.info") | count | scan (i)
    if (i == 0) {
	cd (s1)
	s2 = ""; files ("*_B_mov.fits") | scan (s2)
	if (s2 != "") {
	    printf ("%s", s1)
	    unlearn movrev
	    movrev (s2)
	}
	;
	cd ..
    }
    ;
}
list = ""
