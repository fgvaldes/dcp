int	idx
string	sa, sb, sc, sd, se, sf, sg, sh
file	tmp, tmp1, tmp2
struct	*fd

task	$iqsdb = "$!psql -Aqt -F ' ' < $(1) > $(2)"
task	$dsplanb = "$!dsplanb $(1)"

tmp = mktemp ("A")
tmp1 = tmp // "1.tmp"
tmp2 = tmp // "2.tmp"

# Create list of plprocid and plfname.
files ("*ccd1_diff.cat", > tmp2)
fd = tmp2
while (fscan (fd, sa) != EOF) {
    sa = substr (sa, 1, strstr("_diff.cat",sa)-1)
    if (access (sa//".fits"))
        next
    ;
    sa = substr (sa, 1, strstr("-ccd",sa)-1)
    idx = stridx ("-", sa)
    sb = substr (sa, 1, idx-1)
    sc = substr (sa, idx+1, 999)
    sb = substr (sb, strldx("_",sb)+1, 999)
    printf ("%s %s %s\n", sa, sb, sc, >> tmp1)
}
fd = ""; delete (tmp2)

if (access(tmp1)) {
    # Create sql queries.
    sort (tmp1) | uniq (> tmp1)
    fd = tmp1
    while (fscan (fd, sa, sb, sc) != EOF) {
	printf ("SELECT reference\n", >> tmp2)
	printf ("  FROM viewspace.primary_fits_header\n", >> tmp2)
	printf ("  WHERE plprocid='%s'\n", sb, >> tmp2)
	printf ("  AND plfname='%s'\n", sc, >> tmp2)
	printf ("  AND proctype='InstCal'\n", >> tmp2)
	printf ("  AND prodtype='image';\n", >> tmp2)
#	printf ("SELECT reference\n", >> tmp2)
#	printf ("FROM edu_noao_nsa_system.fits_data_product\n", >> tmp2)
#	printf ("WHERE fits_data_product_id IN (\n", >> tmp2)
#	printf ("  SELECT fits_data_product_id\n", >> tmp2)
#	printf ("  FROM edu_noao_nsa_system.fitsdataproduct_fitsheader_assoc\n", >> tmp2)
#	printf ("  WHERE fits_header_id IN (\n", >> tmp2)
#	printf ("    SELECT fits_header_id\n", >> tmp2)
#	printf ("    FROM edu_noao_nsa_system.fits_header\n", >> tmp2)
#	printf ("    WHERE proctype = 'InstCal' AND fits_header_id IN (\n", >> tmp2)
#	printf ("      SELECT processed_fits_header_id\n", >> tmp2)
#	printf ("      FROM edu_noao_nsa_system.processed_fits_header\n", >> tmp2)
#	printf ("      WHERE plprocid = '%s'\n", sb, >> tmp2)
#	printf ("      AND plfname = '%s'\n", sc, >> tmp2)
#	printf ("      AND prodtype = 'image'\n", >> tmp2)
#	printf ("    )\n", >> tmp2)
#	printf ("  )\n", >> tmp2)
#	printf (");\n", >> tmp2)
    }
    fd = ""; delete (tmp1)

    # Execute sql.
    iqsdb (tmp2, tmp1)
    delete (tmp2)

    # Retrieve files.
    dsplanb (tmp1)
    delete (tmp1)
    delete ("ds_names.log")

    # Extract extensions and make input file.
    files ("tu*.fits", > tmp1)
    mscselect ("@"//tmp1, "$I,plqueue,plqname,plprocid,plfname,extname") |
	sort (col=5, > tmp2)
    fd = tmp2
    while (fscan (fd, sa, sd, se, sf, sg, sh) != EOF) {
	printf ("%s_%s_%s-%s-%s\n", sd, se, sf, sg, sh) | scan (sb)
	printf ("%s_%s_%s-%s-%s_diff.cat\n", sd, se, sf, sg, sh) | scan (sc)
	imcopy (sa, sb, verbose+)
	thselect (sc, "UPARM,PIPEDATA", yes) | scan (sd, se)
	hedit (sb, "UPARM", sd, add+, verify-, show-, update+)
	hedit (sb, "PIPEDATA", se, add+, verify-, show-, update+)
    }
    fd = ""; delete (tmp2)

    imdelete ("@"//tmp1)
    delete (tmp1)
}
;
