procedure mkasteroids (images, astlist, outlist)

string	images			{prompt="List of images"}
string	astlist			{prompt="Asteroid list (dra, ddec, mag, rate, pa)"}
file	outlist			{prompt="Output (append) list (im, ra, dec, x, y)"}

file	ref = ""		{prompt="Reference image"}
string	crval_ra = "!crval1"	{prompt="RA ref (deg)"}
string	crval_dec = "!crval2"	{prompt="DEC ref (deg)"}
string	mjdref = "!mjdmed"	{prompt="MJD reference"}
string	mjd = "!mjd-obs"	{prompt="MJD of observation"}
string	exptime = "!expdur"	{prompt="Exposure time (s)"}
string	seeing = "!fwhm"	{prompt="Seeing (pix)"}
string	scale = "!cd1_1"	{prompt="Pixel scale (deg/pix)"}
string	magzero = "!magzero"	{prompt="Magnitude zeropoint"}
real	tstep = 60		{prompt="Time step (s)"}
bool	verbose = yes		{prompt="Verbose output?"}
int	dispframe = INDEF	{prompt="Display frame (INDEF = no display)"}

struct	*fd1, *fd2

begin
	file	im,  imref, alist, olist
	string	ra, dec
	real	x, y, m, r, p, r1, p2, s, t, pixscale, mz, dt, mjd0, ra0, dec0
	real	z, sinp, cosp, x1, y1, z1, m1
	int	i, j, k, n, nc, nl, frame
	bool	world

	delete ("mkasteroids*.tmp", verify-, >& "dev$null")
	sections (images, option="fullname", > "mkasteroids1.tmp")
	alist = astlist
	olist = outlist
	imref = ref
	frame = dispframe

	printf ("MKASTEROIDS:\n\n")

	fd1 = "mkasteroids1.tmp"; mjd0 = INDEF; ra0 = INDEF; dec0 = INDEF
	for (dt = 0; fscan (fd1, im) != EOF; dt += tstep) {
	    if (imref == "")
		imref = im
	    if (!imaccess (imref))
		error (1, "Reference image not found ("//imref//")")
	    if (!access(alist))
		error (1, "Asteroid list not found ("//alist//")")

	    # Set time origin.
	    if (isindef (mjd0) && imaccess(im)) {
		if (mjdref != "") {
		    if (substr(mjdref,1,1) == '!')
			hselect (imref, substr(mjdref,2,99), yes) | scan (mjd0)
		    else
			mjd0 = real  (mjdref)
		} else if (mjd != "") {
		    if (substr(mjd,1,1) == '!')
			hselect ("@mkastroids1.tmp", substr(mjd,2,99), yes) |
			    sort (num+) | scan (mjd0)
		    else
			mjd0 = real  (mjd)
		}
	    }
	    if (isindef(mjd0))
	        error (1, "Reference time origin not found")

	    # Set coordinate origin.
	    if (isindef (ra0) && imaccess(im)) {
		if (crval_ra != "") {
		    ra0 = 0
		    if (substr(crval_ra,1,1) == '!')
			hselect (imref, substr(crval_ra,2,99), yes) |
			    scan (ra0)
		    else
			ra0 = real  (crval_ra0)
		}
		if (crval_dec != "") {
		    dec0 = 0
		    if (substr(crval_dec,1,1) == '!')
			hselect (imref, substr(crval_dec,2,99), yes) |
			    scan (dec0)
		    else
			dec0 = real  (crval_dec0)
		}
		if (verbose)
		    printf ("  Pointing: RA = %.2H, DEC = %.1h\n", ra0, dec0)
	    }

	    # Get image parameters.
	    if (mjd != "" && imaccess(im)) {
		if (substr(mjd,1,1) == '!')
		    hselect (im, substr(mjd,2,99), yes) | scan (dt)
		else
		    dt = real  (mjd)
		dt -= mjd0
		dt *= 24 * 3600
	    }
	    if (substr(exptime,1,1) == '!')
		hselect (imref, substr(exptime,2,99), yes) | scan (t)
	    else
		t = real  (exptime)
	    if (substr(seeing,1,1) == '!')
		hselect (imref, substr(seeing,2,99), yes) | scan (s)
	    else
		s = real  (seeing)
	    if (substr(scale,1,1) == '!')
		hselect (imref, substr(scale,2,99), yes) | scan (pixscale)
	    else
		pixscale = real  (scale)
	    pixscale = abs (pixscale) * 3600
	    if (substr(magzero,1,1) == '!')
		hselect (imref, substr(magzero,2,99), yes) | scan (mz)
	    else
		mz = real  (magzero)
	    mz -= 2.5 * log10 (t)
	    if (verbose) {
		printf ("  Output image = %s\n", im)
		if (im != imref)
		    printf ("  Reference image = %s\n", imref)
		printf ("  duration = %d, seeing = %.1f, scale = %.2f,\
		    magzero = %.1f dt = %d\n", t, s, pixscale, mz, dt)
	    }
	    s *= 0.45

	    hselect (imref, "NAXIS1,NAXIS2", yes) | scan (nc, nl)

	    # Convert list to objects for mkobjects.
	    fd2 = alist
	    for (j = 1; fscan (fd2, ra, dec, m, r, p) != EOF; j += 1) {

		# If input is offset from reference coordinate convert to
		# actual coordinate.  Note that to discriminate between
		# input of x/y and delta ra/dec we depend on the user
		# specifying the reference coordinate or not.

		x = real (ra); y = real (dec)
	        if (isindef(dec0) == NO) {
		    y = dec0 + y / 3600.
		    printf ("%.1h\n", y) | scan (dec)
		}
	        if (isindef(ra0) == NO) {
		    x = ra0 + x / 3600. / dcos (y)
		    printf ("%.2H\n", x) | scan (ra)
		}
		    
		# Convert to logical coordinates.
		x = real (ra); y = real (dec)
		world = (stridx (":", ra) > 0)
		if (world)
		    print (x, y) | wcsctran ("STDIN", "STDOUT", imref, "world",
			"logical", columns="1 2", units="hours degrees",
			formats="", min_sigdigit=7, verbose=no) | scan (x, y)

		# Convert to celestial coordinates.
		print (x, y) | wcsctran ("STDIN", "STDOUT", imref, "logical",
		    "world", columns="1 2", units="", formats="%.2H %.1h",
		    min_sigdigit=7, verbose=no) | scan (ra, dec)

		# Apply motion from the reference epoch and convert to
		# logical pixel coordinates in the image.

		x1 = real(ra); y1 = real(dec)
		# Convert position angle to celestial.
		movmotion (x1, y1, 60., rate=10., pa=0) | scan (ra, dec)
		movmotion (x1, y1, 60., ra2=real(ra), dec2=real(dec)) |
		    scan (r2, p2, p2)
		p2 -= p

		movmotion (x1, y1, dt/3600., rate=r, pa=p2) | scan (ra, dec)
		#movmotion (x1, y1, 60., ra2=real(ra), dec2=real(dec)) |
		#    scan (r2, p2, p)
		print (ra, " ",  dec) | wcsctran ("STDIN", "STDOUT", imref,
		    "world", "logical", columns="1 2", units="hours degrees",
		    formats="", min_sigdigit=7, verbose=no) | scan (x, y)

		# Check if the input coordinate is within the image.
		if (x < 5 || x > nc-5 || y < 5 || y > nl-5) {
#		    if (verbose &&
#		       (x > -1000 && x < nc+1000 && y > -1000 && y < nl+1000)) {
#			printf ("  %3d: ra = %s, dec = %s, x = %.1f, \
#			    y = %.1f [OOB]\n", j, , ra, dec, x, y)
#			printf ("       mag = %.1f, rate = %d, pa = %d\n",
#			    m, r, p)
#		    }
		    next
		}
		
		# Record added object position.
		if (verbose) {
		    printf ("  %3d: ra = %s, dec = %s, x = %.1f, \
			y = %.1f\n", j, , ra, dec, x, y)
		    printf ("       mag = %.1f, rate = %.2f, pa = %d\n", m, r, p)
		}
		printf ("%s %s %s %.1f %.1f %.1f %d %d %d\n",
		    im, ra, dec, x, y, m, r, p, j, >> olist)

		# Convert rate to pixels per exposure time.
		r *= t / 3600. / pixscale

		# Number of samples.
		n = nint (r / 2 + 1)

		# Make object file.
		#m1 = m + 2.5 * log10 (n) 
		m1 = m
		z =  r / 2.
		sinp = dsin (p+90)
		cosp = dcos (p+90)
		for (i=0; i<n; i+=1) {
		    z1 = 2 * i - z
		    x1 = x + z1 * cosp
		    y1 = y + z1 * sinp

		    print (x1, y1, m1, >> "mkasteroids.tmp")
		}
	    }
	    fd2 = ""

	    # Exit if no asteroids fell in the field.
	    if (!access ("mkasteroids.tmp")) {
		if (verbose)
		    printf ("    No sources in field\n")
	        next
	    }

	    # Add asteroids.
	    if (imaccess(im))
		mkobjects (im, output="", objects="mkasteroids.tmp", xoffset=0.,
		    yoffset=0., star="moffat", radius=s, beta=2.5, ar=1., pa=0.,
		    distance=1., exptime=t, magzero=mz, rdnoise=0.,
		    poisson=no, seed=1, comments=no)
	    else
		mkobjects (imref, output=im, objects="mkasteroids.tmp", xoffset=0.,
		    yoffset=0., star="moffat", radius=s, beta=2.5, ar=1., pa=0.,
		    distance=1., exptime=t, magzero=mz, rdnoise=0.,
		    poisson=no, seed=1, comments=no)
	    delete ("mkasteroids.tmp", v-)

	    # Display (mostly for debugging).
	    if (!isindef(dispframe)) {
		display (im, frame, > "dev$null")
		frame = mod (frame, 4) + 1
	    }
	}
	fd1 = ""; delete ("mkasteroids1.tmp")
end
