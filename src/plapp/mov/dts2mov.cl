# DTS2MOV -- Setup MOV directory from DTS data.

procedure mov2mov (datasets)

string	datasets		{prompt="List of DTS Dataset"}
file	dtsdata = "pipedmn!/data2/MarioData/MOSAIC/MOSAIC_DTS/output/"	{prompt="DTS source directory"}

struct	*fd, *fd1

begin
	int	i, j, k
	string	s1, s2
	string	ds, dts1, dir, ilist, uparm, pipedata
	string	tmp, tmp1, tmp2, tmp3

	tmp = mktemp ("tmp")
	tmp1 = tmp // "1"
	tmp2 = tmp // "2"
	tmp3 = tmp // "3"

	# Go to starting directory.
	cd ("MOSAIC_MOV$/data/")

	# Expand list and loop through the datasets.
	files (dtsdata//datasets, > tmp3)
	fd1 = tmp3
	while (fscan (fd1, dts1) != EOF) {
	    ds = substr (dts1, strldx("/",dts1)+1, 999)
	    printf ("%s ...\n", ds)

	    # Create list of dataset files..

	    dts1 += "/"
	    match ("diff.cat", dts1//"*str*log", > tmp2)
	    count (tmp2) | scan (i)
	    if (i == 0) {
	        delete (tmp2, verify-)
		next
	    }

	    fd = tmp2; touch (tmp1)
	    while (fscan (fd, s1) != EOF) {
		i = strldx ("/", s1)
		j = strldx ("_", s1)
		s2 = substr (s1, i+1, j-1)
		s1 = substr (s1, 1, i-1)
		i = strldx ("/", s1)
		j = strlstr ("-ctr", s1)
		s1 = substr (s1, i+1, j-1)
		i = strstr ("-mdc", s1)
		j = i + 4
		s1 = substr (s1, 1, i) // "sci-mov" // substr (s1, j, 999)
		printf ("%s %s\n", s1, s2, >> tmp1)
	    }
	    fd = ""; delete (tmp2, verify-)

	    # Create directories and files.
	    fd = tmp1
	    while (fscan (fd, ds, s1) != EOF) {
		s2 = "MOSAIC_MOV$/input/"//ds//".mov"
		if (access (s2//"missing"))
		    ilist = s2 // "missing"
		else
		    ilist = s2

		# Create data directory.

		dir = "MOSAIC_MOV$/data/" // ds
		if (!access(dir)) {
		    mkdir (dir)
		    sleep 1
		}
		cd (dir)

		if (!access("data")) {
		    mkdir ("data")
		    sleep 1
		}
		cd ("data")

		# Copy files.

		s2 = s1 // "_diff.cat"
		if (!access(s2)) {
		    if (access (dts1//s2)) {
			copy (dts1//s2, ".")
			path (s2, >> ilist)
		    }
		}
		thselect (s2, "UPARM,PIPEDATA", yes) | scan (uparm, pipedata)

		s2 = s1 // ".fits"
		if (access(ilist) && !access(s2)) {
		    i = strldx ("-", s1)
		    s2 = substr (s1, 1, i-1) // "[" // substr (s1, i+1,999) // "]"
		    if (imaccess(dts1//s2)) {
			imcopy (dts1//s2, s1, verbose-)
			path (s1//".fits", >> ilist)
			hedit (s1, "UPARM", uparm,
			    add+, verify-, show-, update+)
			hedit (s1, "PIPEDATA", uparm,
			    add+, verify-, show-, update+)
		    } else {
			if (strstr ("missing", ilist) == 0) {
			    if (access (ilist))
				rename (ilist, ilist // "missing")
			}
			printf ("Missing %s\n", s2)
		    }
		}

		cd ("MOSAIC_MOV$/data/")
	    }
	    fd = ""; delete (tmp1, verify-)

	    delete (tmp//"[12]", verify-)
	}
	fd1 = ""; delete (tmp3, verify-)
end
