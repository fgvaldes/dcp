# MKCOGRAPHIC -- Make cutout graphics.

procedure mkcographic (cofile)

file	cofile			{prompt="Cutout file"}
string	work = "cotmp"		{prompt="Working rootname"}

struct	*ids, *cos

begin
	file	root, in, out, jpg, html
	file	co, im, coovr, codat, cogrps, cotmp
	file	colist, coovrlist, cooffsets
	file	cotmpim, cotmpbpm, cotmpovr, cotmpgif
	int	i, j, offset
	real	cox, coy
	string	id, coid, cogrp, detsec
	struct	expr

	task	$convert = "$!convert"

	in = cofile
	files (in) | scan (in)
	root = substr (in, strldx("/",in)+1, strldx(".",in)-1)
	html = root // ".html"

	if (!access(html)) {
	    printf ('<html><title>%s</title></body>\n', root, > html)
	    printf ('<table>\n', >> html)
	    ln ("-s", html, "index.html")
	}

	codat = "tmp" // work // ".dat"
	cogrps = "tmp" // work // ".list"
	cotmp = work // ".tmp"
	colist = work // "_list.tmp"
	coovrlist = work // "_ovrlist.tmp"
	cooffsets = work // "_offsets.tmp"
	coovr = work // "_ovr"

	cotmpim = work
	cotmpbpm = work // "_bpm.pl"
	cotmpovr = work // "_ovr.pl"
	cotmpgif = work // ".gif"

	# Create working files.
	mscselect (in, "$I,$COID,$COGRPID,$COX,$COY,$DETSEC", > codat)
	fields (codat, "3") | sort | unique (> cogrps)

	# Loop through groups.
	ids = cogrps
	while (fscan (ids, cogrp) != EOF) {
	    match ("\t"//cogrp//"\t", codat, > cotmp)
	    cos = cotmp; coid = "INDEF"
	    for (i=1; fscan(cos,im,id,cogrp,cox,coy,detsec)!=EOF; i+=1) {
		if (id == "INDEF")
		    printf ("%s_%04d\n", root, cogrp) | scan (id)
		if (id != "INDEF")
		    coid = id

		# Image list
		print (im, >> colist)

		# Display offset.
		j = fscanf (detsec, "[%d", offset)
		print (offset, "0", >> cooffsets)

		# Overlay mask.
		if (id == "INDEF")
		    printf ("0\n", cox, coy) | scan (expr)
		else
		    printf ("cannulus(%.1f,%.1f,10,12)?205: 0\n", cox, coy) |
		        scan (expr)
		mskexpr (expr, coovr//i, im, verb-)
		print (coovr//i, >> coovrlist)
	    }
	    cos = ""; delete (cotmp)

	    jpg = coid // ".jpg"
	    if (coid == "INDEF" || access(jpg)) {
	        delete (work//"*")
		next
	    }

	    # Tile cutouts.
	    imcombine ("@"//colist, cotmpim, headers="", bpmasks=cotmpbpm,
	        rejmasks="", nrejmasks="", expmasks="", sigmas="", imcmb="",
		logfile="", combine="average", reject="none",
		project=no, outtype="real", outlimits="",
		offsets=cooffsets, masktype="none", maskvalue="0",
		blank=0., scale="none", zero="mode", weight="none")
#		blank=0., scale="none", zero="mode", weight="none",
#		lthresh=50);

	    imcombine ("@"//coovrlist, cotmpovr, headers="", bpmasks="",
	        rejmasks="", nrejmasks="", expmasks="", sigmas="", imcmb="",
		logfile="", combine="average", reject="none",
		project=no, outtype="real", outlimits="",
		offsets=cooffsets, masktype="none", maskvalue="0",
		blank=202, scale="none", zero="none", weight="none")

	    # Create graphic.
	    export (images=cotmpim//","//cotmpbpm//","//cotmpovr,
	        binfiles=cotmpgif, format="gif", header="yes", outtype="",
	    outbands="setcmap(i3==0?(zscalem(i1,i2==0)*200/255.):i3,'overlay')",
		interleave=0, bswap="no", verbose=no, >& "dev$null")
	    convert (cotmpgif, jpg)

	    # Add to web page.
	    printf ('<tr><td><img src="%s" height="200"></td></tr>\n', jpg, >> html)

	    delete (work//"*")
	    flpr
	}
	ids = ""; delete (cogrps)

	delete (codat)
end
