# MOVDP -- Create moving data products.
#
# See: http://www.minorplanetcenter.net/iau/info/ObsDetails.html
# See: 

procedure movdp ()

file	input = "*_mov.fits"	{prompt="Input cutout file"}
string	output = ""		{prompt="Output root name"}
string	select = "AA?*"		{prompt="Extensions to select"}
int	grpmin = 2		{prompt="Group minimum"}
bool	decamnum = yes		{prompt="Use exposure number for ID?"}
bool	update = yes		{prompt="Update cutout file?"}
bool	fast = no		{prompt="Fast update?"}
real	duration = INDEF	{prompt="Obs. duration (s)"}
file	movdata = "NHPPS_PIPEAPPSRC$/plapp/mov/movdata/" {prompt="Data files"}


struct	*fd

begin
	bool	upd
	int	i, j, clid, nexp, ngrp, nobs, nmag, cllast, row, nobsmax
	real	xi, eta, xi1, xi2, eta1, eta2, t, t1, t2, mag, magav
	real	rate, ratec, pa, pe, opp
	file	in, in1, out, out1, im, tmp, tmp1, tmp2, tmp3, tmp4
	string	dataset, procid, stbid, filt, mpc, mpctxt, dummy

	int	ival, ival1
	real	rval, rval1
	string	sval
	struct  lval

	# Get query parameters.
	in = input
	out = output
	upd = update

	# Set temporary files.
	tmp = mktemp ("mov")
	tmp1 = tmp // "1.tmp"
	tmp2 = tmp // "2.tmp"
	tmp3 = tmp // "3.tmp"
	tmp4 = tmp // "4.tmp"

	# Set input cutout file.
	files (in) | count | scan (i)
	if (i != 1)
	    error (1, "Unknown or ambiguous input file\n")
	files (in) | scan (in)
	i = strstr (".fits", in) - 1
	if (i > 0)
	    in = substr (in, 1, i)
	in1 = substr (in, 1, strldx("_",in)-1)
	in1 = substr (in1, 1, strldx("_",in1)-1)
	if (upd) {
	    imrename (in//".fits", tmp//".fits")
	    imcopy (tmp//"[0]", in, verbose-)
	    mscextract (tmp, in, extname=select, verbose-)
	    imdelete (tmp)
	}

	# Set output.
	if (out == "") {
	    out = in
	    i = strstr ("_mov", out) - 1
	    if (i > 0)
		out = substr (out, 1, i)
	}

	# Update catalog if desired.
	nobsmax = 100
	if (upd) {
	    # Create catalog.
	    hselect (in//"[0]", "CODATA,COGAMMA,COBETA", yes) |
	        scan (sval, xi, eta)
	    print ("#c CODATA ch*48", > tmp1)
	    print ("#c COGRPID i %4d", >> tmp1)
	    print ("#c COEXPID i %2d", >> tmp1)
	    print ("#c CORA d %12.2h hr", >> tmp1)
	    print ("#c CODEC d %12.1h deg", >> tmp1)
	    print ("#c COTSEP r %8.1f sec", >> tmp1)
	    print ("#c COMAG r %6.2f", >> tmp1)
	    print ("#c EXTNAME ch*16 %-16s", >> tmp1)
	    printf ("#k COGAMMA = %12.2h\n", xi, >> tmp1)
	    printf ("#k COBETA = %12.2h\n", eta, >> tmp1)
	    mscselect (in, "CODATA,COGRPID,COEXPID,CORA,CODEC,COTSEP,COMAG,EXTNAME",
	        expr="(CORA!='INDEF')", extname=select, >> tmp1)
	    tinfo (tmp1, ttout-)
	    ngrp = tinfo.nrows
	    if (ngrp > 0) {
		tcalc   (tmp1, "N", "ROWNUM", datatype="int") 
#		acecopy (tmp1, tmp2, catdef=movdata//"movdp.dat",
#		    filter="", verb-)
#		delete (tmp1, verify-)
#		rename (tmp2, tmp1)
		tsort (tmp1, "COGRPID,COTSEP", ascend+)

		# Make cluster catalog.
#		tdump (tmp1, cdfile="", pfile="", datafile=tmp3,
#		    columns="COGRPID,XI,ETA,COTSEP,COMAG", rows="-", pwidth=-1)
		tdump (tmp1, cdfile="", pfile="", datafile=tmp3,
		    columns="COGRPID,CORA,CODEC,COTSEP,COMAG", rows="-", pwidth=-1)
		fd = tmp3; cllast = 0; nobs = 0; ngrp = 0; nobsmax = 0
		while (fscan (fd, clid, xi, eta, t, mag) != EOF) {
		    if (clid != cllast) {
			# Output cluster information.
printf ("%d -> %d nobs = %d\n", cllast, clid, nobs)
			if (nobs > 0) {
			    nobsmax = max (nobs, nobsmax)
			    ngrp += 1
			    t1 = max (0.01, (t2 - t1) / 3600.)
			    movmotion (xi1, eta1, t1, ra2=xi2, dec2=eta2) |
			        scan (rate, pa, pe)
if (rate > 500) {
print (xi1, eta1, t1, xi2, eta2)
print (rate, pa, pe)
}
#			    xi1 = (xi2 - xi1) / t1
#			    eta1 = (eta2 - eta1) / t1
#			    pa = datan2 (-xi1, eta1)
#			    rate = sqrt (xi1**2 + eta1**2)
			    if (nmag > 0)
				magav /= nmag
printf ("%d %d %6.1f %6.1f %5.1f\n",
cllast, nobs, rate, pe, magav)
			    printf ("%d %d %6.1f %6.1f %5.1f\n",
				cllast, nobs, rate, pe, magav, >> tmp2)
			}

			# Initialize new cluster.
			cllast = clid
			nobs = 0
			nmag = 0
			xi1 = xi
			eta1 = eta
			t1 = t
			magav = 0.
		    }

		    nobs += 1
printf ("clid = %d, nobs = %d\n", clid, nobs)
		    xi2 = xi
		    eta2 = eta
		    t2 = t
		    if (!isindef(mag)) {
			nmag += 1
			magav += mag
		    }
		}
		fd = ""; delete (tmp3, verify-)
concat (tmp2)

		# Output cluster information.
		if (nobs > 0) {
		    nobsmax = max (nobs, nobsmax)
		    ngrp += 1
		    t1 = max (0.01, (t2 - t1) / 3600.)
#print (xi2, eta2, xi1, eta1)
		    movmotion (xi1, eta1, t1, ra2=xi2, dec2=eta2) |
			scan (rate, pa, pe)
if (rate > 500) {
print (xi1, eta1, t1, xi2, eta2)
print (rate, pa, pe)
}
#		    xi1 = (xi2 - xi1) / t1
#		    eta1 = (eta2 - eta1) / t1
#		    pa = datan2 (-xi1, eta1)
#		    rate = sqrt (xi1**2 + eta1**2)
		    if (nmag > 0)
			magav /= nmag
printf ("%d %d %6.1f %6.1f %5.1f\n",
    cllast, nobs, rate, pe, magav)
		    printf ("%d %d %6.1f %6.1f %5.1f\n",
			cllast, nobs, rate, pe, magav, >> tmp2)
		}

		# Join cluster information.
		tjoin (tmp1, tmp2, tmp3, "COGRPID", "c1", extrarows="neither",
		    tolerance="0.0", casesens=yes)
		delete (tmp1, verify-)
		delete (tmp2, verify-)

		# Update cutout file.
		if (fast)
		    tdump (tmp3, cdfile="", pfile="", datafile=tmp4,
			columns="CODATA,COGRPID,C2,C3,C4,C5",
			rows="-", pwidth=-1)
		else {
		    tdump (tmp3, cdfile="", pfile="", datafile=tmp1,
			columns="EXTNAME,C2,C3,C4,C5",
			rows="-", pwidth=-1)
		    delete (tmp3, verify-)
		    fd = tmp1
		    while (fscan(fd,im,nobs,rate,pa,mag)!=EOF) {
			printf ("%s[%s]\n", in, im) | scan (im)
			hedit (im, "CONOBS", nobs, verify-, show-, update+)
			hedit (im, "CORATE", rate, verify-, show-, update+)
			hedit (im, "COPA", pa, verify-, show-, update+)
			hedit (im, "COMAG", mag, verify-, show-, update+)
		    }
		    fd = ""
		}
	    }
	    delete (tmp1, verify-, >& "dev$null")
	    hedit (in//"[0]", "CONGRP", ngrp, verify-, show-, update+)
	}

	hselect (in//"[0]", "CODATA,CONGRP", yes) | scan (sval, ngrp)
	dataset = sval

	# Make output tables.

	out1 = out // "_ds.cat"
	if (access(out1))
	    delete (out1)
	out1 = out // "_exp.cat"
	if (access(out1))
	    delete (out1)
	out1 = out // "_grp.cat"
	if (access(out1))
	    delete (out1)
	out1 = out // "_obs.cat"
	if (access(out1))
	    delete (out1)
	out1 = out // "_mpc.cat"
	if (access(out1))
	    delete (out1)
	out1 = out // "_mpc.txt"
	if (access(out1))
	    delete (out1)
	out1 = out // "_digest2.txt"
	if (access(out1))
	    delete (out1)

	# Dataset table.
	keypar (in1//"_imov.cat", "dtcaldat")
	if (keypar.found)
	    hedit (in//"[0]", "DTCALDAT", keypar.value, add+)
	keypar (in1//"_imov.cat", "observer")
	if (keypar.found)
	    hedit (in//"[0]", "OBSERVER", keypar.value, add+)

	out1 = out // "_ds.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c procid ch*8", >> out1)
	print ("#c nexposures i %2d", >> out1)
	print ("#c date ch*24", >> out1)
	print ("#c mjd d %15.8f", >> out1)
	print ("#c filter ch*64", >> out1)
	print ("#c ra d %13.2h hr", >> out1)
	print ("#c dec d %13.1h deg", >> out1)
	print ("#c gamma d %13.1h deg", >> out1)
	print ("#c beta d %13.1h deg", >> out1)
	print ("#c opposition d %13.1h deg", >> out1)
	print ("#c photref ch*10", >> out1
	print ("#c plver ch*10", >> out1
	print ("#c propid ch*10", >> out1
	print ("#c pi ch*68", >> out1
	print ("#c night ch*10", >> out1
	print ("#c observers ch*68", >> out1
	hselect (in//"[0]",
	    "CODATA,$DUMMY,CONEXP,CODATE,COMJD,COFILTER,CORACEN,CODECCEN,COGAMMA,\
	     COBETA,COGAMMA,$PHOTREF,$PLVER,$DTPROPID,$DTPI,$DTCALDAT,$OBSERVER",
	    yes, >> out1)
	i = strldx ("_", dataset) + 1
	procid = substr (dataset, i, i+6)
	partab (procid, out1, "PROCID", 1)

	hselect (in//"[0]", "COMJD,CORACEN", yes) | scan (t, xi)
	suncoord (t, verbose-)
	opp = (mod (suncoord.ra + 12, 24) - xi) * 15
	if (opp < -180)
	    opp += 360
	if (opp > 180)
	    opp -= 360
	partab (opp, out1, "OPPOSITION", 1)

	# Exposure table.
	hselect (in//"[0]", "CONEXP", yes) | scan (nexp)
	out1 = out // "_exp.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c expname ch*48", >> out1)
	print ("#c dateobs ch*32", >> out1)
	print ("#c mjdobs d %15.8f", >> out1)
	print ("#c expid i %2d", >> out1)
	print ("#c tsep r %d sec", >> out1)
	print ("#c exptime r %d sec", >> out1)
	for (i = 1; i <= nexp; i += 1) {
	    hselect (in//"[0]",
	        "CODATA,COIM"//i//",CODATE"//i//",COMJD"//i//",COEXPI"//i//",COTSEP"//i//",COEXPT"//i,
		yes, >> out1)
	}
	tsort (out1, "tsep", ascend+)

printf ("ngrp=%d nobsmax=%d grpmin=%d\n", ngrp, nobsmax, grpmin)
	if (ngrp > 0 && nobsmax >= grpmin) {

	    if (decamnum) {
		hselect (in//"[1]", "$EXPNUM", yes) | sort | scan (stbid)
	    } else {
		stbid = "INDEF"
		hselect (in//"[1]", "$SB_ID", yes) | sort | scan (stbid)
		if (stbid == "INDEF")
		    hselect (in//"[1]", "$FILENAME", yes) | sort | scan (stbid)
	    }

	    # Group catalog.
	    out1 = out // "_grp.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c movid ch*7", >> out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c nobs i %2d", >> out1)
	    print ("#c rate r %5.1f arcsec/hr", >> out1)
	    print ("#c ratec r %5.1f arcsec/hr", >> out1)
	    print ("#c pa r %4d deg", >> out1)
	    print ("#c mag r %5.1f", >> out1)
	    print ("#c digest i %3d", >> out1)
	    mscselect (in, "CODATA,COGRPID,COGRPID,CONOBS,CORATE,CORATE,COPA,\
	        COMAG,COGRPID",
		expr="(CONOBS>="//grpmin//" && CORA!='INDEF')", >> out1)
	    tproject (out1, tmp1, "", uniq+)
	    rename (tmp1, out1)
	    tsort (out1, "rate", ascend+)

	    tdump (out1, cd="", pf="", data=tmp1, col="groupid,rate")
	    fd = tmp1
	    for (i=1; fscan(fd,ival,rate)!=EOF; i+=1) {
		movid (stbid=stbid, grpid=ival, dir="to",
		    decamnum=decamnum, verbose-)
		ratec = rate / dcos(min(abs(opp),85))
		partab (movid.movid, out1, "MOVID", i)
		partab (ratec, out1, "RATEC", i)
		printf ("%s %d\n", movid.movid, i, >> tmp4)
	    }
	    fd = ""; delete (tmp1)

	    # Observation catalog.
	    out1 = out // "_obs.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c movid ch*7", >> out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c ccd i %02d", >> out1)
	    print ("#c ra_obs d %13.2h hr", >> out1)
	    print ("#c dec_obs d %13.1h deg", >> out1)
	    print ("#c x d %6.1f pix", >> out1)
	    print ("#c y d %6.1f pix", >> out1)
	    print ("#c xco d %6.1f pix", >> out1)
	    print ("#c yco d %6.1f pix", >> out1)
	    print ("#c cutout ch*64", >> out1)

	    mscselect (in, "CODATA,COGRPID,COGRPID,COEXPID,CCDNUM,CORA,\
	        CODEC,COPX,COPY,COX,COY,$I",
		expr="(CORA!='INDEF')", >> out1)
	    tsort (out1, "groupid,expid", ascend+)

	    tdump (out1, cd="", pf="", data=tmp1, col="groupid")
	    fd = tmp1
	    for (i=1; fscan(fd,ival)!=EOF; i+=1) {
		movid (stbid=stbid, grpid=ival, dir="to",
		    decamnum=decamnum, verbose-)
		partab (movid.movid, out1, "MOVID", i)
	    }
	    fd = ""; delete (tmp1)

	    # MPC catalog and text.
	    out1 = out // "_mpc.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c movid ch*7", >> out1)
	    print ("#c groupid i %4d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c mpcrec ch*80", >> out1)

	    tjoin (out//"_ds.cat", out//"_exp.cat", tmp1,
	        "DATASET", "DATASET")
	    tjoin (tmp1, out//"_obs.cat", tmp2,
	        "DATASET,EXPID", "DATASET,EXPID")
	    tjoin (tmp2, out//"_grp.cat", tmp3,
	        "DATASET,GROUPID", "DATASET,GROUPID")
	    delete (tmp//"[12].tmp", verify-)
	    tdump (tmp3, cd="", pf="", data=tmp1,
	        col="DATEOBS,EXPTIME,GROUPID,EXPID,TSEP,MAG,RA_OBS,DEC_OBS", rows="-", pwidth=-1)
	    delete (tmp3, verify-)

	    fd = tmp1
	    for (row=1; fscan(fd,sval,t,ival,ival1,rval,mag,xi,eta)!=EOF; row+=1) {
		mpc = ""
		movid (stbid=stbid, grpid=ival, dir="to",
		    decamnum=decamnum, verbose-)
		#printf ("%11.11s\n", movid.movid) | scan (lval)
		printf ("     %-7.7s\n", movid.movid) | scan (lval)
		mpc += lval
	        print (sval) | translit ("STDIN", "T-", " ", del-) |
		    scan (i, j, t1, t2)
		#t1 += (t2 + rval / 3600) / 24
		t1 += t2 / 24
		if (isindef(duration))
		    t1 += t / 2 / 3600. / 24.
		else
		    t1 += duration / 2 / 3600. / 24.
		if (access (tmp2)) {
		    sval = ""; match (mpc//"*", tmp2, meta=no) | scan (sval)
		}
		if (sval == "") {
		    printf ("* C%04d %02d %08.5f\n", i, j, t1) | scan (lval)
		} else {
		    printf ("/ C%04d %02d %08.5f\n", i, j, t1) | scan (lval)
		    mag = INDEF
		}
		mpc += lval
	        printf ("%011.2h\n", xi) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		printf (" %11s\n", lval) | scan (lval)
		mpc += lval
	        printf ("%010.1h\n", abs(eta)) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		if (eta < 0)
		    printf (" -%10s\n", lval) | scan (lval)
		else
		    printf (" +%10s\n", lval) | scan (lval)
		mpc += lval
		if (isindef(mag))
		    printf ("%9w%5w %2w\n") | scan (lval)
		else
		    printf ("%9w%5.1f %2w\n", mag) | scan (lval)
		mpc += lval
		printf ("%5w%.3s\n", "W84") | scan (lval)
		mpc += lval
		printf ("%s %s %d %d '%s'\n",
		    dataset, movid.movid, ival, ival1, mpc) |
		    translit ("STDIN", "*/", " ", >> out1)
		print (mpc, >> tmp2)
	    }
	    fd = ""; delete (tmp1)

	    # MPC text.
	    mpctxt = out // "_mpc.txt"
	    sort (tmp2) | translit ("STDIN", "*/", " ", > mpctxt)
	    delete (tmp2)
	    digest2 (mpctxt, > out//"_digest2.txt")

	    # Update the digest2 score field in the group table.
	    fd = out//"_digest2.txt"
	    while (fscan (fd, lval) != EOF) {
	        if (fscan (lval, mpc, rval, ival) != 3)
		    next
		i = 0; match (mpc, tmp4) | scan (mpc, i)
		if (i > 0)
		    partab (ival, out//"_grp.cat", "DIGEST", i)  
	    }
	    fd = ""

	} else
	    printf ("WARNING: No groups found\n")

end
