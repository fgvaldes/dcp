# MPC_MATCH -- Find matched and unmatched sources in two MPC files.

procedure mpc_match (old, new, outroot)

file	old			{prompt="Old MPC file"}
file	new			{prompt="New MPC file"}
string	outroot			{prompt="Output root"}
real	match = 2		{prompt="Match distance (arcsec)"}
string	t = "mpc_match"		{prompt="Working root"}

begin
	string	o, n, out, fname
	string	id, matchedid, unmatchedid
	string	s1, s2, s3, s4, s5, s6, s7, s8,s9, s10
	real	d, m
	struct	rec

	# Query parameters.
	o = old
	n = new
	out = outroot
	d = match / 3600.

	#o = "DECamNEOSurvey_20140423Submitted"
	#n = "DEC14A_20140423_8595ac6-VR_AB_hime"

	# Add colons.
	match ("^     ", o, > t//".tmp")
	list = t//".tmp"; delete (t//"_old.tmp")
	while (fscan (list,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,m) != EOF) {
	    printf ("%s %s %s %s %s:%s:%s %s:%s:%s %4.1f\n",
		s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, m, >> t//"_old.tmp")
	}
	list = ""; delete (t//".tmp")

	match ("^     ", n, > t//".tmp")
	list = t//".tmp"; delete (t//"_new.tmp")
	while (fscan (list,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,m) != EOF) {
	    printf ("%s %s %s %s %s:%s:%s %s:%s:%s %4.1f\n",
		s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, m, >> t//"_new.tmp")
	}
	list = ""; delete (t//".tmp")

	# Match.
	tmatch (t//"_old.tmp", t//"_new.tmp", t//".tmp", "c5,c6", "c5,c6",
	    d, incol1="c1", incol2="c1", factor=" ",
	    diagfile="", nmcol1=" ", nmcol2="", sphere=yes)
	fields (t//".tmp", 2) | sort | uniq ( > t//"_matchid.tmp")
	delete (t//"_new.tmp,"//t//"_old.tmp,"//t//".tmp")

	# Extract matched and unmatched.
	delete (out//"_*matched.txt")
	list = n; matchedid = ""; unmatchedid = ""
	while (fscan (list, rec) != EOF) {
	    if (strlen(rec) != 80) {
		print (rec, >> out//"_matched.txt")
		print (rec, >> out//"_unmatched.txt")
		next
	    }
	    ;
	    i = fscan (rec, id)
	    if (id == matchedid) {
		print (rec, >> out//"_matched.txt")
		next
	    }
	    ;
	    if (id == unmatchedid) {
		print (rec, >> out//"_unmatched.txt")
		next
	    }
	    ;
	    matchedid = ""; match (id, t//"_matchid.tmp") | scan (matchedid)
	    if (matchedid == "") {
		unmatchedid = id
		print (rec, >> out//"_unmatched.txt")
	    } else
		print (rec, >> out//"_matched.txt")
	}
	list = ""; delete (t//"_matchid.tmp")
end
