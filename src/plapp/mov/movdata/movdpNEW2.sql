DROP TABLE MOVDS;
CREATE TABLE MOVDS (
    dataset          varchar(64),
    nexposures       int,
    date             timestamp,
    mjd              double precision,
    lst              varchar(13),
    ra_field         varchar(13),
    dec_field        varchar(13),
    gamma_field      varchar(13),
    beta_field       varchar(13),
    opposition       varchar(13),
    filter           varchar(64),
    ngrp             int,
    PRIMARY KEY (dataset)
    )
    ;

DROP TABLE MOVEXP;
CREATE TABLE MOVEXP (
    dataset          varchar(64),
    expname          varchar(64),
    dateobs          timestamp,
    mjdobs           double precision,
    expid            int,
    tsep             int,
    exptime          int,
    magzpt           real,
    fwhm             real,
    PRIMARY KEY (dataset, expid)
    )
    ;

DROP TABLE MOVGRP;
CREATE TABLE MOVGRP (
    dataset          varchar(64),
    groupid          int,
    movid            varchar(8),
    nobs             int,
    rate             real,
    pa               real,
    mag              real,
    digest           int,
    mpcsent          timestamp,
    mpcid            varchar(24),
    mpcsubid	     varchar(8),
    jpg              varchar(24),
    rate_rms         double precision,
    rate_pct         double precision,
    ratec            double precision,
    checked          text,
    duplicate_of     text,
    PRIMARY KEY (dataset, groupid, movid)
    )
    ;

DROP TABLE MOVOBS;
CREATE TABLE MOVOBS (
    dataset          varchar(64),
    groupid          int,
    movid            varchar(8),
    expid            int,
    ccd              int,
    ra_obs           double precision,
    dec_obs          double precision,
    x                real,
    y                real,
    xco              real,
    yco              real,
    cutout           varchar(64),
    duplicate_of     text,
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;

DROP TABLE MOVMPC;
CREATE TABLE MOVMPC (
    dataset          varchar(64),
    groupid          int,
    movid            varchar(8),
    expid            int,
    mpcrec           char(80),
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;

DROP TABLE ADDGRP;
CREATE TABLE ADDGRP (
    dataset          text,
    groupid          int,
    movid            text,
    nobs             int,
    rate             real,
    ratec            real,
    pa               real,
    mag              real,
    jpg              text,
    rate_rms         real,
    rate_pct         real,
    checked          text,
    found_as         text,
    PRIMARY KEY (dataset, groupid, movid)
    )
    ;

DROP TABLE ADDOBS;
CREATE TABLE ADDOBS (
    dataset          text,
    groupid          int,
    movid            text,
    expid            int,
    ccd              int,
    ra_obs           double precision,
    dec_obs          double precision,
    x                real,
    y                real,
    xco              real,
    yco              real,
    cutout           text,
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;

DROP SEQUENCE NSNUM;
CREATE SEQUENCE NSNUM;
