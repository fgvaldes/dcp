CREATE TABLE IF NOT EXISTS MOVDS (
    dataset          varchar(48),
    nexposures       int,
    date             varchar(24),
    mjd              double,
    lst              varchar(13),
    ra_field         varchar(13),
    dec_field        varchar(13),
    gamma_field      varchar(13),
    beta_field       varchar(13),
    opposition       varchar(13),
    filter           varchar(64),
    ngrp             int,
    PRIMARY KEY (dataset)
    )
    ;

CREATE TABLE IF NOT EXISTS MOVEXP (
    dataset          varchar(48),
    expname          varchar(48),
    dataobs          varchar(48),
    mjdobs           double,
    expid            int,
    tsep             int,
    exptime          int,
    PRIMARY KEY (dataset, expid)
    )
    ;

CREATE TABLE IF NOT EXISTS MOVGRP (
    dataset          varchar(48),
    groupid          int,
    movid            varchar(7),
    nobs             int,
    rate             double,
    pa               double,
    mag              double,
    mpcsent          varchar(24),
    mpcid            varchar(24),
    PRIMARY KEY (dataset, groupid, movid)
    )
    ;

CREATE TABLE IF NOT EXISTS MOVOBS (
    dataset          varchar(48),
    groupid          int,
    movid            varchar(7),
    expid            int,
    imageid          varchar(8),
    ccd              int,
    ra_obs           varchar(13),
    dec_obs          varchar(13),
    x                double,
    y                double,
    cutout           varchar(64),
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;

CREATE TABLE IF NOT EXISTS MOVMPC (
    dataset          varchar(48),
    groupid          int,
    movid            varchar(7),
    expid            int,
    mpcrec           char(80),
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;
