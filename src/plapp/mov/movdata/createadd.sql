/*
DROP TABLE ADDGRP;
CREATE TABLE ADDGRP (
    dataset          text,
    groupid          int,
    movid            text,
    nobs             int,
    rate             real,
    ratec            real,
    pa               real,
    mag              real,
    digest	     int,
    jpg              text,
    rate_rms         real,
    rate_pct         real,
    checked          text,
    found_as         text,
    PRIMARY KEY (dataset, groupid, movid)
    )
    ;
*/

DROP TABLE ADDOBS;
CREATE TABLE ADDOBS (
    dataset          text,
    groupid          int,
    movid            text,
    expid            int,
    ccd              int,
    ra_obs           double precision,
    dec_obs          double precision,
    x                real,
    y                real,
    xco              real,
    yco              real,
    cutout           text,
    PRIMARY KEY (dataset, groupid, movid, expid)
    )
    ;
