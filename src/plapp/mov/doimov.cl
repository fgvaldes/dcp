# DOIMOV -- Wrapper script for MOVIMOV.
#
# Input is list of DTS directories.
#
# The list of datasets to exclude is generally dumped from the database.

procedure doimov ()

file	input = ""		{prompt="Input list of dts directories"}
file	toppath = "../"		{prompt="Relative path to top from dts"}
file	exclude = ""		{prompt="List of datasets to exclude"}
bool	redo = no		{prompt="Redo?"}
bool	insert = yes		{prompt="Insert sql into database"}
bool	verbose = no		{prompt="Verbose?"}

struct	*fd, *fd1

begin
	file	in, dts, imov

	# Set input parameters.
	in = input
	if (in == "") {
	    in = "doimov.listtmp"
	    ls ("-d", "*-dts", "*/*-dts", > in)
	}
	;

	# Check for exclude file.
	if (exclude != "") {
	    if (!access(exclude)) {
		printf ("Exclude file not found (%s)\n", exclude)
		return
	    }
	    ;
	    path (exclude) | scan (exclude)
	}
	;

	# Loop through input files.
	fd = in
	while (fscan (fd, dts) != EOF) {
	    if (substr(dts,1,1) == '#')
	        next
	    if (!access(dts))
		next
	    ;

	    cd (dts)
	    files ("*_imov.cat", > "doimov.tmp")
	    fd1 = "doimov.tmp"
	    while (fscan (fd1, imov) != EOF)
		movimov (imov, toppath=toppath, exclude=exclude,
		    redo=redo, insert=insert, verbose=verbose)
	    fd1 = ""; delete ("doimov.tmp")
	    
	    back > "dev$null"
	}
	fd = ""

	if (in == "doimov.listtmp")
	    delete (in)
	;
end
