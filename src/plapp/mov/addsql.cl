# ADDSQL -- Create SQL from tables for added objects.

procedure addsql (root)

string	root			{prompt="Root name for tables and sql"}
bool	del = no		{prompt="Delete from DB first?"}

struct	*fd

begin

	int	i, last
	file	in, sql, tmp, tbl, insert
	string	ds

	int	ival
	real	rval
	string	sval
	struct	lval

	# Get query parameters.
	in = root

	# Set SQL filename.
	sql = in // ".sql"
	if (access(sql))
	    delete (sql)

	# Set temp files.
	tmp = in // ".tmp"
	insert = in // "_insert.tmp"

	# Drop existing data if desired. This assumes the dataset name
	# has a particular relationship to the input root name.
	ds = substr (in, 1, strldx('_',in)-1)
	if (del) {
	    printf ("DELETE FROM ADDGRP WHERE dataset='%s';\n", ds, >> sql)
	    printf ("DELETE FROM ADDOBS WHERE dataset='%s';\n", ds, >> sql)
	} else {
	    printf ("-- DELETE FROM ADDGRP WHERE dataset='%s';\n", ds, >> sql)
	    printf ("-- DELETE FROM ADDOBS WHERE dataset='%s';\n", ds, >> sql)
	}

	# Group table.
	tbl = in // "_grp.cat"
	if (access(tbl)) {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    printf ("INSERT INTO ADDGRP (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n", sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",   ival, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n", rval, >> sql)
		i = fscan (fd, ival); printf ("%d);\n",  ival, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	# Observation table.
	tbl = in // "_obs.cat"
	if (access(tbl)) {
	    tdump (tbl, cdfile=tmp, pfile="", datafile="")
	    printf ("INSERT INTO ADDOBS (\n", >> insert)
	    fd = tmp
	    for (i=1; fscan(fd,sval)!=EOF; i+=1) {
		if (i == 1)
		    printf ("%s", sval, >> insert)
		else
		    printf (",%s", sval, >> insert)
	    }
	    fd = ""; delete (tmp)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, sval) != EOF) {
		concat (insert, sql, append+)
				      printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, rval); rval*=15; printf ("%.6f,\n",   rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, sval); printf ("'%s');\n", sval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

	# Added astroid truth table.
	tbl = substr (in, 1, strldx('_',in)-1) // "_addast.txt"
	if (!access(tbl)) {
	    sval = substr (in, 1, strldx('_',in)-1)
	    tbl = substr (sval, 1, strldx('_',sval)-1) // "_addast.txt"
	}
	if (access(tbl)) {
	    printf ("INSERT INTO ADDGRP (\n", >> insert)
	    printf ("dataset,movid,groupid,rate,pa,mag", >> insert)
	    printf ("\n) VALUES (\n", >> insert)

	    tsort (tbl, "COGRPID,COEXPID")
	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT",
	        columns="cogrpid,corate,copa,comag") |
		words (> tmp)
	    fd = tmp; last = 0
	    while (fscan (fd, ival) != EOF) {
	        lval = ds
		if (ival == last) {
		    i = fscan (fd, rval)
		    i = fscan (fd, rval)
		    i = fscan (fd, rval)
		    next
		}
		last = ival

	        concat (insert, sql, append+)
				      printf ("'%s',\n",  lval, >> sql)
		sval = substr(lval, strldx("_",lval)+1, stridx("-",lval)-1)
		sval += "_" // substr(lval, strldx("-",lval)+1, 999)
		                      printf ("'%s_%03d',\n", sval,ival, >> sql)
		                      printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, rval); printf ("%.2f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%d,\n",    rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f);\n",  rval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)

	    printf ("INSERT INTO ADDOBS (\n", >> insert)
	    printf ("dataset,movid,groupid,expid,ccd,ra_obs,dec_obs,x,y", >> insert)
	    printf ("\n) VALUES (\n", >> insert)

	    tdump (tbl, cdfile="", pfile="", datafile="STDOUT",
	        columns="cogrpid,coexpid,coccd,ra,dec,x,y") | words (> tmp)
	    fd = tmp
	    while (fscan (fd, ival) != EOF) {
	        lval = ds
	        concat (insert, sql, append+)
				      printf ("'%s',\n",  lval, >> sql)
		sval = substr(lval, strldx("_",lval)+1, stridx("-",lval)-1)
		sval += "_" // substr(lval, strldx("-",lval)+1, 999)
		                      printf ("'%s_%03d',\n", sval,ival, >> sql)
		                      printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> sql)
		i = fscan (fd, rval); rval*=15; printf ("%.6f,\n",   rval, >> sql)
		i = fscan (fd, rval); printf ("%.6f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f,\n",  rval, >> sql)
		i = fscan (fd, rval); printf ("%.1f);\n",  rval, >> sql)
	    }
	    fd = ""; delete (insert//","//tmp)
	}

end
