# MOVIMOV -- Create moving data products from imov file.

procedure movimov (imov)

file	imov			{prompt="IMOV ifile"}
file	toppath = "../"		{prompt="Path to root of TOP directory"}
file	exclude = ""		{prompt="List of datasets to exclude"}
bool	redo = no		{prompt="Redo if sql present"}
bool	insert = no		{prompt="Insert sql into database"}
bool	verbose = no		{prompt="Verbose?"}

struct	*fd

begin
	file	top, mov
	file	in, tp, base, out
	real	x, y, z, t
	int	expid, movsize
	string	ds, ds1

	int	ival
	real	rval
	string	sval

	task $du = "$!du"

	# Get query parameters. Check for files.
	in = imov
	if (!access(in)) {
	    if (verbose)
	        printf ("File %s not found\n", in)
	    return
	}
	;
	if (verbose)
	    print (in)

	ds =   substr (in, strldx("/",in)+1, strstr("_imov",in)-1)
	if (access(exclude)) {
	    sval = ""; match (ds, exclude) | scan (sval)
	    if (sval != "") {
		if (verbose)
		    printf ("Exclude %s from exclude file\n", ds)
	        return
	    }
	    ;
	}
	;
	out = ""; files (ds//"_[ABC]_grp.cat") | scan (out)
	if (out != "") {
	    if (verbose)
		printf ("Exclude %s from grp file\n", ds)
	    return
	}
	;
	if (access(ds//".sql")) {
	    if (redo)
	        delete (ds//".sql")
	    else {
#		if (verbose)
#		    printf ("Use existing file %s\n", ds//".sql")
#	        if (insert) {
#		    printf ("Inserting %s\n", ds)
#		    adb ("-qf", ds//".sql")
#		}
#		;
		return
	    }
	    ;
	}
	;
	if (redo) {
	    if (verbose)
	        printf ("Remove any existing ds, exp, and sql files\n")
	    delete (ds//"_[ABC]_ds.cat")
	    delete (ds//"_[ABC]_exp.cat")
	    delete (ds//"_[ABC].sql")
	}
	;

	if (!verbose)
	    print (in)

	out = ""; mov = ""
	files (ds//"_[ABC]_ds.cat") | scan (out)
	files (ds//"*_mov.fits") | scan (mov)
	if (out != "" || mov != "") {
	    if (mov != "") {
		if (verbose)
		    printf ("Create from %s\n", mov)
		du (ds//"*_mov.fits") | sort (num+) | scan (movsize, mov)
		if (movsize < 100000)
		    movdp (mov, exponly+)
		else
		    movdp (mov, exponly+, update-)
		;
		ds1 = substr (mov, 1, strstr("_mov.fits",mov)-1)
	    } else {
		ds1 = substr (out, 1, strstr("_ds.cat",out)-1)
		if (verbose)
		    printf ("Create from %s and %s\n",
		        ds1//"_ds.cat", ds1//"_exp.cat")
	    }
	    ;
	    movsql (ds1, del=redo)
	    if (ds != ds1)
		rename (ds1//".sql", ds//".sql")
	    if (verbose)
	        printf ("Created %s\n", ds//".sql")
	    if (insert == YES) {
		printf ("Inserting %s\n", ds)
		adb ("-qf", ds//".sql")
	    }
	    ;
	    
	    return
	}
	;

	# Get the filenames with the rej file or the top/dcp file.
	tp = ds // "_rej.txt"
	if (access(tp)) {
	    if (verbose)
	        printf ("Set filenames from %s\n", tp)
	    match ("\t", tp) | match ("Filename", stop+, > "movimov.tmp")
	    count ("movimov.tmp") | scan (ival)
	    if (ival == 0)
	        delete ("movimov.tmp")
	    ;
	}
	;

	if (!access("movimov.tmp")) {
	    base = substr (ds, 1, stridx("-",ds)-1)
	    tp = toppath // base // "-top/" // base // ".top"
	    if (!access(tp))
		tp = toppath // base // "-dcp/" // base // ".dcp"
	    if (!access(tp)) {
		printf ("TOP/DCP file not found (%s)\n", base)
		return
	    }
	    ;
	    if (verbose)
	        printf ("Set filenames from %s\n", tp)
	    match ("ori.fits", tp, > "movimov.tmp")
	    count ("movimov.tmp") | scan (ival)
	    if (ival == 0 || ival > 10)
	        delete ("movimov.tmp")
	    ;
	}
	;

	if (!access("movimov.tmp")) {
	    printf ("Could not determine filenames (%s)\n", base)
	    return
	}
	;
	if (verbose)
	    concat ("movimov.tmp")

	if (verbose)
	    printf ("Create from %s\n", in)

	# Do some calculations.
	t = INDEF; x = INDEF; y = INDEF; z = INDEF
	keypar (in, "mjd-obs")
	if (keypar.found)  
	    t = real(keypar.value)
	keypar (in, "ra")
	if (keypar.found)  
	    x = real(keypar.value) * 15
	keypar (in, "dec")
	if (keypar.found)  
	    y = real(keypar.value)
	if (!(isindef(t)||isindef(x)||isindef(y))) {
	    suncoord (t, verbose-)
	    z = (mod (suncoord.ra + 12, 24) - x) * 15
	    if (z < -180)
		z += 360
	    if (z > 180)
		z -= 360
	    print (x, y) |
		ecliptic ("STDIN", in_c="equatorial", units="deg", print-) |
		scan (x, y)
	}

	# Make dataset table.
	out = ds // "_ds.cat"
	print ("#c dataset ch*48", > out)
	print ("#c procid ch*8", >> out)
	print ("#c nexposures i %2d", >> out)
	print ("#c date ch*24", >> out)
	print ("#c mjd d %15.8f", >> out)
	print ("#c filter ch*64", >> out)
	print ("#c ra d %13.2h hr", >> out)
	print ("#c dec d %13.1h deg", >> out)
	print ("#c gamma d %13.1h deg", >> out)
	print ("#c beta d %13.1h deg", >> out)
	print ("#c opposition d %13.1h deg", >> out)
	print ("#c photref ch*10", >> out)
	print ("#c plver ch*10", >> out)
	print ("#c propid ch*10", >> out)
	print ("#c pi ch*68", >> out)
	print ("#c night ch*10", >> out)
	print ("#c observers ch*68", >> out)

	printf ('%-48s', ds, >> out)
	i = strldx ('_', ds) + 1
	sval = substr (ds, i, i+6)
	printf (' %-8s', sval, >> out)
	count ("movimov.tmp") | scan (ival)
	printf (' %2d', ival, >> out)
	keypar (in, "date-obs")
	if (keypar.found)
	    printf (' %-24s', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "mjd-obs")
	if (keypar.found)
	    printf (' %15.8f', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "filter")
	if (keypar.found)
	    printf (' "%-64s"', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "ra")
	if (keypar.found)
	    printf (' %13.2h', real(keypar.value), >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "dec")
	if (keypar.found)
	    printf (' %13.1h', real(keypar.value), >> out)
	else
	    printf (' INDEF', >> out)
	if (!isindef(x))
	    printf (' %13.1h', x, >> out)
	else
	    printf (' INDEF', >> out)
	if (!isindef(y))
	    printf (' %13.1h', y, >> out)
	else
	    printf (' INDEF', >> out)
	if (!isindef(z))
	    printf (' %13.1h', z, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "photref")
	if (keypar.found)
	    printf (' %-10s', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	printf (' %-10s', "INDEF", >> out)
	keypar (in, "dtpropid")
	if (keypar.found)
	    printf (' %-10s', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "dtpi")
	if (keypar.found)
	    printf (' "%-68s"', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "dtcaldat")
	if (keypar.found)
	    printf (' %-10s', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)
	keypar (in, "observer")
	if (keypar.found)
	    printf (' "%-68s"\n', keypar.value, >> out)
	else
	    printf (' INDEF', >> out)

	# Exposure table.
	out = ds // "_exp.cat"
	print ("#c dataset ch*48", > out)
	print ("#c expname ch*48", >> out)
	print ("#c dateobs ch*32", >> out)
	print ("#c mjdobs d %15.8f", >> out)
	print ("#c expid i %2d", >> out)
	print ("#c tsep r %d sec", >> out)
	print ("#c exptime r %d sec", >> out)

	fd = "movimov.tmp"; expid = 0
	while (fscan (fd, in) != EOF) {
	    ival = strstr("_z", in)
	    if (ival > 0)
	        in = substr (in, 1, ival-1)
	    expid += 1
	    printf ('%-48s', ds, >> out)
	    ival = strstr (".fits", in)
	    if (ival == 0) {
		printf (' %-48s', in, >> out)
		printf (' INDEF', >> out)
		printf (' INDEF', >> out)
		printf (' %2d', expid, >> out)
		printf (' INDEF', >> out)
		printf (' INDEF\n', >> out)
	    } else {
		sval = substr (in, strldx("/",in)+1, ival-1)
		if (substr(sval,1,3) == 'c4d')
		    sval = substr (sval, 1, strstr("_ori",sval)-1)
		;
		printf (' %-48s', sval, >> out)
		hselect (in//"[0]", "$DATE-OBS", yes) | scan (sval)
		printf (' %-32s', sval, >> out)
		hselect (in//"[0]", "$MJD-OBS", yes) | scan (rval)
		printf (' %-15.8f', rval, >> out)
		printf (' %2d', expid, >> out)
		printf (' %2d', 0, >> out)
		hselect (in//"[0]", "$EXPTIME", yes) | scan (rval)
		printf (' %d\n', rval, >> out)
	    }
	    ;
	}
	fd = ""
	delete ("movimov.tmp")

	# Create sql from tables.
	movsql (ds, del=redo)
	if (verbose)
	    printf ("Created %s\n", ds//".sql")

	# Insert into database.
	if (insert == YES) {
	    printf ("Inserting %s\n", ds)
	    adb ("-qf", ds//".sql")
	}
	;

end
