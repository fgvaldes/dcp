# DOREV -- Wrapper review script.
#
# Input is list of files from stkmov.
# The list is typically grp.cat files produced by the new alias.

procedure dorev ()

file	input = "dorev.list"	{prompt="Input list of grp files"}
bool	auto = NO		{prompt="Auto review?"}
bool	redo = NO		{prompt="Redo previous reviews?"}
bool	scratch = NO		{prompt="Redo from the backup?"}
int	minexp = 4		{prompt="Min exposures for auto accept"}
int	minrater = 500		{prompt="Min rate for auto delete"}
int	maxrater = 1000		{prompt="Max rate for auto delete"}
file	rates = ""		{prompt="Output rates file"}

struct	*fd

begin
	file	in, dir, root, rfile
	string	mov, f, done, key
	int	i, j, k, m, n

	# Set input parameters.
	in = input
	if (rates != "")
	    path (rates) | scan (rfile)
	else
	    rfile = ""
	;

	# Set countdown value.
	count (in) | scan (m)

	# Loop through input files.
	fd = "dorev.list"; n = 0
	while (fscan (fd, f) != EOF) {
	    n += 1
	    if (access(f)==NO && redo==NO)
		next
	    ;
	    i = strldx("/", f)
	    j = strldx("_", f)
	    if (j == 0)
		next
	    else {
		f = substr (f, 1, j-1) // "_mov.fits"
		done = substr (f, 1, j-1) // "_dorev.done"
	    }
	    ;
	    if (access(f)==NO)
		next
	    ;

	    # Check if done already.
	    if (access(done)==YES) {
	        if (redo)
		    delete (done)
		else
		    next
		;
	    }
	    ;

	    # Review.
	    dir = substr (f, 1, i)
	    printf ("%s: %d / %d\n", dir, n, m)

	    cd (dir)
	    mov = substr (f, i+1, 999)

	    # From backup.
	    if (scratch == YES && access("BAKDIR/"//mov) == YES)
	        move ("BAKDIR/"//mov, ".", verbose+)
	    ;

	    movrev (mov, id=1, lastkey="", auto=auto, rates=rfile,
	        minexp=minexp, maxexpr=3, minrater=minrater, maxrater=maxrater)
	    if (movrev.lastkey == 'Q') {
		back > dev$null
		break
	    }
	    ;
	    if (movrev.lastkey == 'I' || movrev.lastkey == 'K') {
		printf ("Repeat? ")
		key = 'n'
		k = fscan (imcur, x, y, i, key)
		printf ("\n")
		if (key == 'y')
		    movrev (mov, id=1, lastkey="", select="", space="A",
		        auto=auto, minexp=minexp,
			maxexpr=3, minrater=minrater, maxrater=maxrater)
		;
	    }
	    ;

	#    key = ""
	#    while (stridx("ynYN", key) == 0) {
	#	printf ("Update files? ")
	#	k = fscan (imcur, x, y, i, key)
	#	printf ("%s\n", key)
	#	if (stridx("yY", key) > 0)
		if (movrev.lastkey == 'A') {
		    sleep (3)
		    printf ("Redo DP? ")
		    key = 'n'
		    while (fscan (imcur, x, y, i, key) != EOF) {
			if (stridx ("ynYN", key) == 0)
			    next
			;
			printf ("%s\n", key)
			if (stridx ("yY", key) > 0)
			    movdp (in=mov)
			;
			break
		    }
		} else if (movrev.lastkey != 'W')
		    movdp (in=mov)
		;
	#    }
	    back > dev$null
	    touch (done)
	}
	fd = ""
end
