#!/bin/csh -f

cd $NHPPS_DIR_DATASET

putcalci *_ci.fits sflat 1 -1 NULL '\!STARFIL'
if ($? == 1) then
    plexit ERROR; exit $?
endif

plexit COMPLETED; exit $?
