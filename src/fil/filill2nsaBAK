#!/bin/env pipecl
#
# FILILL2NSA - Convert illumcor data products to NSA format.

file	html = "illumcor.html"
string	in, out, out1, extname
string	plfname, pldname, plqueue, plqname, plprocid, subset
string	dateobs
struct	plver, title

#string	trim = "[2:2047,2:4095]"
string	trim = ""

# Load tasks and packages.
noao
artdata
task $mkgraphic = "$!mkgraphic"
task $fpack = "$!fpack"

# Find the data.
if (nhppsargs.dataset == "None")
    files ("*_[0-9][0-9].fits", > "filill2nsa.tmp")
else
    match ("smth.fits", "*.ill", print-, > "filill2nsa.tmp")
count ("filill2nsa.tmp") | scan (i)
if (i < 1)
    plexit NODATA
;

# Set the PL keywords and the output rootname.
match ("PLVER:", "deccp$/Versions", > "filill2nsa1.tmp")
tail ("filill2nsa1.tmp", nl=1) | scan (in, plver)
delete ("filill2nsa1.tmp")
if (nhppsargs.dataset == "None") {
    head ("filill2nsa.tmp", nl=1) | translit ("STDIN", "-", " ") |
	scan (pldname, subset)
    print (pldname) | translit ("STDIN", "_", " ") |
	scan (plqueue, plqname, plprocid)
    print (subset) | translit ("STDIN", "_", " ") |
	scan (subset)
    printf ("%s_%s_%s-%s\n", plqueue, plqname, plprocid, subset) | scan (out)
} else {
    print (dataset.sname) | translit ("STDIN", "-", " ") |
	scan (pldname, subset)
    print (pldname) | translit ("STDIN", "_", " ") |
	scan (plqueue, plqname, plprocid)

    head ("filill2nsa.tmp", nl=1) | scan (in)
    hselect (in, "OBSID", yes) | scan (s2)
    s2 = substr (strupr(s2), 5, 19)
    printf ("%s_%s_%s-%s-%s\n", plqueue, plqname, plprocid, subset, s2) |
        scan (out)

    # Set the HTML preamble.
    print (html, >> dataset.fdelete)
    title = dataset.sname // " Illumination"
    printf ('<html><title>%s</title></body><center><h1>%s</h1></center>\n',
	title, title, > html)
    printf ('<table>\n', >> html)
}

out1 = out // "_ci.fits[0]"
if (imaccess (out1) || imaccess (out//"_ci.fits.fz[0]"))

    printf ("Skipping %s ... \n", out)

else {

    plfname = out // "_ci"
    out1 = plfname // ".fits"

    # Make the global header.
    print (out1//"\n"//out1//".fz", >> dataset.fdelete)
    print (out1, >> ".fildone_delete.tmp")

    mkglbhdr ("@filill2nsa.tmp", out1, exclude="")
#    mkglbhdr ("@filill2nsa.tmp", out1,
#	exclude="@"//nhppsargs.srcdir//"filill2nsa_exclude.dat")
    hedit (out1, "NEXTEND", i, add+)
    hedit (out1, "FILENAME", plfname, add+)
    hedit (out1, "OBSTYPE", "illumcor", add+)
    hedit (out1, "PROCTYPE", "MasterCal", add+)
    hedit (out1, "PRODTYPE", "image", add+)
    hedit (out1, "PLVER", plver, add+)
    hedit (out1, "PLFNAME", "dummy", add+)
    hedit (out1, "PLDNAME", "dummy", add+)
    hedit (out1, "PLQUEUE", "dummy", add+)
    hedit (out1, "PLQNAME", "dummy", add+)
    hedit (out1, "PLPROCID", "dummy", add+)
    hedit (out1, "PLFNAME", plfname, add+)
    hedit (out1, "PLDNAME", pldname, add+)
    hedit (out1, "PLQUEUE", plqueue, add+)
    hedit (out1, "PLQNAME", plqname, add+)
    hedit (out1, "PLPROCID", plprocid, add+)

    list = "filill2nsa.tmp"
    while (fscan(list,in)!=EOF) {
	imcopy (in//trim, "filill2nsatmp.fits[padlines=1024]", verb-)
	hedit ("filill2nsatmp.fits",
	    "FILTER,OBSTYPE,PROCTYPE,PRODTYPE,FILENAME,*SECA,*SECB,IMCMB*",
	    del+)
	hedit ("filill2nsatmp.fits", "GAIN*,RDNOISE*,SATURAT*,SLOT*,\
	    DES*,NSATPIX*,SKYSIGMA,SKYBRIGHT,NBLEED,BLDINTRP,FWHM,\
	    ELLIPTIC,OBJMASK,AVS*,FIXPIX,BPM", del+)
	hedit ("filill2nsatmp.fits", "FZALGOR", "RICE_1", add+)
	hedit ("filill2nsatmp.fits", "FZQMETHD", "SUBTRACTIVE_DITHER_1", add+)
	hedit ("filill2nsatmp.fits", "FZQVALUE", 4, add+)
	hedit ("filill2nsatmp.fits", "FZDTHRSD", "CHECKSUM", add+)
	hselect (in, "DETPOS", yes) | scan (extname)
	printf ("%s -> %s[%s]\n", in, out1, extname)
	printf ("%s[%s,append,inherit,padlines=32]\n", out1, extname) |
	    scan (s1)
	imcopy ("filill2nsatmp.fits", s1, verbose-)
	delete ("filill2nsatmp.fits")
    }
    list = ""

    hedit (out1//"[0]", "EXP*,DARK*,RA,DEC,TELRA,TELDEC,HA,ZD,AZ,DOMEAZ,\
        AIRMASS,CCDSEC,RADE*,DES*,SCAMP*,PUPIL*,SKYSUB,PROCID*", del+)

    ##### JPG file #####

    if (nhppsargs.dataset != "None") {
	out1 = out // "_cj"
	print (out1//".jpg", >> dataset.fdelete)

	imextensions (out//"_ci", > "filill2nsa1.tmp")
	touch (dataset.fprotect)
	#mkgraphic ("@filill2nsa1.tmp", out1, "world", "jpg", 32, "'i1>0'")
	mkgraphic ("@filill2nsa1.tmp", out1, "world", "jpg", 8, "'i1>0'")
	delete ("filill2nsa1.tmp")
    }
    ;
}

##### HTML file #####

if (nhppsargs.dataset != "None") {
    out1 = out // "_ci.fits"

    hselect (out1//"[0]", "$DATE-OBS,TITLE", yes) |
	scan (dateobs,title)
    if (dateobs == "INDEF")
	hselect (out1//"[1]", "$DATE-OBS", yes) | scan (dateobs)
    dateobs = substr (dateobs, 1, strldx(".",dateobs)-1)
    printf ('<tr><td>%s<br>%s<br>%s<td><img src="%s.jpg"></tr>\n',
	"Master Illumcor", dateobs, title, out//"_cj", >> html)

    printf ('</table></body></html>\n', >> html)

    copy (html, "index.html")
}
;

##### Compress FITS files #####

files ("*_ci.fits") | count | scan (i)
if (i > 0)
    #fpack ("-v", "-D", "-Y", "*_ci.fits")
    fpack ("-v", "*_ci.fits")
;

# Let fildp module start up deal with clean up.
if (nhppsargs.dataset == "None")
    delete ("filill2nsa.tmp")
else
    match (".fits", "*.ill", print-, > ".fildp_delete.tmp")

plexit COMPLETED
