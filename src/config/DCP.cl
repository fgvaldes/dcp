#{ DCP.CL -- Pipeline configuration script.
# This may do any valid IRAF setup but it is mostly intended for defining
# global variables.  Note that these become CL package parameters.

string	calprops = ""    	{prompt="PropIDs for cals"}
string	objprops = ""           {prompt="PropIDs for objects"}
string  ccds = ""		{prompt="CCDs to process"}
int	dcp_nextend = 62	{prompt="Required number of extensions"}
string	skip = "cor"		{prompt="Pipelines/Modules to skip"}
string	halt = ""		{prompt="Pipelines/Modules to halt"}
string	ccdproc_skip = "bf"	{prompt="ccdproc steps to skip"}
string	specialflag = ""	{prompt="Special flag"}
string	dqmode = ""		{prompt="DQ/WT/Interp mode"}
bool	zmi_review = no		{prompt="Review master zeros?"}
bool	fmi_review = no		{prompt="Review master dome flats?"}
bool	fil_review = yes	{prompt="Review master dark sky calibrations?"}
string	top_group = "filter"	{prompt="Group objects by"}
int	top_filtrig = 0		{prompt="Num of filters to trigger in parallel"}
int	cal_nmin = 5		{prompt="Min number of exposures in cal stack"}
int	cal_nmax = 100		{prompt="Max number of exposures in cal stack"}
bool	bpm_interp = no		{prompt="Interpolate the BPM features?"}
int	xtk_ovfunc = 0		{prompt="Overscan function"}
int	xtk_ovord = 1		{prompt="Overscan order"}
string	ccd_mask = ""		{prompt="Global CCD mask"}
bool	red_bleed = yes		{prompt="Do CP bleed mask step?"}
bool	omi_dofrg = no		{prompt="Do fringe correction?"}
real	wcs_rms = 1.5		{prompt="Maximum WCS rms for a valid solution"}
string	photref = "usno"	{prompt="Photometric reference"}
string	grp_illum = "yes"	{prompt="Illumination (yes|no|red|redcal|cal)"}
int	grp_nillum = 5		{prompt="Minimum exposures for illumination"}
bool	sky_pupil = yes		{prompt="Do pupil subtraction?"}
bool	sky_mkpupil = no	{prompt="Make pupil?"}
real	pupil_tweak = 1		{prompt="Tweak pupil amplitude"}
int	sky_sub = 8		{prompt="Sky subtraction (-1=none, 0=ace, order)"}
real	slenmin = 2000		{prompt="Minimum streak length (pix)"}
real	spotmin = 0.9		{prompt="Minimum frac of potential streak length"}
string	ill_type = "default"	{prompt="Illumination type"}
string	ill_run = ""		{prompt="Illumination run"}
bool	ill_mkcor = yes		{prompt="Make illumination correction?"}
int	ill_minexp = 40		{prompt="Minimum exptime for illumination"}
string	cor_type = "divide"	{prompt="Dark sky correction type"}
file	rsp_tp = ""		{prompt="Tangent point file"}
real	rsp_grid = 0.8		{prompt="TileFinder HealPix grid (deg)"}
real	rsp_tileradius = 0.25	{prompt="Tangent point tile radius (deg)"}
real	rsp_pixsize = 0.27	{prompt="Resample pixel size (arcsec/pix)"}
bool	rsp_global = yes	{prompt="Resample with global sky subtraction?"}
bool	rsp_local = yes		{prompt="Resample with local sky subtraction?"}
string	rsp_interp = "lsinc17"	{prompt="Resampling interpolant"}
string	stk_mode = "exposure"	{prompt="Stacking mode (exposure|ccd)"}
file	stk_tiles = ""		{prompt="File of tile outlimits\n"}
int	stk_sztile = 16000	{prompt="Maximum tile size in data product"}
string	stk_exclude = "31"	{prompt="CCDs to exclude in stacks"}
int	stk_minstack = 2	{prompt="Minimum number to stack"}
int	stk_maxstack = 100	{prompt="Maximum number to stack"}
file	stk_wts1 = ""		{prompt="File of expnum and weight"}
file	stk_wts2 = ""		{prompt="File of expnum and weight"}
string	stk_reject = "none"	{prompt="Rejection for stacking"}
int	stk_minmax = 0		{prompt="Minmax nlow/nhigh values"}
bool	stk_skip = no		{prompt="Skip difference detection?"}
real	ssk_diffsig = 3.5	{prompt="Difference detection sigma"}
string	ssk_diffconv = ""	{prompt="Difference detection convolution"}
string	stk_bins = "1 10 60 300" {prompt="Stacking bins (E,V,S,M in sec)"}
bool	stk_rejexp = yes	{prompt="Reject outlier exposures?"}
int	stk_mov = 0		{prompt="Find moving objects?"}
string	stk_movonly = "MOV"	{prompt="Dataset substring for moving search only"}
string	stk_movbins = "SML"	{prompt="Stacking bins for moving objects?"}
int	stk_mod = 0		{prompt="Sequence modulus for interleavng"}
int	stk_maxseq = 5		{prompt="Maximum sequence"}
real	stk_sepmax = 200	{prompt="Maximum separation (pixels)"}
real	stk_minrate = 0.01	{prompt="Minimum rate (arcsec/hr)"}
real	stk_maxrate = 600	{prompt="Maximum rate (arcsec/hr)"}
real	stk_pawin = 360		{prompt="PA window around ecliptic motion (deg)"}
int	stk_addast = 0		{prompt="Number of asteroids to add?"}
bool	stk_autompc = no	{prompt="Automatically send to MPC?"}
string	stk_sta = "none"	{prompt="Save stack data for further stacking?",
				enum="all|filter|dtcaldat|none"}
string	stk_alert = "!Alert_operator"	{prompt="Alert command"}
string	nsatrim = "[2:2047,2:4095]" {prompt="CCD trim for InstCal"}
string	rmptrim = "[*,*]"	{prompt="CCD trim for Resampled"}
string	newds = ""		{prompt="Make copy of files to new procid"}
bool	dts_compress = yes	{prompt="Compress data products?"}
bool	dts_submit = no		{prompt="Auto submit from dts pipeline?"}

keep
