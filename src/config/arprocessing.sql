-- Create indexed table of basic data.

CREATE TEMP TABLE raw AS
SELECT date(start_date) date, dtnsanam, dtpropid, dtpi FROM voi.siap
WHERE instrument='DECam' AND proctype='raw'
AND obstype IN ('object', 'standard')
AND object NOT SIMILAR TO '%(focus|donut|junk|pointing)%'
AND dtpropid NOT SIMILAR TO '%(0001|9999|0005)%'
AND start_date BETWEEN TIMESTAMP '20150804' AND TIMESTAMP '20150904';

CREATE INDEX rawidx ON raw (dtnsanam);

CREATE TEMP TABLE proc AS
SELECT date(start_date) date, dtnsanam, dtpropid, dtpi FROM voi.siap
WHERE instrument='DECam' and proctype='InstCal'
AND start_date BETWEEN TIMESTAMP '20150804' and TIMESTAMP '20150904';

CREATE INDEX procidx ON proc (dtnsanam);

-- Create count tables.

CREATE TEMP TABLE unprocessed AS
SELECT date, dtpropid, dtpi, count(date) unprocessed FROM
(SELECT * FROM raw EXCEPT SELECT * FROM proc) a
GROUP BY date, dtpropid, dtpi;

CREATE TEMP TABLE processed AS
SELECT date, dtpropid, dtpi, count(date) processed FROM
(SELECT * FROM raw INTERSECT SELECT * FROM proc) a
GROUP BY date, dtpropid, dtpi;

CREATE TEMP TABLE unprocessed_only AS
SELECT a.date, a.dtpropid, a.dtpi, 0 processed, unprocessed FROM unprocessed a,
(SELECT date, dtpropid, dtpi FROM unprocessed EXCEPT SELECT date, dtpropid, dtpi FROM processed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

CREATE TEMP TABLE processed_only AS
SELECT a.date, a.dtpropid, a.dtpi, processed, 0 unprocessed FROM processed a,
(SELECT date, dtpropid, dtpi FROM processed EXCEPT SELECT date, dtpropid, dtpi FROM unprocessed) b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

CREATE TEMP TABLE foo AS
SELECT a.date, a.dtpropid, a.dtpi, processed, unprocessed
FROM processed a, unprocessed b
WHERE a.date=b.date AND a.dtpropid=b.dtpropid AND a.dtpi=b.dtpi;

-- Create output.

SELECT * FROM processed_only UNION
SELECT * FROM unprocessed_only UNION
SELECT * FROM foo
ORDER BY date, dtpropid, dtpi;
