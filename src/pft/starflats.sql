-- Find star flat observations --
-- This assumes the object titles were done consistently --

select date(start_date) as date, object, prop_id, count(*) from voi.siap
where instrument='decam' and object~'star flat [ugrizYVN]' and proctype='raw'
group by date, object, prop_id order by date, object;
