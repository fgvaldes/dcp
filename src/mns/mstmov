#!/bin/env pipecl
#
# MSTMOV -- Find moving sources from difference catalogs.

# Check whether to run this module.
# Note that it will also exit early if no difference catalogs were made.
stk_mov = 3
if (stk_mov == 0)
    plexit COMPLETED
;

int	baddiff = 7000		# Max number of diff sources
int	badfilt = 5000		# Max number of filtered sources
int	maxpairs = 10000	# This gets multiplied by nim
real	minexp = 3
real	minrate = 0.01
real	maxrate = 600
real	minerr = 0.01
real	maxerr = 0.4
real	sepmin = 2.
real	sepmax = 300.
real	itersep = 0.8
real	sharp = 0.1
#real	isigavg = 0.8
#real	isigmax = 2.5
real	isigavg = 0.1
#real	iterisig = 0.5
real	iterisig = 0.
real	isigmax = 1.0
#int	iwin = 4
int	iwin = 6
#int	xborder = 200
int	xborder = 10
int	yborder = 10

real	maxdm=0.8
real	maxdpp=2
real	maxdw=1.0
real	maxde=0.2
real	maxdp=4
real	maxdr = -0.2
#real	maxdw=INDEF
#real	maxde=INDEF
#real	maxdp=INDEF

real	maxclp = 1
real	maxclr = -0.05
real	maxclm = 0.5

int	ncpix = 700
int	nlpix = 600

minexp = stk_mov

struct	*fd, wcs
int	l, m, n, nim, ncl, nout, maxncl
real	mjdmin, mjdmax, expmin, expmax, cotsep, avtsep
string	local, movdata, im, imlist
string	cofile, cofile1, cofile2, coimid
string	cat, icat, mcat, ocat, pcat, acat, bcat, ccat, dcat, tcat
string	fname, s4, s11, s21, s31, s41
struct	filter, line1

# Tasks and packages.
task $mklocal = "$!mklocal"
task $rmdir = "$!rm -r $(1)"
task $mail = "$!mail -s $1 $2 < $(3) >& /dev/null"
nfextern
ace
aceproto
cache	tinfo tstat
mov

# Make local working directory if desired.
local = ""
#mklocal ("/tmp") | scan (local)
#if (access ("local"))
#    cd ("local")
#;

# Paths and files.
movdata = nhppsargs.srcdir // "mstdata/"
cofile = dataset.sname // "_mov"
icat = dataset.sname // "_imov.cat"
mcat = dataset.sname // "_mmov.cat"

# Additional clean up for restart.
delete ("*cat")
delete ("*_[ABCDE]_*")

# Find difference catalogs and grouping parameters.
# Check for suitable exposure times and intervals.
# Right now we don't group but in future we should.

files (nhppsargs.datadir//"*.msk", > "mstmov2.tmp")
list = "mstmov2.tmp"
while (fscan (list, s1) != EOF) {
    s2 = ""; match ("cat", s1) | scan (s2)
    if (s2 == "")
        next
    ;
    thselect (s2, "MJD-OBS,EXPTIME", yes, >> "mstmov1.tmp")
    match ("cat", s1, >> "mstmov0.tmp")
}
list = ""; delete ("mstmov2.tmp")

# No catalogs.
if (access("mstmov1.tmp")==NO)
    plexit COMPLETED
;

sort ("mstmov1.tmp", num+, > "mstmov2.tmp"); delete ("mstmov1.tmp")
list = "mstmov2.tmp"
for (nim=0; fscan(list,x,y)!=EOF; nim+=1) {
    if (nim == 0) {
        mjdmin = x; mjdmax = x
	expmin = y; expmax = y
    } else {
        mjdmin = min (x, mjdmin); mjdmax = max (x, mjdmax)
        expmin = min (y, expmin); expmax = max (y, expmax)
    }
}
list = ""; delete ("mstmov2.tmp")

# Reset some parameters.
if (nim < 4) {
    maxpairs = 30000
    isigavg = 1.0
    sharp = 0.1
    sepmin = 1
}
;
    
minexp = stk_mov

printf ("xborder = %d, yborder = %d, baddiff = %d, badfilt = %d\n",
    xborder, yborder, baddiff, badfilt)
printf ("maxpairs = %d, minexp = %d\n", maxpairs, minexp)
printf ("minrate = %.0f, maxrate = %.0f, sepmin = %.0f, sepmax = %.0f\n",
    minrate, maxrate, sepmin, sepmax)
printf ("sharp = %.1f, isigavg = %.2f, isigmax = %.2f, iwin = %d\n",
    sharp, isigavg, isigmax, iwin)
printf ("maxdm = %.1f, maxdpp = %.0f, maxdw = %.1f\n",
    maxdm, maxdpp, maxdw)
printf ("maxde = %.1f, maxdp = %.0f, maxdr = %.1f\n",
    maxde, maxdp, maxdr)

# Add new identifiers for merged catalog.
s3 = dataset.name // "_filter.dat"
if (access(s3)) {
    printf ("Applying catalog filter: %s\n", s3)
    concat (s3)
}
;

list = "mstmov0.tmp"; s1 = ""; nim = 0
for (i=10; fscan(list,ocat)!=EOF; i+=tinfo.nrows) {
    cat = substr (ocat, strldx("/",ocat)+1, 999)
    fspath (cat, >> dataset.fdelete)

    # Filter the catalog.
    thselect (ocat, "CRMIN1,CRMAX1,CRMIN2,CRMAX2", yes) | scan (j, k, l, m)
    j += xborder; k -= xborder; l += yborder; m -= yborder
    printf ("X>%d&&X<%d&&Y>%d&&Y<%d\n", j, k, l, m) | scan (s2)
    acefilter ("NONE", icatalogs=ocat, ocatalogs=cat,
	nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	catomid="NUM", update-)
    flpr
    if (access(s3)) {
        fd = s3
	while (fscan(fd,line)!=EOF) {
	    acefilter ("NONE", icatalogs=cat, ocatalogs="mstmov1.tmp",
		nmaxrec=INDEF, iobjmasks="", oobjmasks="",
		catfilter=line, logfiles="", verbose=0, extnames="",
		omtype="all", catomid="NUM", update-)
	    flpr
	    rename ("mstmov1.tmp", cat)
	}
	fd = ""
    }
    ;
    printf ("B>%.2f&&(ISIGMAX>%.1f||ISIGAVG>%.1f)\n",
        sharp, isigmax, isigavg) | scan (s2)
    acefilter ("NONE", icatalogs=cat, ocatalogs="mstmov1.tmp",
	nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	catomid="NUM", update-)
    flpr
    rename ("mstmov1.tmp", cat)
    x = isigmax; y = isigavg; z = sharp
    tinfo (cat, ttout-)
    if (tinfo.nrows > baddiff) {
        printf ("WARNING: Skipping %s (nsource = %d > %d)\n",
	    cat, tinfo.nrows, baddiff)
	delete (cat)
        next
    }
    ;
printf ("%s: %3d %.1f %.2f %.2f\n", cat, tinfo.nrows, x, y, z)
    x = min (8., x + 0.1); y = min (4., y + 0.05); z = min (0.6, z + 0.02)
    for (; tinfo.nrows > badfilt && !(x==8. && y==4. && z==0.6);) {
	printf ("B>%.2f&&(ISIGMAX>%.1f||ISIGAVG>%.1f)\n", z, x, y) | scan (s2)
	acefilter ("NONE", icatalogs=cat, ocatalogs="mstmov1.tmp",
	    nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	    catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	    catomid="NUM", update-)
	flpr
	rename ("mstmov1.tmp", cat)
	tinfo (cat, ttout-)
printf ("%s: %3d %.1f %.2f %.2f\n", cat, tinfo.nrows, x, y, z)
	x = min (8., x + 0.1); y = min (4., y + 0.05); z = min (0.6, z + 0.02)
    }
    ;

    if (tinfo.nrows > badfilt) {
        printf ("WARNING: Skipping %s (nsource = %d > %d)\n",
	    cat, tinfo.nrows, badfilt)
	delete (cat)
        next
    }
    ;

#tinfo (cat, ttout-)
#printf ("%s: %3d\n", cat, tinfo.nrows)
    coimid = substr (cat, 1, strldx(".",cat)-1)
print (coimid)
    if (coimid != s1) {
        nim += 1
	s1 = coimid
    }
    ;

    thedit (cat, "COEXPID", nim, del-, show-)
    thedit (cat, "COIMID", coimid, del-, show-)

    if (tinfo.nrows == 0)
        next
    ;

    tcalc (cat, "COEXPID", nim, datatype="int")
    tcalc (cat, "NUM1", i//"+ROWNUM", datatype="int")
    thselect (cat, "MJD-OBS,EXPTIME", yes) | scan (x, y)
    z = x + y / 2 / 86400
    tcalc (cat, "MJD", z, datatype="double")
    tcalc (cat, "EXP", y, datatype="real")
    tinfo (cat, ttout-)
}

if (nim < stk_mov) {
    printf ("  Too few exposures (%d < %d)\n", nim, stk_mov)
    plexit HALT
}
;

# Merge the catalogs. Reverse the list to get the header we want.
files ("*cat*", > "mstmov0.tmp")
sort ("mstmov0.tmp", reverse+, > "mstmov0r.tmp")
tmerge ("@mstmov0r.tmp", icat, "append", allcols=yes,
    tbltype="default", allrows=100, extracol=0)
fspath (icat, >> dataset.fdelete)

# Make asteroid catalogs.
acat = "mstmova.tmp"
bcat = "mstmovb.tmp"
ccat = "mstmovc.tmp"
dcat = "mstmovd.tmp"
ocat = "mstmovo.tmp"
pcat = "mstmovp.tmp"
tcat = "mstmovt.tmp"

maxpairs *= nim
while (YES) {
    # Create pair catalog. Spatial limits are in arc seconds.
    y = sepmax
    for (x = isigavg ; x <= 5.; x += 0.05) {
	#acepalign (icat, ocat, pcat, acat, "",
	acepalign (icat, ocat, pcat, "", "",
	    icatdef=movdata//"acepairs.dat", ocatdef="", pcatdef="",
	    filter="S>"//x, iwin=iwin, minsep=sepmin, maxsep=y,
	    minrate=minrate, maxrate=maxrate, maxdm=maxdm, maxdw=maxdw,
	    maxde=maxde, maxdp=maxdp, type="moving", wempty=no,
	    maxdpp=maxdpp, maxdr=maxdr, align=0.)
	if (access(pcat)==YES && access(acat)==NO) {
	    tinfo (pcat, ttout-)
	    ncl = tinfo.nrows
	    if (ncl > maxpairs) {
		printf ("maxsep = %d, isigavg = %.2f: ncl = %d\n", y, x, ncl)
	        x += iterisig
		y *= itersep
		next
	    }
	    ;
	}
	;
	if (nim >= 5 && access(acat)==YES)
	    rename (acat, pcat)
	;
	if (access(pcat) == YES) {
	    tinfo (pcat, ttout-)
	    ncl = tinfo.nrows
	} else
	    ncl = 0
	printf ("maxsep = %d, isigavg = %.2f: ncl = %d\n", y, x, ncl)
	if (ncl < maxpairs)
	    break
	;
	if (nim == 3 && y > 0.5 *  sepmax) {
	    y *= 0.75
	    x -= 0.05
	}
	;
    }
    if (ncl > 0) {
	# Add columns for clustering.
	tcalc (pcat, "COSP", "cos(P/57.296)", datatype="real",
	    colunits="", colfmt="%.6g")
	tcalc (pcat, "SINP", "sin(P/57.296)", datatype="real",
	    colunits="", colfmt="%.6g")
	#tcalc (pcat, "SGN", "if (P<=-90||P>90) then -1 else 1", datatype="int",
	#    colunits="", colfmt="%2d")
	#tcalc (pcat, "DH", "SGN*((X-XH)*COSP+(Y-YH)*SINP)-RATE/3600*U",
	#    datatype="real", colunits="arcsec", colfmt="%.6g")
	#tcalc (pcat, "DH", "(X*COSP+(Y-YH)*SINP)-RATE/3600*U",
	#    datatype="real", colunits="arcsec", colfmt="%.6g")

	# Cluster the pairs.
	if (nim < 4)
	   #ncl = 1
	   ncl = 2
	else if (nim < 5)
	   ncl = 2
	else
	   ncl = 2
	maxncl = nim * (nim - 1) / 2 * 2

	for (x=2; x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1<=22\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc1.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc1.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc1.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc1.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>21.5&&M1<=23\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc2.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc2.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc2.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc2.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>22.5&&M1<=23.5\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc3.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc3.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc3.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc3.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>23&&M1<=24\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc4.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc4.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc4.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc4.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>23.5&&M1<=24.5\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc5.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc5.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc5.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc5.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>24&&M1<=24.5\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc6.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc6.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc6.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc6.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>24.25&&M1<=24.75\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc7.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc7.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc7.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc7.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=min(2,x+1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>24.5&&M1<=25\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc8.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc8.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc8.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc8.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	for (x=max(x,1); x>=0.4; x-=0.2) {
	    printf ("%.1f,%.1f,%.1f,%.1f,%.1f\n",
	        x, x, maxclp, maxclr, maxclm) | scan (s1)
	    printf ("M1>24.75&&M1<=25.25\n", minrate) | scan (s2)
	    acecluster (pcat, "mstmovc9.tmp", s1, mincluster=ncl,
		icatdef=movdata//"acecluster.dat",
		ocatdef=movdata//"acecluster1.dat",
		ifilter=s2, ofilter="")
	    tstat ("mstmovc9.tmp", "COGRPID", out=""); i = tstat.vmax
	    tstat ("mstmovc9.tmp", "NCLUSTER", out=""); j = tstat.vmax
	    printf ("%-20s %-20s: nclusters = %4d, max size = %5d\n",
	        s2, s1, i, j)
	    if (isindef(i)) {
	        delete ("mstmovc9.tmp")
		break
	    }
	    ;
	    if (j < maxncl)
	        break
	    ;
	}
	movacmerge ("mstmovc[1-9]*.tmp", ccat, tmp="mstmov_mrg")
	if (access(ccat)) {
	    tstat (ccat, "COGRPID", out=""); i = tstat.vmax
	    tstat (ccat, "NCLUSTER", out=""); j = tstat.vmax
	    printf ("Merged: nclusters = %d, max size = %d\n", i, j)
	    delete ("mstmovc[1-9]*.tmp")
	}
	;
	if (access(ccat) == YES) {
	    tinfo (ccat, ttout-)
	    ncl = tinfo.nrows
	} else
	    ncl = 0
	if (ncl > 0) {
	    # Set the average standard coordinates at the average time.
	    thselect ("@mstmov0.tmp", "TSEP", yes) | average | scan (avtsep)
	    tcalc (ccat, "AVXI", "AVXH+AVCOSP*AVRATE/3600*"//avtsep,
		datatype="real", colunits="arcsec", colfmt="%.4f")
	    tcalc (ccat, "AVETA", "AVYH+AVSINP*AVRATE/3600*"//avtsep,
		datatype="real", colunits="arcsec", colfmt="%.4f")
	    tcalc (ccat, "AVP", "57.296*atan2(AVSINP,AVCOSP)", datatype="real",
		colunits="arcsec", colfmt="%.4f")

	    # Make catalog of clusters.
	    tproject (ccat, dcat,
		"COGRPID AVXH AVYH AVRATE AVCOSP AVSINP AVXI AVETA AVP",
		uniq+)
	    tinfo (dcat, ttout-)
	    ncl = tinfo.nrows

	    # Expand the pair catalog to include cluster information.
	    tjoin (pcat, ccat, tcat, "N", "N", extrarows="neither",
	        tolerance="0.0")
	    rename (tcat, pcat)

	    # Separate pairs into unique sources.
	    tproject (pcat, "mstmov2.tmp",
	        "NA COGRPID AVXI AVETA AVRATE", uniq-)
	    tchcol ("mstmov2.tmp", "NA", "N", "", "", verbose-)
	    tproject (pcat, "mstmov3.tmp",
	        "NB COGRPID AVXI AVETA AVRATE", uniq-)
	    tchcol ("mstmov3.tmp", "NB", "N", "", "", verbose-)
	    tmerge ("mstmov2.tmp,mstmov3.tmp", "mstmov4.tmp", "append",
		allcols=yes, tbltype="default")
	    delete ("mstmov[23].tmp")
	    tproject ("mstmov4.tmp", "mstmov2.tmp",
	        "N COGRPID AVXI AVETA AVRATE", uniq+)
	    tproject (icat, "mstmov3.tmp", "NUM1 COEXPID")
	    tjoin ("mstmov2.tmp", "mstmov3.tmp", tcat, "N", "NUM1")
	    rename (tcat, ocat)
	    delete ("mstmov[234].tmp")
	    
	    # Require a minimum number of unique exposures in a cluster.
	    i = nim / 2
	    tproject (ocat, "mstmov2.tmp", "COGRPID COEXPID", uniq+)
	    thist ("mstmov2.tmp", "STDOUT", "COGRPID", nbins=INDEF,
	        lowval=INDEF, highval=INDEF, dx=1., clow=INDEF, chigh=INDEF,
		rows="-", outcolx="COGRPID", outcoly="CONEXPID",
		> "mstmov3.tmp")
	    tchcol ("mstmov3.tmp", "C1", "COGRPID", "%d", "", verb-)
	    tchcol ("mstmov3.tmp", "C2", "CONEXPID", "%d", "", verb-)
	    tselect ("mstmov3.tmp", "mstmov4.tmp", "CONEXPID>="//i)
	    tinfo ("mstmov4.tmp", ttout-)
	    ncl = tinfo.nrows
	    if (ncl > 0) {
		tcalc ("mstmov4.tmp", "REGRPID", "ROWNUM", datatype="int")
		tjoin (ocat, "mstmov4.tmp", "mstmov5.tmp", "COGRPID",
		    "COGRPID", extra="neither", tol=0.0, case-)
		delete (ocat)
		tproject ("mstmov5.tmp", ocat, "!COGRPID", uniq-)
		tchcol (ocat, "REGRPID", "COGRPID", "", "", verbose-)
		tjoin (dcat, "mstmov4.tmp", "mstmov6.tmp", "COGRPID",
		    "COGRPID", extra="neither", tol=0.0, case-)
		delete (dcat)
		tproject ("mstmov6.tmp", "mstmov7.tmp", "!COGRPID", uniq-)
		tquery ("mstmov7.tmp", dcat, "abs(AVRATE)<500", "", "")
		tchcol (dcat, "REGRPID", "COGRPID", "", "", verbose-)
	    }
	    ;
	    delete ("mstmov[234567].tmp")
	}
	;
    }
    ;

    if (access(dcat)==YES) {
	tinfo (dcat, ttout-)
	ncl = tinfo.nrows
	if (ncl == 0)
	    break
	;
    }
    ;

    # Create cutouts and final catalogs.
    imlist = "mstmed_ims.tmp"

    # Make global header.
    fspath (cofile//".fits", >> dataset.fdelete)
    mkglbhdr ("@"//imlist, cofile, ref="", exclude="")
    hselect (cofile, "$FILTER", yes) | scan (filter)

    # Add some global keywords.
    hedit (cofile, "CONGRP", ncl, add+)
    hedit (cofile, "CODATA", dataset.sname, add+)
    hedit (cofile, "COFILE", cofile, add+)
    head (imlist, nl=1) | scan (im)
    hselect (im, "$DATE-OBS,$MJD-OBS,$LSTHDR", yes) | scan (s1, x, y)
    hedit (cofile, "CODATE", s1, add+)
    hedit (cofile, "COMJD", x, add+); hedit (cofile, "COMJD", x)
    if (isindef(y))
        s3 = "INDEF"
    else
	printf ("%10.1h\n", y) | scan (s3)
    hedit (cofile, "COLST", "", add+); hedit (cofile, "COLST", s3)
    hedit (cofile, "CONEXP", nim, add+)
    hedit (cofile, "COFILTER", filter, add+)

    # Average pointing.
    hselect ("@"//imlist, "CRVAL1,CRVAL2", yes, > "mstmov3.tmp")
    tstat ("mstmov3.tmp", "c1", outtable="")
    printf ("%13.2H\n", tstat.mean) | scan (s1)
    hedit (cofile, "CORACEN", "", add+);  hedit (cofile, "CORACEN", s1)
    tstat ("mstmov3.tmp", "c2", outtable="")
    printf ("%13.1h\n", tstat.mean) | scan (s1)
    hedit (cofile, "CODECCEN", "", add+);  hedit (cofile, "CODECCEN", s1)
    hselect (cofile, "CORACEN,CODECCEN", yes, > "mstmov3.tmp")
    acecopy ("mstmov3.tmp", "mstmov4.tmp",
        catdef="#ELON(c1,c2)\nELAT(c1,c2)", filter="", verb-)
    tdump ("mstmov4.tmp", cdfile="", pfile="", datafile="STDOUT", columns="",
	rows="-", pwidth=-1) | scan (x, y)
    printf ("%13.1h %13.1h\n", x, y) | scan (s1, s2)
    hedit (cofile, "COGAMMA", "", add+); hedit (cofile, "COGAMMA", s1)
    hedit (cofile, "COBETA", "", add+); hedit (cofile, "COBETA", s2)
    delete ("mstmov[34].tmp")

    # Exposure information.
    thselect ("@mstmov0.tmp", "FILENAME,COEXPID,DATE-OBS,MJD-OBS,TSEP,EXPTIME",
        yes) | sort | uniq ( > "mstmov2.tmp")
    list = "mstmov2.tmp"; i = 0; s3 = ""
    while (fscan(list,s1,j,s2,z,x,y)!=EOF) {
        if (s1 == s3)
	    next
	;
	i += 1; s3 = s1
        hedit (cofile, "COIM"//i, s1, add+)
        hedit (cofile, "COEXPI"//i, i, add+)
        hedit (cofile, "CODATE"//i, s2, add+)
        hedit (cofile, "COMJD"//i, z, add+); hedit (cofile, "COMJD"//i, z)
        hedit (cofile, "COTSEP"//i, x, add+)
        hedit (cofile, "COEXPT"//i, y, add+)
    }
    list = ""
    delete ("mstmov[234].tmp")

    if (ncl == 0)
        break
    ;

    list = "mstmov0.tmp"
    for (nout=0; fscan(list,cat)!=EOF;) {
	thselect (cat, "FILENAME,COEXPID,COIMID,TSEP,RSPRA,RSPDEC", yes) |
	    scan (fname, i, coimid, cotsep, x, y)
	match (substr(fname,1,stridx(".",fname)-1), imlist) | scan (im)
	#z = cotsep - avtsep
	z = cotsep
	printf ("gnomonic %g %g\n", 15*x, y) | scan (wcs)

	# Catalog of clusters.

	tcalc (dcat, "COEXPID", i, datatype="int")
	tcalc (dcat, "XI1", "AVXH+AVCOSP*AVRATE/3600*"//z, datatype="real",
	    colunits="arcsec", colfmt="%.4f")
	tcalc (dcat, "ETA1", "AVYH+AVSINP*AVRATE/3600*"//z, datatype="real",
	    colunits="arcsec", colfmt="%.4f")

	# Object catalog.
	tinfo (cat, ttout-)
	if (tinfo.nrows > 0) {
	    tjoin (ocat, cat, tcat, "N COEXPID", "NUM1 COEXPID",
	        extra="neither") 
	    rename (tcat, cat)
	}
	;

	# Cutouts
	if (tinfo.nrows > 0) {
	    tproject (dcat, "mstmov2.tmp",
		"COEXPID COGRPID XI1 ETA1 AVXI AVETA AVRATE", uniq-)
	    tproject (cat, "mstmov3.tmp",
		"COEXPID COGRPID RA DEC MAG XI ETA AVXI AVETA", uniq-)
	    tjoin ("mstmov2.tmp", "mstmov3.tmp", "mstmov4.tmp",
		"COEXPID,COGRPID,AVXI,AVETA", "COEXPID,COGRPID,AVXI,AVETA",
		extrarows="first", tolerance="0.0")
	    tsort ("mstmov4.tmp", "COGRPID")
	    delete ("mstmov[23].tmp")

	    # Because ttools doesn't handle INDEFs well we need to manually
	    # create the cutout catalog.  We also remove sources with the same
	    # group id.

	    tprint ("mstmov4.tmp", prparam-, prdata+, pwidth=1024,
		plength=0, showrow-, showhdr-,
	        columns="COGRPID,XI,ETA,XI1,ETA1,AVXI,AVETA,AVRATE,COEXPID,RA,DEC,MAG",
		rows="-", option="plain", align-, > "mstmov3.tmp")
	    delete ("mstmov4.tmp")

	    print ("#c COGRPID i %11d", > "mstmov2.tmp")
	    print ("#c XI d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c ETA d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c AVXI d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c AVETA d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c CORATE d %.4f arcsec/hr", >> "mstmov2.tmp")
	    print ("#c COEXPID i %11d", >> "mstmov2.tmp")
	    print ("#c RA d %13.3h hr", >> "mstmov2.tmp")
	    print ("#c DEC d %13.2h deg", >> "mstmov2.tmp")
	    print ("#c COMAG r %6.2f", >> "mstmov2.tmp")

	    print ("#c COIMID ch*10 %11s", >> "mstmov2.tmp")
	    print ("#c CONOBS i %d", >> "mstmov2.tmp")
	    print ("#c COTSEP i %d sec", >> "mstmov2.tmp")
	    print ("#c COPA r %6.1f", >> "mstmov2.tmp")

	    printf ("#k FILENAME = '%s'\n", fname, >> "mstmov2.tmp")

	    fd = "mstmov3.tmp"
	    k = fscan (fd, j, s1, s2, s3, s4, line); k = 0
	    i = j; s11 = s1; s21 = s2; s31 = s3; s41 = s4; line1 = line
	    while (fscan (fd, j, s1, s2, s3, s4, line) != EOF) {
		if (i == j)
		    k = i
		else if (i != k)
		    k = 0
		;
		if (i != k) {
		    if (s11=="INDEF")
			printf ("%6d %s %s %s %s 1 %d 0.\n",
			    i, s31, s41, line1, coimid, cotsep, >> "mstmov2.tmp")
		    else
			printf ("%6d %s %s %s %s 1 %d 0.\n",
			    i, s11, s21, line1, coimid, cotsep, >> "mstmov2.tmp")
		    nout += 1
		}
		;
		i = j; s11 = s1; s21 = s2; s31 = s3; s41 = s4; line1 = line
	    }
	    fd = ""; delete ("mstmov3.tmp")
	    if (j != k) {
		if (s1=="INDEF")
		    printf ("%6d %s %s %s %s 1 %d 0.\n",
		        j, s3, s4, line, coimid, cotsep, >> "mstmov2.tmp")
		else
		    printf ("%6d %s %s %s %s 1 %d 0.\n",
		        j, s1, s2, line, coimid, cotsep, >> "mstmov2.tmp")
		nout += 1
	    }
	    ;
	} else {
	    tproject (dcat, "mstmov4.tmp",
		"COEXPID COGRPID XI1 ETA1 AVXI AVETA AVRATE", uniq-)
	    tprint ("mstmov4.tmp", prparam-, prdata+, pwidth=1024,
		plength=0, showrow-, showhdr-,
	        columns="COGRPID,XI1,ETA1,AVXI,AVETA,AVRATE,COEXPID",
		rows="-", option="plain", align-, > "mstmov3.tmp")
	    delete ("mstmov4.tmp")

	    print ("#c COGRPID i %11d", > "mstmov2.tmp")
	    print ("#c XI d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c ETA d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c AVXI d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c AVETA d %.4f arcsec", >> "mstmov2.tmp")
	    print ("#c CORATE d %.4f arcsec/hr", >> "mstmov2.tmp")
	    print ("#c COEXPID i %11d", >> "mstmov2.tmp")

	    print ("#c RA d %13.3h hr", >> "mstmov2.tmp")
	    print ("#c DEC d %13.2h deg", >> "mstmov2.tmp")
	    print ("#c COMAG r %6.2f", >> "mstmov2.tmp")
	    print ("#c COIMID ch*10 %11s", >> "mstmov2.tmp")
	    print ("#c CONOBS i %d", >> "mstmov2.tmp")
	    print ("#c COTSEP i %d sec", >> "mstmov2.tmp")
	    print ("#c COPA r %6.1f", >> "mstmov2.tmp")
	    
	    printf ("#k FILENAME = '%s'\n", fname, >> "mstmov2.tmp")

	    fd = "mstmov3.tmp"
	    k = fscan (fd, i, line1); k = 0
	    while (fscan (fd, j, line) != EOF) {
		if (i == j)
		    k = i
		else if (i != k)
		    k = 0
		;
		if (i != k) {
		    printf ("%6d %s INDEF INDEF INDEF %s 1 %d 0.\n",
			i, line1, coimid, cotsep, >> "mstmov2.tmp")
		    nout += 1
		}
		;
		i = j; line1 = line
	    }
	    fd = ""; delete ("mstmov3.tmp")
	    if (j != k) {
		printf ("%6d %s INDEF INDEF INDEF %s 1 %d 0.\n",
		    j, line, coimid, cotsep, >> "mstmov2.tmp")
		nout += 1
	    }
	    ;
	}
	# Create cutouts.
#	acecutout (im, "mstmov2.tmp", cofile, coords="XI,ETA,RA,DEC",
#	    wcs=wcs, tiles="COEXPID,COGRPID", ncpix=ncpix, nlpix=nlpix,
#	    blkavg=1, gap=2, extname="XA",
#	    fields="COGRPID,COEXPID,COIMID,CONOBS,COTSEP,CORATE,COPA,COMAG")
	concat ("mstmov2.tmp", >> "mstmov_co.tmp")
	print ("#--------", >> "mstmov_co.tmp")
	delete ("mstmov2.tmp")
    }
    list = ""

    # Select real tracklets.
printf ('movselect ("mstmov_co.tmp", "mstmov_co.tmp", maxclust=10, minexp=%g,\n\t\
	minerr=%g, maxerr=%g, minrate=%g, maxrate=%g, rateA=2, rateB=3,\n\t\
	addast="", work="mstmov_co")\n',
	minexp, minerr, maxerr, minrate, maxrate)
    movselect ("mstmov_co.tmp", "mstmov_co.tmp", maxclust=10, minexp=minexp,
	minerr=minerr, maxerr=maxerr, minrate=minrate, maxrate=maxrate,
	rateA=2, rateB=3, addast="", work="mstmov_co")

    files ("mstmov_co_?.tmp", > "mstmov_co_list.tmp")

    # Create cutouts.
    fd = "mstmov_co_list.tmp"
    while (fscan (fd, s1) != EOF) {
        i = strldx (".", s1)
        s2 = substr (s1, i-2, i-1)
	cofile1 = dataset.sname // s2
	cofile2 = cofile1 // "_mov"
	if (access(cofile2//".fits"))
	    delete (cofile2//".fits")
	;
	imcopy (cofile, cofile2, v-)
	fspath (cofile2//".fits", >> dataset.fdelete)
	if (access("mstmov2.tmp")==YES)
	    delete ("mstmov2.tmp")
	;
	list = s1
	while (fscan (list, line) != EOF) {
	    if (substr (line, 1, 2) != "#-") {
		print (line, >> "mstmov2.tmp")
		next
	    }
	    ;

	    thselect ("mstmov2.tmp", "FILENAME", yes) | scan (fname)
	    match (fname, imlist) | scan (im)
	    acecutout (im, "mstmov2.tmp", cofile2, coords="XI,ETA,RA,DEC",
		wcs=wcs, tiles="COEXPID,COGRPID", ncpix=300, nlpix=300,
		blkavg=1, gap=2, extname="AA",
		fields="COGRPID,COEXPID,COIMID,CONOBS,COTSEP,CORATE,COPA,COMAG")
	    delete ("mstmov2.tmp")
	}
	list = ""

	# Check results.
	movdp (input=cofile2, select="A?*", update=yes, fast=no)
	fspath (cofile1//"_*", >> dataset.fdelete)
    }
    fd = ""

    imdelete (cofile)

    break
}
print Z

# Merge the catalogs.
delete (mcat, >& "dev$null")
if (ncl > 0) {
    tmerge ("@mstmov0r.tmp", mcat, "append", allcols=yes,
	tbltype="default", allrows=100, extracol=0)
    fspath (mcat, >> dataset.fdelete)
}
;

# Final files.
files ("*_mov.fits", > "mstmov.tmp")
files ("*_grp*,*_obs*,*_exp*,*_mpc*,*_ds*,*_dup*", >> "mstmov.tmp")
files ("*_mpc*,*digest2*,*_alert*,*.sql", >> "mstmov.tmp")
files ("*_imov,*_mmov*", >> "mstmov.tmp")
if (local != "")
    move ("@mstmov.tmp", nhppsargs.datadir)
;
concat ("mstmov.tmp", >> dataset.fdelete)

# Clean up.
delete ("@mstmov0.tmp")
if (access("mstmov_co.tmp")==YES) {
    rename ("mstmov_co.tmp", "save.tmp")
    delete ("mstmov*.tmp")
    rename ("save.tmp", "mstmov_co.tmp")
} else
    delete ("mstmov*.tmp")
;
cd (nhppsargs.datadir)
if (access ("local")) {
    rmdir (local)
    rmdir ("local")
}
;
match ("dcat", "*.msk", print-, >> ".mstdone_delete.tmp")

#plexit HALT
plexit COMPLETED
